<?php

require_once 'contributiontransactlegacy.civix.php';
use CRM_Contributiontransactlegacy_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function contributiontransactlegacy_civicrm_config(&$config) {
  _contributiontransactlegacy_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function contributiontransactlegacy_civicrm_xmlMenu(&$files) {
  _contributiontransactlegacy_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function contributiontransactlegacy_civicrm_install() {
  _contributiontransactlegacy_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function contributiontransactlegacy_civicrm_postInstall() {
  _contributiontransactlegacy_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function contributiontransactlegacy_civicrm_uninstall() {
  _contributiontransactlegacy_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function contributiontransactlegacy_civicrm_enable() {
  _contributiontransactlegacy_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function contributiontransactlegacy_civicrm_disable() {
  _contributiontransactlegacy_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function contributiontransactlegacy_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _contributiontransactlegacy_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function contributiontransactlegacy_civicrm_managed(&$entities) {
  _contributiontransactlegacy_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function contributiontransactlegacy_civicrm_angularModules(&$angularModules) {
  _contributiontransactlegacy_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function contributiontransactlegacy_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _contributiontransactlegacy_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function contributiontransactlegacy_civicrm_entityTypes(&$entityTypes) {
  _contributiontransactlegacy_civix_civicrm_entityTypes($entityTypes);
}
