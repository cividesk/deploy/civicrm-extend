<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */

use Civi\Payment\PropertyBag;

/**
 * Adjust Metadata for Transact action.
 *
 * The metadata is used for setting defaults, documentation & validation.
 *
 * @param array $params
 *   Array of parameters determined by getfields.
 */
function _civicrm_api3_contribution_transact_spec(&$params) {
  $fields = civicrm_api3('Contribution', 'getfields', ['action' => 'create']);
  $params = array_merge($params, $fields['values']);
  $params['receive_date']['api.default'] = 'now';
}

/**
 * Process a transaction and record it against the contact.
 *
 * @deprecated
 *
 * @param array $params
 *   Input parameters.
 *
 * @return array
 *   contribution of created or updated record (or a civicrm error)
 */
function civicrm_api3_contribution_transact($params) {
  // Start with the same parameters as Contribution.transact.
  $params['contribution_status_id'] = 'Pending';
  if (!isset($params['invoice_id']) && !isset($params['invoiceID'])) {
    // Set an invoice_id here if you have not already done so.
    // Potentially Order api should do this https://lab.civicrm.org/dev/financial/issues/78
    $params['invoice_id'] = md5(uniqid(rand(), TRUE));
  }
  if (isset($params['invoice_id']) && !isset($params['invoiceID'])) {
    // This would be required prior to https://lab.civicrm.org/dev/financial/issues/77
    $params['invoiceID'] = $params['invoice_id'];
  }
  elseif (!isset($params['invoice_id']) && isset($params['invoiceID'])) {
    $params['invoice_id'] = $params['invoiceID'];
  }

  $order = civicrm_api3('Order', 'create', $params);
  try {
    $params['amount'] = $params['total_amount'];
    $params['contribution_id'] = $order['id'];
    $payParams = $params;
    $payResult = reset(civicrm_api3('PaymentProcessor', 'pay', $payParams)['values']);

    // webform_civicrm sends out receipts using Contribution.send_confirmation API if the contribution page is has is_email_receipt = TRUE.
    // We allow this to be overridden here but default to FALSE.
    $params['is_email_receipt'] = $params['is_email_receipt'] ?? FALSE;

    // payment_status_id is deprecated - https://lab.civicrm.org/dev/financial/-/issues/141
    if (!isset($payResult['payment_status'])) {
      $payResult['payment_status'] = 'Pending';
      // payment_status_id = 1 -> payment completed;
      // payment_status_id = 2 -> payment NOT completed;
      if ($payResult['payment_status_id'] == '1') {
        $payResult['payment_status'] = 'Completed';
      }
    }

    if ($payResult['payment_status'] === 'Completed') {
      // Assuming the payment was taken, record it which will mark the Contribution
      // as Completed and update related entities.
      civicrm_api3('Payment', 'create', [
        'contribution_id' => $order['id'],
        'total_amount' => $payParams['amount'],
        'fee_amount' => $payResult['fee_amount'] ?? 0,
        'payment_instrument_id' => $order['values'][$order['id']]['payment_instrument_id'],
        // If there is a processor, provide it:
        'payment_processor_id' => $params['payment_processor_id'],
        'is_send_contribution_notification' => $params['is_email_receipt'],
        'trxn_id' => $payResult['trxn_id'] ?? NULL,
      ]);
    } else {
      \Civi\Api4\Contribution::update(FALSE)
        ->addWhere('id', '=', $order['id'])
        ->addValue('fee_amount', $payResult['fee_amount'] ?? 0)
        ->addValue('trxn_id', $payResult['trxn_id'] ?? NULL)
        ->execute();
    }
  }
  catch (\Exception $e) {
    // Payment failed - update the Contribution status in CiviCRM to "Failed"
    \Civi\Api4\Contribution::update(FALSE)
      ->addWhere('id', '=', $order['id'])
      ->addValue('contribution_status_id:name', 'Failed')
      ->execute();
    return ['error_message' => $e->getMessage()];
  }

  // Contribution.transact is expected to return an API3 result containing the contribution
  //   eg. [ 'id' => X, 'values' => [ X => [ contribution details ] ]
  return civicrm_api3('Contribution', 'get', ['id' => $order['id']]);
}
