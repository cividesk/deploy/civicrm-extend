<?php return array(
    'root' => array(
        'name' => 'civicrm/sqltasks',
        'pretty_version' => '2.1.0',
        'version' => '2.1.0.0',
        'reference' => '79eb09c8fc477213a19f201bfd6097541267b1d6',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'civicrm/sqltasks' => array(
            'pretty_version' => '2.1.0',
            'version' => '2.1.0.0',
            'reference' => '79eb09c8fc477213a19f201bfd6097541267b1d6',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
