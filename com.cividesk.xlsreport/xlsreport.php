<?php

require_once 'xlsreport.civix.php';

/**
 * Implementation of hook_civicrm_config
 */
function xlsreport_civicrm_config(&$config) {
  _xlsreport_civix_civicrm_config($config);
  // Include path is not working if relying only on the above function
  // seems to be a side-effect of CRM_Core_Smarty::singleton(); also calling config hook
  $extRoot = dirname( __FILE__ ) . DIRECTORY_SEPARATOR;
  set_include_path($extRoot . PATH_SEPARATOR . get_include_path());
  if (is_dir($extRoot . 'packages')) {
    set_include_path($extRoot . 'packages' . PATH_SEPARATOR . get_include_path());
  }
}

/**
 * Implementation of hook_civicrm_xmlMenu
 *
 * @param $files array(string)
 */
function xlsreport_civicrm_xmlMenu(&$files) {
  _xlsreport_civix_civicrm_xmlMenu($files);
}

/**
 * Implementation of hook_civicrm_install
 */
function xlsreport_civicrm_install() {
  return _xlsreport_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_uninstall
 */
function xlsreport_civicrm_uninstall() {
  return _xlsreport_civix_civicrm_uninstall();
}

/**
 * Implementation of hook_civicrm_enable
 */
function xlsreport_civicrm_enable() {
  return _xlsreport_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_disable
 */
function xlsreport_civicrm_disable() {
  return _xlsreport_civix_civicrm_disable();
}

/**
 * Implementation of hook_civicrm_upgrade
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed  based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 */
function xlsreport_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _xlsreport_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implementation of hook_civicrm_managed
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 */
function xlsreport_civicrm_managed(&$entities) {
  return _xlsreport_civix_civicrm_managed($entities);
}
