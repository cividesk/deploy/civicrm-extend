<?php
require_once 'PHPExcel.php';

/**
 * Membership01 report
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_xls_report_membership01($params) {
  $returnValues = array();

  // Process input parameters (w/ default values)
  $day = CRM_Utils_Array::value('day', $params, 1); // Which day of the month to generate report?
  $back = CRM_Utils_Array::value('back', $params, 15); // How many months back?
  $email = CRM_Utils_Array::value('email', $params, 'nicolas@cividesk.com');

  // While scheduler does not support monthly jobs ...
  if (date('d') != $day) {
    $returnValues[] = "Nothing to do today";
    return civicrm_api3_create_success($returnValues);
  }

  // Create spreadsheet & define styles
  $objPHPExcel = new PHPExcel();
  $objPHPExcel->getProperties()->setCreator("cividesk")
    ->setLastModifiedBy("cividesk")
    ->setTitle("Membership Report");
  $objWorksheet = $objPHPExcel->getActiveSheet();
  $style = array(
    'title'     => array('font' => array('size' => 14), 'alignment' => array('horizontal' => 'center')),
    'bold'      => array('font' => array('bold' => true)),
    'right'     => array('alignment' => array('horizontal' => 'right')),
    'shortdate' => array(
      'numberformat' => array('code' => 'mmm-yy'),
      'alignment' => array('horizontal' => 'right'),
      'borders' => array('bottom' => array('style' => 'thin')),
    ),
  );

  // Title rows
  $col = PHPExcel_Cell::stringFromColumnIndex($back);
  $objWorksheet->mergeCells("B1:".$col."1");
  $objWorksheet->setCellValue("B1", 'Membership Report');
  $objWorksheet->getStyle("B1")->applyFromArray($style['title']);
  $params = array('id' => CRM_Core_Config::domainID());
  CRM_Core_BAO_Domain::retrieve($params, $domain);
  $objWorksheet->mergeCells("B2:".$col."2");
  $objWorksheet->setCellValue("B2", $domain['name']);
  $objWorksheet->getStyle("B2")->applyFromArray($style['title']);

  $col = 'A'; $rmin = 3;
  // Fill in the membership type names
  // and get $rmax at the same time
  $query = "
SELECT mt.id, mt.name
  FROM civicrm_membership_type mt";
  $dao = CRM_Core_DAO::executeQuery($query);
  while ($dao->fetch()) {
    $objWorksheet->setCellValue($col.($rmin + $dao->id), $dao->name);
    $objWorksheet->getStyle($col.($rmin + $dao->id))->applyFromArray($style['right']);
    $rmax = max($rmax, $rmin + $dao->id);
  }
  // Other labels
  $objWorksheet->setCellValue($col.($rmax + 1), "Total");
  $objWorksheet->getStyle($col.($rmax + 1))->applyFromArray($style['bold']);
  $objWorksheet->setCellValue($col.($rmax + 3), "New");
  $objWorksheet->setCellValue($col.($rmax + 4), "Lapsed");
  $objWorksheet->getColumnDimension($col)->setAutoSize(true);

  // Fill in the information for the past months
  $now = date("Y-m-01"); // so we do not have strange effects with -x month calculations
  for ($m=0 ; $m<$back ; $m++) {
    $col = PHPExcel_Cell::stringFromColumnIndex($m + 1);
    $first = date("Y-m-01", strtotime($now .' -'.($back-$m).' month'));
    $last =  date("Y-m-01", strtotime($now .' -'.($back-$m).' month +1 month'));
    // Month header
    $objWorksheet->setCellValue($col.$rmin, PHPExcel_Shared_Date::stringToExcel($first));
    $objWorksheet->getStyle($col.$rmin)->applyFromArray($style['shortdate']);
    // Number of members for each membership type
    $query = "
SELECT mt.id, mt.name, COUNT(*) as num
  FROM civicrm_membership m
  LEFT JOIN civicrm_membership_type mt ON mt.id = m.membership_type_id
 WHERE m.is_test = 0 AND m.owner_membership_id IS NULL
   AND m.join_date <= '$last' AND '$last' < m.end_date
 GROUP BY mt.id";
    $dao = CRM_Core_DAO::executeQuery($query);
    while ($dao->fetch()) {
      $objWorksheet->setCellValue($col.($rmin + $dao->id), $dao->num);
    }

    // Now the Total row, in bold
    $objWorksheet->setCellValue($col.($rmax + 1), "=SUM($col".($rmin+1).":$col$rmax)");
    $objWorksheet->getStyle($col.($rmax + 1))->applyFromArray($style['bold']);

    // New, renew, lapsed
    $query = "
SELECT COUNT(*)
  FROM civicrm_membership m
 WHERE m.is_test = 0 AND m.owner_membership_id IS NULL
   -- NEW: join_date is within the month
   AND '$first' <= m.join_date AND m.join_date < '$last'";
    $count = CRM_Core_DAO::singleValueQuery($query);
    $objWorksheet->setCellValue($col.($rmax + 3), $count);
    $query = "
SELECT COUNT(*)
  FROM civicrm_membership m
 WHERE m.is_test = 0 AND m.owner_membership_id IS NULL
   -- LAPSED: end_date is within month
   AND '$first' <= m.end_date AND m.end_date < '$last'";
    $count = CRM_Core_DAO::singleValueQuery($query);
    $objWorksheet->setCellValue($col.($rmax + 4), $count);

  } // end for loop on past months

  // Some explanations
  $row = $rmax + 8;
  $explanations = array(
    'Explanations' => 'This report only considers PRIMARY members',
    'Total' => 'Active members as of the last day of the month',
    'New' => 'Number of new memberships created during the month',
    'Renew' => 'Number of existing memberships renewed during the month',
    'Laspsed' => 'Number of existing membership that expired during the month',
    'NOTE' => 'Small discrepancies will show up if membership dates have not been entered correctly in your database',
  );
  foreach ($explanations as $title => $text) {
    $objWorksheet->setCellValue('A'.$row, $title);
    $objWorksheet->setCellValue('B'.$row, $text);
    $row ++;
  }

  // Write the resulting spreadheet
  $fileName = tempnam(sys_get_temp_dir(), 'CDR');
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $objWriter->setIncludeCharts(TRUE);
  $objWriter->save($fileName);
  $returnValues[] = "Spreadsheet successfully created";

  // Send an email with the report attached
  $domain = CRM_Core_BAO_Domain::getDomain();
  list($domainName, $domainEmail) = CRM_Core_BAO_Domain::getNameAndEmail();
  $emails = explode(';', $email);
  foreach ($emails as $email) {
    $mailParams = array(
      'from' => "\"$domainName\" <$domainEmail>",
      'toEmail' => $email,
      'subject' => 'Monthly Membership Report',
      'html' => '
Dear Client,<br/><br/>
Per your request, please find attached your latest Monthly Membership Report.<br/><br/>
Sincerely,<br/>
Your <span style="color: #006fbf;">civi</span><span style="color: #ffc000;">desk</span> team.',
      'attachments' => array(
        0 => array(
          'fullPath'  => $fileName,
          'mime_type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          'cleanName' => 'MembershipReport-' . date('Y-m') . '.xlsx',
        ),
      ),
    );
    $returnValues[] = ( CRM_Utils_Mail::send($mailParams) ? "Mail successfully sent to $email" : "Mail coud not be sent to $email" );
  }
  unlink($fileName);

  return civicrm_api3_create_success($returnValues);
}

