<?php
/*
 * Cividesk commons module
 *
 * Adds the following features:
 * - adds Help menu entry to Support Center
 *
 */

require_once 'commons.civix.php';

/**
 * Implementation of hook_civicrm_config
 */
function commons_civicrm_config(&$config) {
  _commons_civix_civicrm_config($config);
}

/**
 * Implementation of hook_civicrm_xmlMenu
 *
 * @param $files array(string)
 */
function commons_civicrm_xmlMenu(&$files) {
  _commons_civix_civicrm_xmlMenu($files);
}

/**
 * Implementation of hook_civicrm_navigationMenu
 *
 * @param $files array(string)
 */
function commons_civicrm_navigationMenu(&$params) {
  $config = CRM_Core_Config::singleton();
  $help = dirname(CIVICRM_UF_BASEURL) . '/help';
  $account_status = CRM_Utils_System::url('civicrm/civideskaccount', 'reset=1' ,true, null, false);

  if ( $config->userFramework != 'Standalone' ) {
    $help = CRM_Utils_System::url('civicrm/cividesk', 'r=help&reset=1', true, null, false);
  }
  $currentVer = CRM_Core_BAO_Domain::version();
  if (version_compare($currentVer, '4.6.alpha1') < 0) {
    $parentMenu = 'Help';
  } else {
    $parentMenu = 'Support';
  }

  // Hide Menu of Support
  // Finds the 'Support' submenu
  $id_support = _find_menu($params, 'Support');
  $menu = $params[$id_support]['child'];

  // Re-key menu as will ease re-ordering
  $menu = array_values($menu);
  $menu_to_hide = array('Get started', 'Get expert help', 'Ask a question', 'Developer', 'Community Forums');

  foreach ($menu_to_hide as $item) {
    $id_hide_menu = _find_menu($menu, $item);
    $menu[$id_hide_menu]['attributes']['active'] = 0;
  }

  // And re-assign modified menu to parent
  $params[$id_support]['child'] = $menu;
/*
  _commons_civix_insert_navigation_menu($params, $parentMenu, array(
    'name'       => 'Support Center',
    'url'        => $help,
    'permission' => 'administer CiviCRM',
    'separator'  => 2, // before menu item
  ));

 _commons_civix_insert_navigation_menu($params, $parentMenu, array(
    'name'	 => 'Cividesk Account',
    'url'        => $account_status,
    'permission' => 'administer CiviCRM',
    'separator'  => 2, // before menu item
  ));
*/
}

function _find_menu($menu, $name) {
  foreach ($menu as $key => $value) {
    if ($value['attributes']['name'] == $name) {
      return $key;
    }
  }
  return NULL;
}

/**
 * Implementation of hook_civicrm_install
 */
function commons_civicrm_install() {
  return _commons_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_uninstall
 */
function commons_civicrm_uninstall() {
  return _commons_civix_civicrm_uninstall();
}

/**
 * Implementation of hook_civicrm_enable
 */
function commons_civicrm_enable() {
  return _commons_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_disable
 */
function commons_civicrm_disable() {
  return _commons_civix_civicrm_disable();
}

/**
 * Implementation of hook_civicrm_upgrade
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed  based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 */
function commons_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _commons_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implementation of hook_civicrm_managed
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 */
function commons_civicrm_managed(&$entities) {
  return _commons_civix_civicrm_managed($entities);
}

/**
 * Implementation of hook_civicrm_pageRun
 */
function commons_civicrm_pageRun(&$page) {
  if ($page->getVar('_name') == 'CRM_Group_Page_Group') {
    CRM_Core_Resources::singleton()->addScript("
      cj( document ).ready(function() {
        cj('.crm-group-group_id').hide();
      });
      cj(document).ajaxComplete(function(event, xhr, settings){
        if (settings.url.indexOf('civicrm/ajax/grouplist') >= 0) {
          cj('.crm-group-group_id').hide();
        }
      });
    ");
  }
}

/**
 * Implementation of hook_civicrm_post
 */
function commons_civicrm_post( $op, $objectName, $objectId, &$objectRef ) {
  //https://projects.cividesk.com/projects/95?modal=Task-6644-95
  if (($objectName == 'LineItem' && $op == 'create')) {
    $contributionID = $objectRef->contribution_id;
    $query = "
  SELECT COALESCE(SUM(qty * COALESCE(t3.non_deductible_amount, 0)))
      FROM civicrm_contribution t1
 JOIN civicrm_financial_type t2 ON
      t1.financial_type_id = t2.id AND is_deductible = 1
      JOIN civicrm_line_item t3 ON t1.id = t3.contribution_id
      WHERE t1.id = %1";

    $params = [
      1 => [$contributionID, 'Integer'],
    ];

    $non_deductible_amount = CRM_Core_DAO::singleValueQuery($query, $params);

    $query = "UPDATE civicrm_contribution SET non_deductible_amount = %1 WHERE id = %2";
    $params = [
      1 => [$non_deductible_amount, 'Float'],
      2 => [$contributionID, 'Integer'],
    ];

    CRM_Core_DAO::executeQuery($query, $params);
  }

  //this was a part of payment instrument
  //extension - moving this here so that all
  //the clients get this
  if (($objectName == 'FinancialTrxn')) {
    $paymentInstrumentID = $objectRef['payment_instrument_id'];
    $contributionID = $objectRef['contribution_id'];
    if ($paymentInstrumentID && $contributionID) {
      $p = CRM_Core_DAO::getFieldValue('CRM_Contribute_DAO_Contribution', $contributionID, 'payment_instrument_id');
      if ($p != $paymentInstrumentID) {
	//update contribution with the financial trxn
	//payment instrument if not the same
	//do it this way to avoid creation of more financial trxns
        $query = "UPDATE civicrm_contribution SET payment_instrument_id = %1 WHERE id = %2";
        $params = [
          1 => [$paymentInstrumentID, 'Integer'],
          2 => [$contributionID, 'Integer'],
         ];

         CRM_Core_DAO::executeQuery($query, $params);
      }
    }
  }
}

/**
 * Implementation of hook_civicrm_tokens
 */
function commons_civicrm_tokens( &$tokens ) {

  if (!array_key_exists('membership', $tokens)) {
    $tokens['membership'] = array();
  }
  $tokens['membership']['membership.name']        = ts('Membership Name');
  $tokens['membership']['membership.minimum_fee'] = ts('Membership Minimum Fee');
  $tokens['membership']['membership.duration']    = ts('Membership Duration');
  $tokens['membership']['membership.start_date']  = ts('Membership Start Date');
  $tokens['membership']['membership.end_date']    = ts('Membership End Date');
  $tokens['membership']['membership.paid_date']   = ts('Membership Paid Date');

  $tokens['contribution']['contribution.financial_type'] =  ts('Financial Type');
  //FIXME : this causes contact tokens not to be evaluated for civirules
  //$tokens['contact']['contact.full_address'] =  ts('Full Address');
  $tokens['crm_date']['crm_date.year']        = 'Date: ' . date('Y');
  $tokens['crm_date']['crm_date.monthfull']   = 'Date: ' . date('F');
  $tokens['crm_date']['crm_date.monthabbr']   = 'Date: ' . date('M');
  $tokens['crm_date']['crm_date.full']        = 'Date: ' . date('F j, Y');
  $tokens['crm_date']['crm_date.long']        = 'Date: ' . date('l, F j, Y');
  $tokens['crm_date']['crm_date.short_us']    = 'Date: ' . date('n/j/Y') ;
  $tokens['crm_date']['crm_date.short_other'] = 'Date: ' . date('d/m/Y') ;
  $tokens['crm_date']['crm_date.daymonth']    = 'Date: ' . date('j M') ;
  $tokens['crm_date']['crm_date.monthday']    = 'Date: ' . date('M j') ;
  $tokens['crm_date']['crm_date.monthyear']   = 'Date: ' . date('M Y') ;
  $tokens['contact_details']['contact_details.first_last_name'] = ts('First and Last Name');
}

/**
 * Implementation of hook_civicrm_tokenValues
 */
function commons_civicrm_tokenValues( &$values, &$contactIDs, $jobID = null, $tokens = array(), $context = null, $componentID = null) {
  if ( is_array( $contactIDs ) ) {
    $contactIDString = implode( ',', array_values( $contactIDs ) );
    $single = false;
  } else {
    $contactIDString = "$contactIDs";
    $single = true;
  }
  if ( ! is_array( $contactIDs ) ) {
    $crmContactIDs = array($contactIDs);
  } else {
    $crmContactIDs = array_values( $contactIDs );
  }

  foreach($crmContactIDs as $crmContactID) {
    if (array_key_exists('crm_date', $tokens ) ) {
      $values[$crmContactID]['crm_date.year']        = date('Y');
      $values[$crmContactID]['crm_date.monthfull']   = date('F');
      $values[$crmContactID]['crm_date.monthabbr']   = date('M');
      $values[$crmContactID]['crm_date.full']        = date('F j, Y');
      $values[$crmContactID]['crm_date.long']        = date('l, F j, Y');
      $values[$crmContactID]['crm_date.short_us']    = date('n/j/Y') ;
      $values[$crmContactID]['crm_date.short_other'] = date('d/m/Y') ;
      $values[$crmContactID]['crm_date.daymonth']    = date('j M') ;
      $values[$crmContactID]['crm_date.monthday']    = date('M j') ;
      $values[$crmContactID]['crm_date.monthyear']   = date('M Y') ;
    }
    if ( array_key_exists('contact', $tokens ) &&  in_array('full_address', $tokens['contact']) ) {
      $values[$crmContactID]['contact.full_address'] = '';
      $format = CRM_Core_BAO_Setting::getItem(CRM_Core_BAO_Setting::SYSTEM_PREFERENCES_NAME, 'address_format');
      $format = str_replace('{contact.address_name}', '', $format);

      $entityBlock = array('contact_id' => $crmContactID);
      $address = CRM_Core_BAO_Address::getValues($entityBlock);
      if ( ! empty( $address) ) {
        $addressPrimary = array_filter($address,
                            function($data) {
                              // filter data using is_primary flag
                              if ( $data['is_primary']) {
                                return $data;
                              } else {
                                return '';
                              }
                            }
                          );
        $fullAddress = reset($addressPrimary);
        if (! empty($fullAddress)) {
          $new_address = CRM_Utils_Address::format($fullAddress, $format);
          $values[$crmContactID]['contact.full_address'] =  nl2br($new_address);
        }
      }
    }

    if ( array_key_exists('contact_details', $tokens) ) {
      $name = array();
      $input = array('id' => $crmContactID);
      $contact_details = CRM_Core_DAO::commonRetrieve('CRM_Contact_DAO_Contact', $input, $name,
	         array('first_name', 'last_name')
      );
      $values[$crmContactID]['contact_details.first_last_name'] = $name['first_name'] . " " . $name['last_name'];
    }
  }
  if (array_key_exists('membership', $tokens ) || array_key_exists('contact_id', $values)) {
    $pendingStatusId = array_search('Pending', CRM_Member_PseudoConstant::membershipStatus());
    if ($context == 'CRM_Member_Form_Task_PDFLetterCommon' && !empty($componentID)) {
      $clause = ' m.id = ' . $componentID;
    } else {
      $clause = " m.contact_id IN ($contactIDString)";
    }
    $query = "SELECT * FROM (
      SELECT m.contact_id, m.start_date, m.end_date,
         mt.name, mt.minimum_fee, mt.duration_unit, mt.duration_interval,
         c.receive_date
      FROM   civicrm_membership m
        LEFT JOIN civicrm_membership_type mt ON mt.id = m.membership_type_id
        LEFT JOIN civicrm_membership_payment mp ON mp.membership_id = m.id
        LEFT JOIN civicrm_contribution c ON c.id = mp.contribution_id
      WHERE {$clause}
        AND  m.status_id != $pendingStatusId
        AND  m.is_test = 0
      ORDER BY m.contact_id, m.start_date DESC, c.receive_date DESC ) a GROUP BY a.contact_id
      ";

    $dao = CRM_Core_DAO::executeQuery( $query );
    while ( $dao->fetch( ) ) {
      if ( $single && ! is_array($values[$contactIDString])) {
        $value =& $values;
      } else if ( $single ) {
        $value =& $values[$contactIDString];
      } else {
        if ( ! array_key_exists( $dao->contact_id, $values ) ) {
          $values[$dao->contact_id] = array( );
        }

        $value =& $values[$dao->contact_id];
      }

      $value['membership.name'] = $dao->name;
      $value['membership.minimum_fee'] = CRM_Utils_Money::format($dao->minimum_fee, NULL, NULL, TRUE);
      if ($dao->duration_unit == 'lifetime')
        $value['membership.duration'] = $dao->duration_unit;
      else
        $value['membership.duration'] = $dao->duration_interval .' '. $dao->duration_unit;

      $domain = getenv('DOMAIN');
      if ($domain == 'mahj') {
        $start_date   = date('j/m/Y', strtotime($dao->start_date));
        $end_date     = date('j/m/Y', strtotime($dao->end_date));
        $receive_date = date('j/m/Y', strtotime($dao->receive_date));
      } else {
        $start_date   = date('F j, Y', strtotime($dao->start_date));
        $end_date     = date('F j, Y', strtotime($dao->end_date));
        $receive_date = date('F j, Y', strtotime($dao->receive_date));
      }
      // Sometimes date is NULL and NULL is converted as December 31, 1969
      $value['membership.start_date'] = $dao->start_date   ? $start_date   : '';
      $value['membership.end_date']   = $dao->end_date     ? $end_date     : '';
      $value['membership.paid_date']  = $dao->receive_date ? $receive_date : '';
    }
  }
}

/**
 * Implementation of hook_civicrm_buildForm
 */
function commons_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Contribute_Form_Contribution_Main') {
    if ( isset($form->_membershipBlock) && is_array($form->_membershipBlock) && array_key_exists( 'auto_renew', $form->_membershipBlock) ) {
      $auto_renew = array_filter($form->_membershipBlock['auto_renew']);
      if ( $form->_values['is_pay_later'] && is_array($form->_paymentProcessors) && ! empty( $auto_renew )) {
        if ($form->elementExists('auto_renew')) {
          $form->removeElement('auto_renew');
        }
        $form->addElement('checkbox', 'auto_renew', ts('Please renew my membership automatically.'));
        CRM_Core_Resources::singleton()->addScript("
          checkPaylaterAutoRenew();

          function checkPaylaterAutoRenew() {
            if ( cj('#auto_renew').is(':checked') && cj('#CIVICRM_QFID_0_payment_processor_id').length) {
              cj('input[name=\'payment_processor_id\']').removeAttr('checked');
              cj('#CIVICRM_QFID_0_payment_processor_id').hide();
              cj('label[for=\'CIVICRM_QFID_0_payment_processor_id\']').hide();
            } else if ( ! cj('#auto_renew').is(':checked') && cj('#CIVICRM_QFID_0_payment_processor_id').length) {
              //cj('input[name=\'payment_processor_id\']').removeAttr('checked');
              cj('#CIVICRM_QFID_0_payment_processor_id').show();
              cj('label[for=\'CIVICRM_QFID_0_payment_processor_id\']').show();
            }
          }

          cj( function() {
            cj('#auto_renew').change( function() {
              setTimeout(function() {
                checkPaylaterAutoRenew();
              }, 1000)
            });

            cj('.price-set-row').click( function() {
              checkPaylaterAutoRenew();
            });
          });
        ");
      }
    }
  }
}

/**
 *  Function to decide to show Desk Menu in Navigation bar.
 * @return bool
 */
function _commons_skip_desk_menu() {
  $domainList = array('demo-eu');
  $domain = getenv('DOMAIN');
  if (in_array($domain, $domainList)) {
    return true;
  }
  return false;
}

function _commons_civix_civicrm_js($config) {
  if (_commons_skip_desk_menu()) {
    return;
  }
  // add js only for normal civicrm url and skip for cron job.
  // Joomla throw error for isUserLoggedIn function because we calling isUserLoggedIn function though config hook that time JFactory class is not initialised.
  if (isset($_SERVER['SCRIPT_URL']) && preg_match('#civicrm/(packages|extern|bin)#', $_SERVER['SCRIPT_URL']) ) {
    return;
  }
  // CRM_Utils_System::isUserLoggedIn() bootstraps Drupal9, which creates errors in cv command.
  if ((CIVICRM_UF != 'Drupal8') && CRM_Utils_System::isUserLoggedIn()) {
    $desk = dirname(CIVICRM_UF_BASEURL) . '/sso/index.php';
    if ( $config->userFramework != 'Standalone' ) {
     $desk = CRM_Utils_System::url('civicrm/cividesk', 'r=sso/index.php&reset=1', true, null, false);
    }

    if (array_key_exists('snippet', $_GET)) {
      return;
    }
    CRM_Core_Resources::singleton()->addScript("
      cj( document ).ready(function() {
        cj('#menu-logout a').attr('href', '". $desk ."').html('Desk').attr('target','_blank');
        if(!cj('#menu-logout a').length) {
          cj('ul#civicrm-menu').append('<li class=\'menumain\' style=\'float:right;border-left: 1px solid #5D5D5D;margin-right: 20px;\' tabindex=\'25\'><a href=\'".$desk."\' target=\'_blank\'>Desk</a></li>');
        }
      });
    ");
  }
}

/**
 * Implementation of hook_civicrm_searchColumns()
 *
 */
function commons_civicrm_searchColumns($objectName, &$headers, &$rows, &$selector) {
  if ($objectName == 'relationship.rows' || $objectName == 'relationship.rows.user') {
    foreach ($rows as $rowId => $row) {
      if (empty($row['DT_RowAttr']['data-id'])) {
       continue;
      }

      $relationshipParams = [
        'id' => $row['DT_RowAttr']['data-id'],
        'return' => ['relationship_type_id', 'contact_id_a', 'contact_id_b'],
      ];
      try {
       // We always want the details for the contact on the "other" side of the relationship than the contact being shown.
       $currentContactId = CRM_Utils_Request::retrieveValue('cid', 'String');
       $relationship = civicrm_api3('Relationship', 'getsingle', $relationshipParams);
       if (!empty($relationship['contact_id_b'] && ($currentContactId != $relationship['contact_id_b']))) {
         $relationshipContactId = $relationship['contact_id_b'];
       }
       elseif (!empty($relationship['contact_id_a'] && ($currentContactId != $relationship['contact_id_a']))) {
         $relationshipContactId = $relationship['contact_id_a'];
       }
       if (!empty($relationshipContactId)) {
        $rows[$rowId]['position'] = CRM_Core_DAO::getFieldValue('CRM_Contact_DAO_Contact',   $relationshipContactId, 'job_title');
        }
      }
      catch (Exception $e) {
       // That's ok
      }
    }
  }
  if ($objectName == 'relationship.columns') {
    unset($headers['end_date']);
    $tmp = array_slice($headers, 0, 3, true) +
      array('position' => array('name' => ts('Position'))) +
      array_slice($headers, 3, count($headers)-1, true);
    $headers = $tmp;
    $headers['start_date']['name'] = ts('Since');
  }
}
/**
 * Implementation of hook_civicrm_alterTemplateFile
 */
function commons_civicrm_alterTemplateFile($formName, $form, $context, &$tplName) {
  // Dev domains have access to all
  $domain = getenv('DOMAIN');
  if ($domain == 'axa-dev') {
    return;
  }

  // Otherwise restrict some pages to @cividesk.com users only
  if (in_array($formName, [
    'CRM_Admin_Form_CMSUser',
    'CRM_Admin_Form_Setting_Smtp',
    //https://projects.cividesk.com/projects/28/tasks/5375
    'CRM_Admin_Page_MailSettings',
    'CRM_Admin_Form_Setting_Mail',
    'CRM_Admin_Form_Setting_Miscellaneous',
    'CRM_Admin_Form_Setting_Path',
    'CRM_Admin_Form_Setting_Url',
    'CRM_Admin_Form_Setting_UF',
    'CRM_Admin_Form_Setting_Debugging',
    'CRM_Admin_Form_Preferences_Multisite',
    'CRM_Admin_Page_Job',
    'CRM_SMS_Page_Provider',
    'CRM_Admin_Form_Generic',
    'CRM_Admin_Form_Setting_Component',
    'CRM_Admin_Page_Extensions',
    'CRM_Admin_Page_APIExplorer',
    'CRM_Admin_Page_PaymentProcessor'])) {
    $session = CRM_Core_Session::singleton();
    if (CRM_Utils_System::isUserLoggedIn() ) {
      $email = CRM_Contact_BAO_Contact::getPrimaryEmail($session->get('userID'));
      if (_common_domain_specific_permission($formName, $email) && substr($email, -13) !== '@cividesk.com') {
        $tplName = 'CRM/Commons/Error.tpl';
      }
    }
  }
}

/**
 * Implementation of hook_civicrm_alterMailParams
 */

function commons_civicrm_alterMailParams(&$params, $context) {
  // Add custom headers for ElasticEmail
  if ($context != 'messageTemplate') {
    if (!array_key_exists('headers', $params)) $params['headers'] = array();
    $params['headers']['X-ElasticEmail-Channel'] = getenv('DOMAIN');
  }
}

// Call ElasticEmail API function
// - also used in api/v3/ElasticEmail.php
function elasticemail_api($action, $params = array()) {
  $query = '';
  foreach ($params as $param => $value) $query .= "$param=$value&";
  $url = "https://api.elasticemail.com/v2/$action?" . substr($query, 0, -1);
  $response = json_decode(file_get_contents($url), true);  // will decode into an array rather than object
  return $response['data'];
}

/**
 * Implementation of hook_civicrm_pre
 */
function commons_civicrm_pre($op, $objectName, $objectId, &$objectRef) {
  //https://projects.cividesk.com/projects/7?modal=Task-6049-7
  if ($objectName == 'Activity' && $op == 'create') {
    if (CRM_Core_PseudoConstant::getLabel('CRM_Activity_BAO_Activity', 'activity_type_id', $objectRef['activity_type_id']) == 'Membership Signup') {
      $objectRef['status_id'] = 2;
    }
  }

  /*       FOR ElasticEmail only
   * When email is updated for contact, check if on hold flag is removed
   * If changed (unchecked) then remove same email from EE suppression list
   */
  $server = getenv('SMTP_HOST');
  if ($server != 'smtp.elasticemail.com') return;

  if ($objectName == 'Email' && $op == 'edit' && $objectId && empty($objectRef['on_hold'])) {
    $email = civicrm_api3('Email', 'getsingle', ['id' => $objectId]);
    if ($email['on_hold'] == 1) {
      $params = [
        'apikey' => getenv('SMTP_PASS') ?? getenv('SMTP_PASSWORD'),
        'status' => 0,
        'emails' => $email['email'],
      ];
      $result = elasticemail_api('contact/changestatus', $params);
      // Nothing to do if it fails
    }
  }

  //https://projects.cividesk.com/projects/107?modal=Task-5209-107
  if ($objectName == 'Profile' && $op == 'create' && !empty($objectRef['uf_group_id'])) {
    $title = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_UFGroup', $objectRef['uf_group_id'], 'title');
    $now = date('d/m/Y');
    //when a contact is added via profile,
    //source should reflect the same
    $objectRef['source'] = ts("Profile '{$title}' on {$now}");
  }
}

function _common_domain_specific_permission($formName, $email) {
  $domain = getenv('DOMAIN');
  if ($domain == 'uscj' && $formName == 'CRM_Admin_Page_Job' && $email == 'kunoff@uscj.org') {
    return false;
  }
  return true;
}

function commons_civicrm_idsException( &$skip ) {
  $skip[] = 'civicrm/ajax/markSelection';
  $skip[] = 'civicrm/admin/ckeditor';
}

function commons_civicrm_alterSettingsMetaData( &$settingsMetaData, $domainID, $profile) {
  // setting to hide status check
  $settingsMetaData['systemStatusCheck'] = array(
    'group_name' => 'CiviCRM Preferences',
    'group' => 'core',
    'name' => 'systemStatusCheck',
    'type' => 'Integer',
    'html_type' => 'Text',
    'default' => '0',
    'add' => '4.7',
    'title' => 'systemStatusCheck',
    'is_domain' => '1',
    'is_contact' => '0',
    'description' => '',
    'help_text' => '',
  );
}

/*
 * Generic debug function for all our developments
 */
function debug_log($message) {
  $config = CRM_Core_Config::singleton();
  file_put_contents($config->configAndLogDir . 'debug_log', $message . PHP_EOL, FILE_APPEND);
}
