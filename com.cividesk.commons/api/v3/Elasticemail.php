<?php

function ee_save_csv($file, $data) {
  $config = CRM_Core_Config::singleton();
  $tmpDir = $config->uploadDir;

  $fp = fopen($tmpDir . $file, 'w');
  if (sizeof($data)) {
    $header = array_keys(reset($data));
    fputcsv($fp, $header);
    foreach ($data as $line) fputcsv($fp, $line);
  }
  fclose($fp);
}

/**
 * Cividesk.Getaccountdetails API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_Elasticemail_sync($params) {

  $statuses = [
    -2 => 'Transactional',
    -1 => 'Engaged',
     0 => 'Active',
     1 => 'Bounced',
     2 => 'Unsubscribed',
     3 => 'Abuse',
     4 => 'Inactive',
     5 => 'Stale',
     6 => 'NotConfirmed',
  ];

  $apikey = $_SERVER['EE_APIKEY'] ?? $_SERVER['SMTP_PASS'] ?? $_ENV['SMTP_PASSWORD'];
  $count = 0; $inc = 250;
  $stats['count'] = 0;
  foreach ($statuses as $status) $stats[$status] = 0;

  CRM_Core_DAO::executeQuery("DROP TABLE IF EXISTS `ee_contact`");
  CRM_Core_DAO::executeQuery("
CREATE TABLE IF NOT EXISTS `ee_contact` (
  `Email` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateUpdated` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LastSent` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TotalSent` int(8) DEFAULT NULL,
  `TotalFailed` int(8) DEFAULT NULL,
  KEY `EE_Mail` (`Email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
");

  do {
    $query = "INSERT INTO `ee_contact` (`Email`, `Status`, `DateUpdated`, `LastSent`, `TotalSent`, `TotalFailed`) VALUES ";
    $execute = false;
    $data = elasticemail_api('contact/list', ['apikey' => $apikey, 'limit' => $inc, 'offset' => $count]);
    $count += $inc;
    foreach ($data as $line) {
      $line['status'] = $statuses[$line['status']];
      $stats['count']++;
      $stats[$line['status']]++;
// $query .= print_r($line, true);
      $email = str_replace("'", "\\'", $line['email']);
      $query .= "('$email','$line[status]','$line[dateupdated]','$line[lastsent]',$line[totalsent],$line[totalfailed]),";
      $execute = true;
    }
    if ($execute) CRM_Core_DAO::executeQuery(substr($query, 0, -1));
  } while (!empty($data));

  $allhold = CRM_Core_DAO::executeQuery("
SELECT COUNT(*) AS cnt FROM `civicrm_email` WHERE `on_hold`=1
")->fetchAll();
  $stats['allhold'] = $allhold[0]['cnt'];

  $unhold = CRM_Core_DAO::executeQuery("
SELECT cc.id, cc.display_name, ce.email, ce.on_hold, cc.is_opt_out, ee.status, ee.dateupdated, ee.lastsent, ee.totalsent, ee.totalfailed
  FROM `ee_contact` ee
       JOIN civicrm_email ce ON ce.email = ee.email AND ce.is_primary = 1
       JOIN civicrm_contact cc ON cc.id = ce.contact_id
 WHERE cc.is_deceased = 0 AND cc.is_deleted = 0 -- also ensures we have a matching contact!
       AND ee.status IN ('Active', 'Engaged') AND totalsent > 0
       AND ce.on_hold = 1
 GROUP BY ee.email
")->fetchAll();
  $stats['unhold'] = sizeof($unhold);
  ee_save_csv('unhold.csv', $unhold);

  $notsent = CRM_Core_DAO::executeQuery("
SELECT cc.id, cc.display_name, ce.email, ce.on_hold, cc.is_opt_out, ee.status
  FROM civicrm_email ce
       LEFT JOIN `ee_contact` ee ON ee.email = ce.email
       LEFT JOIN civicrm_contact cc ON cc.id = ce.contact_id
 WHERE cc.is_deceased = 0 AND cc.is_deleted = 0 -- also ensures we have a matching contact!
       AND ce.on_hold = 1 AND ee.status IS NULL
 GROUP BY ce.email
")->fetchAll();
  $stats['notsent'] = sizeof($notsent);

  $optout = CRM_Core_DAO::executeQuery("
SELECT cc.id, cc.display_name, ee.email, ce.on_hold, cc.is_opt_out, ee.status, ee.dateupdated, ee.lastsent, ee.totalsent, ee.totalfailed
  FROM `ee_contact` ee
       JOIN civicrm_email ce ON ce.email = ee.email AND ce.is_primary = 1
       JOIN civicrm_contact cc ON cc.id = ce.contact_id
 WHERE cc.is_deceased = 0 AND cc.is_deleted = 0           -- also ensures we have a matching contact!
       AND ee.status = 'Abuse' AND cc.is_opt_out = 0 AND ce.on_hold = 0
 GROUP BY ee.email
")->fetchAll();
  $stats['optout']  = sizeof($optout);
  ee_save_csv('optout.csv', $optout);

  return civicrm_api3_create_success($stats, $params, 'cividesk', 'Getaccountdetails');
}
