<?php
/**
 * Cividesk.Getaccountdetails API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC/API+Architecture+Standards
 */

function _query($query) {
  /* itbliss database connection handle */
  $dbhost = 'shakti.ops.cividesk.com';
  $dbuser = 'itbliss';
  $dbpass = 'xDnjyjd4VeuxBhba';
  $dbname = 'itbliss_customers';

  // DB connection
  try {
    $dbh = new PDO('mysql:dbname='. $dbname .';host='.$dbhost , $dbuser , $dbpass);
    $result = array();
    foreach($dbh->query($query) as $row) {
      $result[] = $row;
    }
  } catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
  }
  return $result;
}

function _civicrm_api3_cividesk_Getaccountdetails_spec(&$spec) {
  $spec['domain_name']['api.required'] = 1;
}

/**
 * Cividesk.Getaccountdetails API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_cividesk_Getaccountdetails($params) {

  if (array_key_exists('domain_name', $params) && !empty($params['domain_name'])) {

    $plain_domain = $params['domain_name'];
    // Get the Domain details
    $result = _query( "SELECT domain_nickname FROM domains WHERE domain='". $plain_domain . "'" );
    if(isset($result) && !empty($result[0]['domain_nickname'])) {
       $plain_domain = $result[0]['domain_nickname'];
    }

    // Get the Contact Details from 'Domain'
    $contact_details = civicrm_api3('Contact', 'get', array(
      'sequential' => 1,
      'return' => array("display_name", "id", "custom_256", "custom_63"),
      'nick_name' => $plain_domain,
    ));

    $result = '';
    $returnResult = array();
    if(!empty($contact_details) && isset($contact_details['id'])) {

      $contactID = $contact_details['id'];
      // Get all the Contact Tags
      $contactTags = civicrm_api3('EntityTag', 'get', array(
        'sequential' => 1,
        'entity_id' => $contactID,
        'entity_table' => "civicrm_contact",
      ));

      // Get All Account Managers Tag ////////////////////////////
      // Account Manager Tag ID : 77
      $acTags = civicrm_api3('Tag', 'get', array(
        'sequential' => 1,
        'return' => array("name", "id"),
        'parent_id' => 77,
      ));

      // check contact tag present in account manager tag list
      // if present then show the name of tag.
      $found = array();
      if(!empty($contactTags['values']) && isset($contactTags['values'])) {
        foreach($contactTags['values'] as $tagId) {
          foreach( $acTags['values'] as $accountManager) {
           if( in_array($tagId['tag_id'],$accountManager ) ) {
             $found[] = $accountManager['name'];
           }
          }
        }
      }
      $returnResult['account_managers'] =  $found;

      // Get Employee for the Contact /////////////////////////////
      $relationshipData = civicrm_api3('Relationship', 'get', array(
        'sequential' => 1,
        'contact_id_b' => $contactID, // contact id or org
        'relationship_type_id' => 4, // employee relationship type id
        'is_active' => 1,
        'options' => array(
          'limit' => 0,
        ),
        'return' => array("id", "contact_id_a", "contact_id_b"),
      ));

      // Get Contact Details
      $contactNameDetails = array();
      $contactFullName = array();
      if(!empty($relationshipData['values'])) {
        foreach($relationshipData['values'] as $k => $contact_ids) {
          //$contactNameDetails[$k]['description']    = $contact_ids['description'];
          $contactNameDetails[$k]['contact'] = civicrm_api3('Contact', 'get', array('sequential' => 1,'id' => $contact_ids['contact_id_a'], 'return' => array("first_name", "last_name", "tag")));
        }
      }

      // Get only FIRST/LAST name of contact
      foreach( $contactNameDetails  as $key => $cname ) {
        if(!empty($cname['contact']['values'][0])) {
          $cid = $cname['contact']['id'];
          $contactFullName[$cid]['tags'] = $cname['contact']['values'][0]['tags'];
          $contactFullName[$cid]['name'] = $cname['contact']['values'][0]['first_name'] . " " . $cname['contact']['values'][0]['last_name'];
        }
      }

      $returnResult['related_contacts'] = $contactFullName;

      // Get the CiviCRM Platform
      $returnResult['civicrm_platform'] = $contact_details['values'][0]['custom_63'][0];

      // Get the Service on CMS
      $returnResult['service_cms'] = $contact_details['values'][0]['custom_256'];

      // Prepare return value array with tag and employee
      $result = civicrm_api3_create_success($returnResult, $params, 'cividesk', 'Getaccountdetails');
      return $result;
    } else {
      $result = 0;
      return $result;
    }
  }
  else {
    throw new API_Exception('Domain Name is required field', 404);
  }
}

