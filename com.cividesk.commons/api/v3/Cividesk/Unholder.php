<?php

/**
 * Cividesk.Unholder API
 *
 * This API unholds emails that may have been put on hold in error
 * It selects these email addresses based on:
 * - ratio of successful sends per bounce (> 10 by default)
 * - number of addresses to unhold per call (100 by default)
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_cividesk_Unholder($params) {

  $ratio = CRM_Utils_Array::value('ratio', $params, 10);
  $limit = CRM_Utils_Array::value('limit', $params, 100);

  $emails = CRM_Core_DAO::executeQuery("
SELECT c.display_name, q.email_id, e.email,
  COUNT(s.id) AS success_sends, LEFT(MAX(s.time_stamp), 10) AS last_success,
  COUNT(b.id) AS bounced_sends, LEFT(MAX(b.time_stamp), 10) AS last_bounce,
  COUNT(s.id) / COUNT(b.id) AS ratio
  FROM civicrm_mailing_event_queue q
       LEFT JOIN civicrm_mailing_event_bounce b ON b.event_queue_id = q.id
       LEFT JOIN civicrm_mailing_event_delivered s ON s.event_queue_id = q.id
       LEFT JOIN civicrm_contact c ON c.id = q.contact_id
       LEFT JOIN civicrm_email e ON e.id = q.email_id
 WHERE e.on_hold = 1
 GROUP BY q.email_id
HAVING (bounced_sends < 5) AND (last_bounce < DATE_SUB(CURRENT_DATE(), INTERVAL 30 day)) AND (ratio > $ratio)
 ORDER BY ratio DESC
 LIMIT $limit
  ")->fetchAll();

  foreach($emails as &$mail) {
    $result = civicrm_api3('Email', 'create', [
      'id' => $mail['email_id'],
      'on_hold' => 0,
      // No need to specify reset_date as automatically calculated and added by BAO
    ]);
    $mail['changed'] = ($result['is_error'] == '0' ? 'Yes' : $result['error_message']);
  }

  return civicrm_api3_create_success($emails, $params, 'Email', 'Unholder');
}
