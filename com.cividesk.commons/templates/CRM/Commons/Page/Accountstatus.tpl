{* Display of Account Managers *}
{* Id:1 = Maintenance and support included *}
{* Id:2 = Maintenance included, support per hour *}
{* Id:3 = Not Included *}
{* Id:4 = Per hour *}
{* Id:5 = N/A *}

{literal}
<style type="text/css">
  .page-civicrm-civideskaccount #crm-container #crm-main-content-wrapper {
    background-color: #fff;
  }
</style>
{/literal}
<div id="main_content">
    <table>
        <tr>
            <td>
                <h3>Cividesk Contacts</h3>
                <div id="mngsub_1"></div>
                <table>
                    {if !empty($account_managers)}
                       <tr style="background-color: #FFFFCC;"><td><strong>Account Manager</strong></td></tr>
                        {foreach from=$account_managers key=mcid item=manager}
                            <tr>
                                <td>
                                    <div style="float: left;padding:1px 10px 15px 1px;"><img width="100px" height="100px" src="http://cividesk.com/sites/cividesk.com/files/{$manager.profile_img}"> </img> </div>
                                    <div style="padding-bottom: 5px;"><h3 style="position: inherit; padding: 5px; font-size: 15px;">{$manager.full_name}</h3></div>
                                    <div style="padding-bottom: 5px;"><a href="mailto:{$manager.email}">{$manager.email}</a></div>
                                    <div style="padding-bottom: 5px;">{$manager.phone}</div>
				                    <div style="padding-bottom: 5px;margin-top: 45px; color:gray; font-size:12px;">Contact me if you have questions about Cividesk services such as training, mentoring and process consulting, if you wish to add or remove authorized users, if you are interested in new features, customization, or making changes in your CiviCRM configuration.</div>
                                </td>
                            </tr>
                        {/foreach}
                       <tr style="background-color: #FFFFCC;"><td> <strong>Customer Support & Project Manager</strong></td></tr>
                        {foreach from=$support_managers key=macid item=csmanager}
                            <tr>
                                <td>
                                    <div style="float: left;padding:1px 10px 15px 1px;"><img width="100px" height="100px" src="http://cividesk.com/sites/cividesk.com/files/{$csmanager.profile_img}"> </img> </div>
                                    <div style="padding-bottom: 5px;"><h3 style="position: inherit; padding: 5px; font-size: 15px;">{$csmanager.full_name}</h3></div>
                                    <div style="padding-bottom: 5px;"><a href="mailto:{$csmanager.email}">{$csmanager.email}</a></div>
                                    <div style="padding-bottom: 5px;">{$csmanager.phone}</div>
				                    <div style="padding-bottom: 5px; margin-top: 45px; color:gray; font-size:12px;">Contact me if you have questions about how to use CiviCRM or if you need troubleshooting.</div>
                                </td>
                            </tr>
                        {/foreach}
                    {else}
                        <tr><td> No Records Found !!!</td></tr>
                    {/if}
                </table>
                <br/>
                <h3>Authorized Users</h3>
                <div id="mngsub_2" style="color:gray; font-size:12px;">Only Full Seat Users are authorized to contact our customer support.</div>
                <table>
                    <tr>
                        <th>Contact</th>
                        <th>Status</th>
                    </tr>
                    {if !empty($related_contacts)}
                        {foreach from=$related_contacts key=cid item=contacts}
                        <tr>
                            <td> {$contacts.name} </td>
                            <td> {$contacts.tags} </td>
                        </tr>
                        {/foreach}
                    {else}
                        <tr><td colspan="2"> No Records Found !!! </td></tr>
                    {/if}
                </table>
            </td>
            <td width="66%">
                <h3>My Cividesk Subscription</h3>
                <div id="mngsub_1"></div>
                <table>
                    {if !empty($civi_extensions_list) || !empty($non_civi_extensions_list)}
                        <tr>
                            <td width="50%" style="background-color: #FFFFCC;"><strong>Included</strong></td>
                            <td width="50%" style="background-color: #FFFFCC;"><strong>Billable work</strong></td>
                        </tr>
                        <tr>
                            <td style="border-right: 1px solid #efefef;">
                               <ul>
                                  <li><strong>Infrastructure and system maintenance</strong></li>
                                  <li><strong>CiviCRM security updates and software upgrades</strong></li>

                                   {if !empty($civicrm_platform) && !empty($support_on_cms) }
                                       {if $civicrm_platform eq 'Drupal' && $support_on_cms eq 1}
                                           <li><strong>Drupal security updates</strong></li>
                                           <li><strong>Unlimited support on Drupal</strong></li>
                                       {/if}

                                       {if $civicrm_platform eq 'Drupal' && $support_on_cms eq 4}
                                           <li><strong>Drupal security updates</strong></li>
                                       {/if}

                                       {if $civicrm_platform eq 'WordPress' && $support_on_cms eq 1}
                                           <li><strong>WordPress security updates and software upgrades</strong></li>
                                           <li><strong>Unlimited support on WordPress</strong></li>
                                       {/if}

                                       {if $civicrm_platform eq 'WordPress' && $support_on_cms eq 4}
                                           <li><strong>WordPress security updates and software upgrades</strong></li>
                                       {/if}

                                       {if $civicrm_platform eq 'Joomla' && $support_on_cms eq 1}
                                           <li><strong>Joomla security updates and software upgrades</strong></li>
                                           <li><strong>Unlimited support on Joomla</strong></li>
                                       {/if}

                                       {if $civicrm_platform eq 'Joomla' && $support_on_cms eq 4}
                                           <li><strong>Joomla security updates and software upgrades</strong></li>
                                       {/if}
                                   {/if}
                                  <li><strong>Unlimited support on CiviCRM (except CiviCase and Accounting integration) and the following extensions:</strong></li>
                                  <div style="line-height: 25px; margin-top:10px;">
                                    {foreach from=$civi_extensions_list key=isCivi item=cextension}
                                      - {$cextension.name}</br>
                                    {/foreach}
                                  </div>
                               </ul>
                            </td>
                            <td>
                               <ul><strong>
                                  <li>Bug fixes</li>
                                  <li>Custom developments</li>
                                       {if !empty($civicrm_platform) && !empty($support_on_cms) }
                                           {if $civicrm_platform eq 'Drupal' && $support_on_cms eq 4}
                                               <li><strong>Support on Drupal</strong></li>
                                           {/if}

                                           {if $civicrm_platform eq 'WordPress' && $support_on_cms eq 4}
                                               <li><strong>Support on WordPress</strong></li>
                                           {/if}

                                           {if $civicrm_platform eq 'Joomla' && $support_on_cms eq 4}
                                               <li><strong>Support on Joomla</strong></li>
                                           {/if}
                                       {/if}
                                  <li>Support on CiviCase and Accounting integration</li>
                                  <li>Support on the following extensions:</li>
                                  </strong>
                                  <div style="line-height: 25px; margin-top:10px;">
                                     {foreach from=$non_civi_extensions_list key=isCivi item=ncextension}
                                        - {$ncextension.name}</br>
                                     {/foreach}
                                  </div>
                               </ul>
                            </td>
                        </tr>
                    {else}
                        <tr><td colspan="2"> No Records Found !!! </td></tr>
                    {/if}

                </table>
            </td>
        </tr>
    </table>
 </div>
