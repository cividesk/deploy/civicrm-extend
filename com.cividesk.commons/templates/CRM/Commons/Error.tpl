<div class="messages status no-popup">  <i class="crm-i fa-exclamation-triangle crm-i-red"></i>
  <span class="status-fatal">{ts}<b>Access is restricted to CiviDesk users only</b>{/ts}</span>
  <div class="crm-section crm-error-message">{$message}</div>
    <div class="crm-section crm-error-message">{ts}Contact CiviDesk for assistance.{/ts}</div>
  <p><a href="{$config->userFrameworkBaseURL}" title="{ts}Main Menu{/ts}">{ts}Return to home page.{/ts}</a></p>
</div>
