<?php
class CRM_Commons_Page_Accountstatus extends CRM_Core_Page {

  public function run() {
    // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
    CRM_Utils_System::setTitle(ts('My Account'));
    require_once 'api/class.api.php';

    $api = new civicrm_api3( array(
        'server' => 'https://www.cividesk.com',
        'api_key' => 'rUCNlUofzQIR',
        'key' => 'lkjhw34567o8osdfew5qsx',
      )
    );

    $plain_domain = getenv('DOMAIN');

    // Get the Current Domain
    if ($api->Cividesk->Getaccountdetails(array('domain_name' => $plain_domain ))) {
      $related_contacts_arr = array();
      $account_manager_arr = array();
      $extensions_arr = array();
      $civicrm_version ='';

      if(is_array($api->lastResult->values)) {
        $account_managers = $api->lastResult->values[0];
        $related_contacts = $api->lastResult->values[1];
        $civicrm_platform = $api->lastResult->values[2];
        $support_on_cms   = $api->lastResult->values[3];

        foreach ($related_contacts as $key => $related_contact) {
          // Renamed the Tag name as per mentioned in task : https://projects.cividesk.com/projects/3/tasks/1921
         if(!empty($related_contact->tags) &&
            (in_array('Full User', explode(',', $related_contact->tags)) ||
             in_array('Half User', explode(',', $related_contact->tags)) ||
             in_array('R/O User', explode(',', $related_contact->tags)))
           ) {
           if (in_array('R/O User', explode(',', $related_contact->tags))) {
             $status_name = 'Read Only User';
             $tag_id      = 3; // This need to display last
           } else if (in_array('Half User', explode(',', $related_contact->tags))) {
             $status_name = 'Half Seat User';
             $tag_id      = 2; // This need to display Middle
           } else if (in_array('Full User', explode(',', $related_contact->tags))) {
             $status_name = 'Full Seat User';
             $tag_id      = 1; // This need to display First
           }
           // Extract the last name so as to use this last name for sorting
           list($firstname, $middlename, $lastname) = explode(' ', $related_contact->name);
           $related_contacts_arr[$key]['cid']   = $key;
           $related_contacts_arr[$key]['lname'] = (isset($middlename) && isset($lastname)) ? $lastname : $middlename;
           $related_contacts_arr[$key]['name']  = $related_contact->name;
           $related_contacts_arr[$key]['tag_id']= $tag_id;
           $related_contacts_arr[$key]['tags']  = $status_name;
         }
        }

        // Sorting of values by Status Name & then by Last name
        $tag_col   = array_column($related_contacts_arr, 'tag_id');
        $lname_col = array_column($related_contacts_arr, 'lname');
        array_multisort($tag_col, SORT_ASC, $lname_col, SORT_ASC, $related_contacts_arr);

        $manager_details = array(
          "Tim" => array(
            'full_name' => 'Timothée Colmet Daâge',
            'email' => 'support@cividesk.com',
            'phone' => '+1 720-381-3939 x 103',
            'profile_img' => 'Timothee%20headshot.JPG'
          ),
          "Nicolas" => array(
            'full_name' => 'Nicolas Ganivet',
            'email' => 'nicolas@cividesk.com',
            'phone' => '+1 720-381-3939 x 102',
            'profile_img' => 'Nicolas_headshot_2.jpg'
          ),
          "Virginie" => array(
            'full_name' => 'Virginie Ganivet',
            'email' => 'virginie@cividesk.com',
            'phone' => '+1 720-381-3939 x 101',
            'profile_img' => 'Profile_Vivie.jpg'
          ),
          "Tamar" => array(
            'full_name' => 'Tamar Meir',
            'email' => 'support@cividesk.com',
            'phone' => '+1 720-381-3939 x 3',
            'profile_img' => 'tamar_201709_small_1.jpg'
          ),
        );

        foreach ($account_managers as $k => $v) {
          $account_manager_arr[] = $manager_details[$v];
        }

        $support_manager_arr[] = $manager_details['Tim'];

        // Get the list of Enabled Extension details for the customers ///////////////
        $extensionsList = civicrm_api3('Extension', 'get', array(
          'sequential' => 1,
          'is_active' => 1,
          'options' => array('limit' => 0),
          'return' => array("key", "name", "description", "status"),
        ));

        $extensions = array();
        if(!empty($extensionsList['values'])) {
          $extensions = $extensionsList['values'];
        }
        $extensions_details = $extensions;

        // Get the CiviCRM version Installed //////////////////////
        $version = civicrm_api3('Domain', 'get', array(
          'return' => array("version"),
        ));
        $civicrm_version = $version['values'][1]['version'];

        //  Third party Extensions to remove as Billable & display on to Included
        $civiExt = array("Mailchimp", "Mosaico", "Event Calendar", "CiviDiscount", "iATS Payments", "CiviCRM Bootstrap theme", "CiviCustom Kairos Canada", 'AirMail', 'Recent Items Menu');

        // Excluded Extension list to NOT to display anywhere
        $excludedExt = array("Cividesk Commons", "CiviCRM Report Error", "FlexMailer", "E-mail API", "Angular Profiles", "Sequential credit notes");

        foreach ($extensions_details as $k => $extensions) {
          // Check only for Installed/Active Extensions
          if(!in_array($extensions['name'], $excludedExt)) {
            if ($extensions['status'] == "installed") {
              if ((strpos($extensions['key'], "com.cividesk") !== false) || in_array($extensions['name'], $civiExt)) {
                $extensions_arr['cividesk_ext'][$k]['name'] = $extensions['name'];
                $extensions_arr['cividesk_ext'][$k]['description'] = $extensions['description'];
                $extensions_arr['cividesk_ext'][$k]['key'] = $extensions['key'];
              } else {
                $extensions_arr['non_cividesk_ext'][$k]['name'] = $extensions['name'];
                $extensions_arr['non_cividesk_ext'][$k]['description'] = $extensions['description'];
                $extensions_arr['non_cividesk_ext'][$k]['key'] = $extensions['key'];
              }
            }
          }
        }
      }

      $this->assign('civi_extensions_list', $extensions_arr['cividesk_ext']);
      $this->assign('non_civi_extensions_list', $extensions_arr['non_cividesk_ext']);
      $this->assign('civi_version', $civicrm_version);
      $this->assign('account_managers', $account_manager_arr);
      $this->assign('support_managers', $support_manager_arr);
      $this->assign('related_contacts', $related_contacts_arr);
      $this->assign('civicrm_platform', $civicrm_platform);
      $this->assign('support_on_cms', $support_on_cms);

    } else {
      echo $api->errorMsg();
    }

    parent::run();
  }
}
