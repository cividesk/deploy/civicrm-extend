<?php

class CRM_Commons_Page_Help  extends CRM_Core_Page {

  function run() {
    $base_dir = (is_dir('/opt/sso') ? '/opt/sso' : '/var/www/html/sso');
    require_once "$base_dir/jwt/JWT.php";
    require_once "$base_dir/customer.php";
    $config =& CRM_Core_Config::singleton();
    $session = CRM_Core_Session::singleton();
    $url_data = parse_url(CIVICRM_UF_BASEURL);
    $url = isset($url_data['host']) ?  $url_data['host'] : $url_data['path'];

    if (CRM_Utils_System::isUserLoggedIn()) {
      $email = CRM_Contact_BAO_Contact::getPrimaryEmail($session->get('userID'));
      if ($email) {
        $type     = CRM_Utils_Request::retrieve('r', 'String', CRM_Core_DAO::$_nullObject);
        if (!empty($_SERVER['DOMAIN']) && ($type == 'sso/index.php')) {
          $domain = $domains[0]['domain'];
          $redirectUrl = 'https://'. $_SERVER['DOMAIN'] . '.cividesk.com/' . $type;
          $loginUrl    = 'https://'. $_SERVER['DOMAIN'] . '.cividesk.com/sso/login.php?redirect='. urlencode($redirectUrl);
        } else {
          $loginUrl = urldecode($type);
        }

        if ($loginUrl) {
          $token = array(
            "exp" => time() + 60,
            "email" => $email
          );
          $jwt = JWT::encode($token, CIVIDESK_JWT_KEY);
          $sep = (strpos($loginUrl, '?') ? '&' : '?');
          $loginUrl = $loginUrl . $sep . 'jwt=' .$jwt;
          header("Location: $loginUrl");
        }
      }
    }

/* Not quite clear why this is here ... check Standalone Plus ...
    if (! $is_success) {
      $this->redirect();
    }
*/
  }

  function redirect() {
    $url = CRM_Utils_System::url('civicrm/dashboard', 'reset=1');
    $statusMessage = "Not able to redirect to Support Center";
    CRM_Core_Error::statusBounce($statusMessage, $url);
  }
}
