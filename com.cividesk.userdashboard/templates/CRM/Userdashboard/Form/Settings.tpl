{* template for a single row *}
{if $soInstance}
  {if $rowType eq 'default'}
    {* Add related default element *}
    <tr class= "user-dashboard-row-default {if $soInstance % 2}odd{else}even{/if}-row" data-row="{$soInstance}"
    xmlns="http://www.w3.org/1999/html">
      <td>{$form.default_profile[$soInstance].html}</td>
    </tr>
  {/if}
  {if $rowType eq 'related'}
    {* Add related profile element *}
    <tr class= "user-dashboard-row-related {if $soInstance % 2}odd{else}even{/if}-row" data-row="{$soInstance}"
    xmlns="http://www.w3.org/1999/html">
      <td>{$form.default_profile_related[$soInstance].html}</td>
    </tr>
  {/if}

{else}
{* this template is used for adding/editing userdashboard overrides  *}
<div class="crm-form crm-form-block crm-userdashboard-form-block">
  <div class="crm-submit-buttons">
    {include file="CRM/common/formButtons.tpl" location='top'}
  </div>
   <table class="form-layout-compressed">
     <tr>
       <td>
         <table class= "user-dashboard-table-default row-highlight">
           <thead>
              <tr class= "columnheader">
                <th>{ts}{$form.default_profile.1.label}{/ts}</th>
              </tr>
            </thead>
            <tbody>
              {section name="numStringsDefaults" start=1 step=1 loop=$numStringsDefaults+1}
                {include file="CRM/Userdashboard/Form/Settings.tpl" soInstance=$smarty.section.numStringsDefaults.index rowType="default"}
              {/section}
            </tbody>
        </table>
        &nbsp;&nbsp;&nbsp;<a class="action-item crm-hover-button buildUserDashboardRow" href="#" data-record-type="default"><i class="crm-i fa-plus-circle"></i> {ts}Add row{/ts}</a>
      </td>
    </tr>

    <tr>
      <td>
        <table class= "user-dashboard-table-related row-highlight">
          <thead>
             <tr>
               <td>{ts}{$form.userdashboard_related_enable_link.html}{/ts} {ts}{$form.userdashboard_related_enable_link.label}{/ts}</td>
             </tr>
             <tr>
               <td>{ts}{$form.remove_renew_link.html}{/ts} {ts}{$form.remove_renew_link.label}{/ts}</td>
             </tr>
             <tr>
               <td>{ts}{$form.remove_renew_link_non_current.html}{/ts} {ts}{$form.remove_renew_link_non_current.label}{/ts}</td>
             </tr>
             <tr>
               <td>{ts}{$form.pre_text.label}{/ts} {ts}{$form.pre_text.html}{/ts}</td>
             </tr>
             <tr class= "columnheader">
               <th>{ts}{$form.default_profile_related.1.label}{/ts}</th>
             </tr>
           </thead>
           <tbody>
             {section name="numStringsDefaultsRelated" start=1 step=1 loop=$numStringsDefaultsRelated+1}
               {include file="CRM/Userdashboard/Form/Settings.tpl" soInstance=$smarty.section.numStringsDefaultsRelated.index rowType="related"}
             {/section}
          </tbody>
     </table>
       &nbsp;&nbsp;&nbsp;<a class="action-item crm-hover-button buildUserDashboardRow" href="#" data-record-type="related"><i class="crm-i fa-plus-circle"></i> {ts}Add row{/ts}</a>
     </td>
   </tr>
  </table>
  <div class="crm-submit-buttons">
    {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>

</div>
{literal}
<script type="text/javascript">
  CRM.$(function($) {
    {/literal}
    {if $userDashboardInstances}
      {* Rebuild necessary rows in case of form error *}
      {foreach from=$userDashboardInstances key="index" item="instance"}
        buildUserDashboardRow( {$instance}, 'all' );
      {/foreach}
    {/if}
    {literal}

    function buildUserDashboardRow( curInstance, rowType ) {
      var newRowNum;

      if (curInstance) {
        if ($('tr.user-dashboard-row-' + rowType + '[data-row=' + curInstance + ']').length) {
          return;
        }
        newRowNum = curInstance;
      } else {
        newRowNum = 1 + $('tr.user-dashboard-row-' + rowType + ':last').data('row');
      }
      var dataUrl = {/literal}"{crmURL q='snippet=4' h=0}"{literal};
      dataUrl += "&rowType="+rowType+"&instance="+newRowNum;

      $.ajax({
        url: dataUrl,
        async: false,
        success: function(html) {
          $('.user-dashboard-table-' + rowType + ' tbody').append(html);
          $('tr.user-dashboard-row-' + rowType + ':last').trigger('crmLoad');
        }
      });
    }

    $('.buildUserDashboardRow').click(function(e) {
      var rowType = $(this).attr('data-record-type');
      buildUserDashboardRow(false, rowType);
      e.preventDefault();
    });
  });
</script>
{/literal}
{/if}
