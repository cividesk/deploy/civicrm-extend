<?php

/**
 * This class generates form components for the Userdashboard settings form
 *
 */
class CRM_Userdashboard_Form_Settings extends CRM_Admin_Form_Setting {
  protected $_numStringsDefaults = 1;
  protected $_numStringsDefaultsRelated = 1;

  /**
   * Pre process function.
   */
  public function preProcess() {
    $this->_soInstance = CRM_Utils_Array::value('instance', $_GET);
    $this->_rowType = CRM_Utils_Array::value('rowType', $_GET);
  }

  /**
   * Build the form object.
   *
   * @return void
   */
  public function buildQuickForm() {
    $settings = Civi::settings()->get('USERDASHBOARD_EXTENSION_SETTINGS') ? Civi::settings()->get('USERDASHBOARD_EXTENSION_SETTINGS') : array();
    $instances = 0;

    if (!empty($settings)) {
      $instances = count($settings['default_profile']) + count($settings['default_profile_related']);
    }

    if ($instances > 2) {
      $this->_numStringsDefaults = count($settings['default_profile']);
      $this->_numStringsDefaultsRelated = count($settings['default_profile_related']);
    }

    $soInstances = range(1, $instances, 1);
    $userDashboardInstances = array();

    if ($this->_soInstance) {
      $soInstances = array($this->_soInstance);
    }
    elseif (!empty($_POST['old'])) {
      $soInstances = $userDashboardInstances = array_keys($_POST['old']);
    }
    elseif (!empty($this->_defaults) && is_array($this->_defaults)) {
      $userDashboardInstances = array_keys($this->_defaults['new']);
      if (count($this->_defaults['old']) > count($this->_defaults['new'])) {
        $userDashboardInstances = array_keys($this->_defaults['old']);
      }
    }

    $contactTypes = CRM_Contact_BAO_ContactType::getSelectElements(FALSE, FALSE);
    $sel1 = array('' => '- select -') + $contactTypes;
    foreach ($contactTypes as $key => $value) {
      if ($key == 'Individual') {
        $profileTypes = array('Contact', 'Individual');
      }
      elseif ($key == 'Household') {
        $profileTypes = array('Contact', 'Household');
      }
      elseif ($key == 'Organization') {
        $profileTypes = array('Contact', 'Organization');
      }
      else {
        $basictype = CRM_Contact_BAO_ContactType::getBasicType($key);
        if ($basictype == 'Individual') {
          $profileTypes = array('Contact', 'Individual', $key);
        }
        elseif ($key == 'Household') {
          $profileTypes = array('Contact', 'Household', $key);
        }
        elseif ($key == 'Organization') {
          $profileTypes = array('Contact', 'Organization', $key);
        }
      }
      $sel2[$key] = CRM_Core_BAO_UFGroup::getProfiles($profileTypes);
    }

    foreach ($soInstances as $instance) {
      if (empty($this->_rowType)) {
        $this->_rowType = 'all';
        // Add all elements.
        $sel = &$this->addElement('hierselect', "default_profile[$instance]", ts('Profile for userdashboard contact'));
        $sel->setOptions(array($sel1, $sel2));

        $sel_related = &$this->addElement('hierselect', "default_profile_related[$instance]", ts('Profile for related contact'));
        $sel_related->setOptions(array($sel1, $sel2));
      }
      else {
        // Add element based on type.
        if ($this->_rowType == 'default') {
          $sel = &$this->addElement('hierselect', "default_profile[$instance]", ts('Default Profile'));
          $sel->setOptions(array($sel1, $sel2));
        }
        if ($this->_rowType == 'related') {
          $sel_related = &$this->addElement('hierselect', "default_profile_related[$instance]", ts('Related Profile'));
          $sel_related->setOptions(array($sel1, $sel2));
        }
        if ($this->_rowType == 'all') {
          // Add all elements.
          $sel = &$this->addElement('hierselect', "default_profile[$instance]", ts('Default Profile'));
          $sel->setOptions(array($sel1, $sel2));

          $sel_related = &$this->addElement('hierselect', "default_profile_related[$instance]", ts('Related Profile'));
          $sel_related->setOptions(array($sel1, $sel2));
        }
      }
    }

    $this->addElement('checkbox', 'userdashboard_related_enable_link', ts('Remove link for relationship and related contact?'));

    $this->addElement('checkbox', 'remove_renew_link', ts('Remove link for "Renew" for organizations?'));

    //https://projects.cividesk.com/projects/27/tasks/4635
    $this->addElement('checkbox', 'remove_renew_link_non_current', ts('Remove link for "Renew" for inactive memberships?'));

    $this->add('wysiwyg', 'pre_text', ts('Introduction text'));
    $this->assign('soInstance', $this->_soInstance);
    $this->assign('rowType', $this->_rowType);
    $this->assign('numStringsDefaults', $this->_numStringsDefaults);
    $this->assign('numStringsDefaultsRelated', $this->_numStringsDefaultsRelated);

    if ($this->_soInstance) {
      return;
    }
    $this->addButtons(array(
      array (
        'type' => 'submit',
        'name' => ts('Submit'),
        'isDefault' => TRUE,
      )
    ));

    $this->assign('elementNames', $this->getRenderableElementNames());
    $this->assign('userDashboardInstances', empty($userDashboardInstances) ? FALSE : $userDashboardInstances);
    parent::buildQuickForm();
  }

  /**
   * Set default values for the form.
   *
   * @return array
   *   default value for the fields on the form
   */
  public function setDefaultValues() {
    $settings = Civi::settings()->get('USERDASHBOARD_EXTENSION_SETTINGS');
    // Make sure that settings are available to avoid warning.
    if (!empty($settings)) {
      foreach ($settings as $key => $value) {
        if (!is_null($value)) {
          $this->_defaults[$key] = $value;
        }
      }
    }

    $this->_defaults['userdashboard_related_enable_link'] = Civi::settings()->get('userdashboard_related_enable_link');
    $this->_defaults['remove_renew_link'] = Civi::settings()->get('remove_renew_link');
    $this->_defaults['remove_renew_link_non_current'] = Civi::settings()->get('remove_renew_link_non_current');

    $this->_defaults['pre_text'] = Civi::settings()->get('pre_text');
    return $this->_defaults;
  }

  /**
   * Process postProcess.
   *
   *
   * @return void
   */
  public function postProcess() {
    $formValues = $this->_submitValues;
    foreach ($formValues as $key => $value) {
      if (strpos($key, 'default_profile') !== false) {
        $userDashboard[$key] = $value;
      }
    }

    Civi::settings()->set('USERDASHBOARD_EXTENSION_SETTINGS', $userDashboard);

    $relatedSettings = isset($formValues['userdashboard_related_enable_link']) ? $formValues['userdashboard_related_enable_link'] : NULL;
    Civi::settings()->set('userdashboard_related_enable_link', $relatedSettings);

    $removeLink = isset($formValues['remove_renew_link']) ? $formValues['remove_renew_link'] : NULL;
    Civi::settings()->set('remove_renew_link', $removeLink);

    $removeLinkNonCurrent = isset($formValues['remove_renew_link_non_current']) ? $formValues['remove_renew_link_non_current'] : NULL;
    Civi::settings()->set('remove_renew_link_non_current', $removeLinkNonCurrent);

    $preText = isset($formValues['pre_text']) ? $formValues['pre_text'] : NULL;
    Civi::settings()->set('pre_text', $preText);
  }

  /**
   * Get the fields/elements defined in this form.
   *
   * @return array (string)
   */
  function getRenderableElementNames() {
    // The _elements list includes some items which should not be
    // auto-rendered in the loop -- such as "qfKey" and "buttons". These
    // items don't have labels. We'll identify renderable by filtering on
    // the 'label'.
    $elementNames = array();
    foreach ($this->_elements as $element) {
      $label = $element->getLabel();
      if (!empty($label)) {
        $elementNames[] = $element->getName();
      }
    }
    return $elementNames;
  }

}
