<?php

require_once 'userdashboard.civix.php';
const USERDASHBOARD_EXTENSION_SETTINGS = 'Userdashboard Extension Settings';

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function userdashboard_civicrm_config(&$config) {
  _userdashboard_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @param array $files
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function userdashboard_civicrm_xmlMenu(&$files) {
  _userdashboard_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function userdashboard_civicrm_install() {
  _userdashboard_civix_civicrm_install();
}

/**
* Implements hook_civicrm_postInstall().
*
* @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
*/
function userdashboard_civicrm_postInstall() {
  _userdashboard_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function userdashboard_civicrm_uninstall() {
  _userdashboard_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function userdashboard_civicrm_enable() {
  _userdashboard_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function userdashboard_civicrm_disable() {
  _userdashboard_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed
 *   Based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function userdashboard_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _userdashboard_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function userdashboard_civicrm_managed(&$entities) {
  _userdashboard_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * @param array $caseTypes
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function userdashboard_civicrm_caseTypes(&$caseTypes) {
  _userdashboard_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function userdashboard_civicrm_angularModules(&$angularModules) {
_userdashboard_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function userdashboard_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _userdashboard_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Functions below this ship commented out. Uncomment as required.
 *

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function userdashboard_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function userdashboard_civicrm_navigationMenu(&$menu) {
  _userdashboard_civix_insert_navigation_menu($menu, NULL, array(
    'label' => ts('The Page', array('domain' => 'com.cividesk.userdashboard')),
    'name' => 'the_page',
    'url' => 'civicrm/the-page',
    'permission' => 'access CiviReport,access CiviContribute',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _userdashboard_civix_navigationMenu($menu);
} // */

/**
 * Implementation of hook_civicrm_pageRun
 */
function userdashboard_civicrm_pageRun(&$page) {
  $pageName = $page->getVar('_name');
  if ($pageName == 'CRM_Contact_Page_View_UserDashBoard') {

    $preText = Civi::settings()->get('pre_text');
    if ($preText) {
      CRM_Core_Region::instance('form-top')->add(array(
        'markup' => "$preText",
      ));
    }
    $contactID = $page->getVar('_contactId');
    $type = CRM_Contact_BAO_Contact::getContactType($contactID);

    //https://projects.cividesk.com/projects/27/tasks/4635
    $removeLinkNonCurrent = Civi::settings()->get('remove_renew_link_non_current');
    if ($removeLinkNonCurrent == 1) {
      CRM_Core_Region::instance('page-body')->add(array(
        'script' => "
          cj('#renewInactiveLink').hide();
        ",
      ));
    }

    if ($type == 'Organization') {
      //Get dashboard settings.
      $removeLink = Civi::settings()->get('remove_renew_link');
      if ($removeLink == 1) {
        CRM_Core_Region::instance('page-body')->add(array(
          'script' => "
            cj('#renewActiveLink, #renewInactiveLink').hide();
          ",
        ));
      }
    }
    $name = CRM_Contact_BAO_Contact::displayName($contactID);
    $subtype = CRM_Contact_BAO_Contact::getContactSubType($contactID);
    $settings = Civi::settings()->get('USERDASHBOARD_EXTENSION_SETTINGS');
    $types = array();
    foreach ($settings as $names => $profileid) {
      if (strpos($names, '_related') == false) {
        foreach ($profileid as $key => $value) {
          if($value[0] &&  $value[1]) {
            $types[$value[0]] = $value[1];
          }
        }
      }
    }
    if (!empty($subtype)) {
      foreach ($subtype as $key => $value) {
        if( array_key_exists($value, $types) && $types[$value]) {
          $subtypeProfile = $subtype[$key];
        }
      }
    }
    $profileType = !empty($subtypeProfile)? $subtypeProfile : $type;
    $gid = $types[$profileType];

    if ($gid) {
      $url = CRM_Utils_System::url('civicrm/profile/edit', "reset=1&id={$contactID}&gid={$gid}");
      //translation fixes for dashboard
      CRM_Utils_System::setTitle(ts('Dashboard') . " - <a href= $url><i style='color: #5589b7' class='crm-i fa-pencil'></i> $name</a>");
    }
  }
}

/**
 * Implementation of hook_civicrm_searchColumns
 */
function userdashboard_civicrm_searchColumns($objectName, &$headers, &$rows, &$selector) {
  //Get dashboard settings.
  $disableLinks = Civi::settings()->get('userdashboard_related_enable_link');
  if ($disableLinks == 1) {
    if ($objectName == 'relationship.rows.user') {
      foreach($rows as $rowID => $row) {
	// show only text for contact and relationship
	// on user dashboard instead of links
        $rows[$rowID]['sort_name'] = strip_tags($row['sort_name']);
        $rows[$rowID]['relation'] = strip_tags($row['relation']);
      }
    }
  }
}

/**
 * Implementation of hook_civicrm_alterAdminPanel
 */
function userdashboard_civicrm_alterAdminPanel(&$adminPanel) {
  $adminPanel['Customize_Data_and_Screens']['fields']['{weight}.UserDashboard']['title'] = ts('User Dashboard Settings');
  $adminPanel['Customize_Data_and_Screens']['fields']['{weight}.UserDashboard']['url'] =  CRM_Utils_System::url('civicrm/userdashboard/settings' , 'reset=1');
  $adminPanel['Customize_Data_and_Screens']['fields']['{weight}.UserDashboard']['desc'] = ts('User Dashboard Configuration Settings');
}

/**
 * Implementation of hook_civicrm_links
 */
function userdashboard_civicrm_links($op, $objectName, $objectId, &$links, &$mask, &$values) {
  switch ($objectName) {
    case 'Relationship':
      switch ($op) {
        case 'relationship.selector.row':
          if (isset($values['cbid'])) {
            $type = CRM_Contact_BAO_Contact::getContactType($values['cbid']);
            $subtype = CRM_Contact_BAO_Contact::getContactSubType($values['cbid']);
            $settings = Civi::settings()->get('USERDASHBOARD_EXTENSION_SETTINGS');
            $types = array();
            foreach ($settings as $names => $profileid) {
              if (strpos($names, '_related') == true) {
                foreach ($profileid as $key => $value) {
                  if($value[0] && $value[1]) {
                    $types[$value[0]] = $value[1];
                  }
                }
              }
            }
            if (!empty($subtype)) {
              foreach ($subtype as $key => $value) {
                if( array_key_exists($value, $types) && $types[$value]) {
                  $subtypeProfile = $subtype[$key];
                }
              }
            }

            $profileType = !empty($subtypeProfile)? $subtypeProfile : $type;
            $gid = $types[$profileType];

            if ($subtype[0] == 'Sponsorchild') {
              //FIXME
              $gid = 22;
            }

            if ($gid) {
              foreach ($links as $key => $link) {
                if ($link['url'] == 'civicrm/contact/relatedcontact') {
                  $links[$key]['url'] = 'civicrm/profile/edit';
                  $links[$key]['qs'] = "reset=1&id=%%cbid%%&gid={$gid}";
                }
              }
            }
          }
          break;
      }
  }
}
