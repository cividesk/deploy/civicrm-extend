{crmScope extensionKey='membershipreport'}
{if $reportaudit_headers}
  <table>
    <thead>
      <tr>
        {foreach from=$reportaudit_headers item=h}
          <th style="text-align: center;">{$h}</th>
        {/foreach}
      </tr>
    <thead>
    <tbody>
      {foreach from=$reportaudit_rows item=row}
        <tr>
          {foreach from=$row key=key item=v}
            <td style="text-align: {$reportaudit_align.$key}">{$v}</td>
          {/foreach}
        </tr>
      {/foreach}
    </tbody>
  </table>
{else}
  <p>{ts}No results found.{/ts}
{/if}
{/crmScope}
