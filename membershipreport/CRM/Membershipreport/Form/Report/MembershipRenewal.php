<?php

use CRM_Membershipreport_ExtensionUtil as E;

class CRM_Membershipreport_Form_Report_MembershipRenewal extends CRM_Report_Form {

  protected $primary_membership_type_ids = [];

  function __construct() {
    parent::__construct();

    $this->_columns = [
      'civicrm_membership' => [
        'dao' => 'CRM_Member_DAO_Membership',
        'fields' => [
          'membership_type' => [
            'title' => E::ts('Membership Type'),
            'type' => CRM_Utils_Type::T_STRING,
            'pseudofield' => true,
            'default' => 1,
          ],
          'preferred_language' => [
            'title' => E::ts('Preferred Language'),
            'type' => CRM_Utils_Type::T_STRING,
            'pseudofield' => true,
          ],
          'primary_membership_type' => [
            'title' => E::ts('Primary Membership Type'),
            'type' => CRM_Utils_Type::T_STRING,
            'pseudofield' => true,
          ],
          'new' => [
            'title' => E::ts('New members'),
            'type' => CRM_Utils_Type::T_INT,
            'pseudofield' => true,
            'default' => 1,
            'function' => 'calculateNew',
          ],
          'lapsed' => [
            'title' => E::ts('New (Lapsed)'),
            'type' => CRM_Utils_Type::T_INT,
            'pseudofield' => true,
            'default' => 1,
            'function' => 'calculateLapsed',
          ],
          'renewal' => [
            'title' => E::ts('Renewal (Same)'),
            'type' => CRM_Utils_Type::T_INT,
            'pseudofield' => true,
            'default' => 1,
            'function' => 'calculateRenewalSame',
          ],
          'changed' => [
            'title' => E::ts('Renewal (Changed)'),
            'type' => CRM_Utils_Type::T_INT,
            'pseudofield' => true,
            'default' => 1,
            'function' => 'calculateRenewalChanged',
          ],
          'grace' => [
            'title' => E::ts('Grace'),
            'type' => CRM_Utils_Type::T_INT,
            'pseudofield' => true,
            'default' => 1,
            'function' => 'calculateGraceMembers',
          ],
          'total' => [
            'title' => E::ts('Total Active Members'),
            'type' => CRM_Utils_Type::T_INT,
            'pseudofield' => true,
            'default' => 1,
            'function' => 'calculateTotalActiveMembers',
          ],
          'missing1' => [
            'title' => E::ts('Not in Active'),
            'type' => CRM_Utils_Type::T_INT,
            'pseudofield' => true,
            'default' => 1,
            'function' => 'calculateMissingInActiveMembers',
          ],
          'missing2' => [
            'title' => E::ts('Not in Others'),
            'type' => CRM_Utils_Type::T_INT,
            'pseudofield' => true,
            'default' => 1,
            'function' => 'calculateMissingInSpecificColumns',
          ],
          'expired' => [
            'title' => E::ts('Expired'),
            'type' => CRM_Utils_Type::T_INT,
            'pseudofield' => true,
            'default' => 1,
            'function' => 'calculateExpiredMembers',
          ],
        ],
        'filters' => [
          'date' => [
            'title' => E::ts("Date"),
            'operatorType' => CRM_Report_Form::OP_DATE,
            'type' => CRM_Utils_Type::T_DATE,
          ],
          'membership_type' => [
            'title' => E::ts("Membership Type"),
            'operatorType' => CRM_Report_Form::OP_MULTISELECT,
            'type' => CRM_Utils_Type::T_INT,
            'options' => ['' => E::ts('- select -')] + $this->getMembershipTypes(),
          ],
        ],
      ],
    ];

    // This needs to be set otherwise beginPostProcess() freaks out
    // because we did not explicitely define any fields here.
    $this->_noFields = TRUE;

    // Used when displaying the primary membership type
    $options = [];

    $t = civicrm_api3('MembershipType', 'get', [
      'member_of_contact_id' => 1,
      'is_active' => 1,
      'options' => ['limit' => 0],
    ])['values'];

    foreach ($t as $key => $val) {
      $options[$key] = $val['name'];
    }

    $this->primary_membership_type_ids = $options;
  }

  /**
   * Defines the headers for the audit report (popup).
   *
   * @param $function
   *   Makes it possible to return different headers depending on the function.
   */
  public function getAuditHeaders($function, $params = []) {
    // Using 'ts' because these strings are in Core.
    $headers = [
      'id' => ts('ID'),
      'mlogid' => ts('Log ID'),
      'contact_id' => ts('Contact'),
      'join_date' => ts('Join Date'),
      'start_date' => ts('Start Date'),
      'end_date' => ts('End Date'),
      'status_id' => ts('Status ID'),
      'membership_type_id' => ts('Membership Type'),
    ];

    if (!empty($params['display_primary_membership_type'])) {
      $headers['primary_membership_type_id'] = E::ts('Primary Membership Type');
    }

    if (in_array($function, ['calculateRenewalSame', 'calculateRenewalChanged', 'calculateLapsed'])) {
      $headers['modified_date'] = E::ts('Modified Date');
      $headers['last_end_date'] = E::ts('Last End Date');
      $headers['last_modified_date'] = E::ts('Last Modified Date');
      $headers['last_status_id'] = E::ts('Last Status ID');
      $headers['last_membership_type_id'] = E::ts('Last Membership Type ID');
    }
    elseif ($function == 'calculateGraceMembers' || $function == 'calculateExpiredMembers') {
      $headers['modified_date'] = E::ts('Modified Date');
    }

    return $headers;
  }

  /**
   * Helper to fetch membership types.
   */
  function getMembershipTypes() {
    $mtypes = civicrm_api3('Membership', 'getoptions', [
      'field' => 'membership_type_id',
    ])['values'];

    return $mtypes;
  }

  /**
   * Returns all the memberships IDs of a contact.
   */
  function getPrimaryMembershipIDsForContact($contact_id) {
    $ids = [];

    $dao = CRM_Core_DAO::executeQuery('SELECT id FROM civicrm_membership WHERE contact_id = %1 AND membership_type_id IN (%2)', [
      1 => [$contact_id, 'Positive'],
      2 => [implode(',', array_keys($this->primary_membership_type_ids)), 'CommaSeparatedIntegers'],
    ]);

    while ($dao->fetch()) {
      $ids[] = $dao->id;
    }

    return $ids;
  }

  /**
   * Most of the processing happens here. We are not using the
   * usual Report functions (from, where, etc) because we are
   * running many separate queries, and also for reporting.
   */
  function postProcess() {
    $this->beginPostProcess();

    list($from, $to) = $this->getFromToDates('date');

    if (!$from || !$to) {
      CRM_Core_Session::setStatus(E::ts('Please enter a start and end date for the Date filter.'), '', 'error');
      return;
    }

    // Ensures that columnHeaders are built
    $this->select();

    // This is the CRM_Report_Form way to modify columnHeaders
    // Other reports might extend our class and implement it.
    $this->modifyColumnHeaders();

    $rows = [];

    // If no membership types were selected, default to All
    $mtypes_all = $this->getMembershipTypes();
    $mtypes_selected = [];

    if (!empty($this->_params['membership_type_value'])) {
      $mtypes_selected = $this->_params['membership_type_value'];
    }
    else {
      $mtypes_selected = array_keys($this->getMembershipTypes());
    }

    // Column types that have an impact on the "group by"
    // The dummies help simplify the code later on, to avoid having to manage two use-cases
    $dummy_groupby = [ 'field' => 'dummy', 'options' => [1 => '']];
    $groupby = $dummy_groupby;

    if (!empty($this->_params['fields']['preferred_language'])) {
      $options = civicrm_api3('Contact', 'getoptions', [
        'field' => 'preferred_language',
      ])['values'];

      $groupby = [
        'field' => 'preferred_language',
        'options' => $options,
      ];
    }

    if (!empty($this->_params['fields']['primary_membership_type'])) {
      // Allow only one groupby
      // and allow this option only if one membership type selected
      if (!empty($this->_params['fields']['preferred_language'])) {
        CRM_Core_Session::setStatus(E::ts("Please select either the Preferred Language or the Primary Membership Type. Enabling both is not supported at the moment."), '', 'error');
      }
      elseif (count($mtypes_selected) != 1) {
        CRM_Core_Session::setStatus(E::ts("The report can display a breakdown per primary membership type, but (for performance reasons) only if one membership subtype is selected. The information is still displayed in the popups."), '', 'warning');
      }
      else {
        $groupby = [
          'field' => 'primary_membership_type',
          'options' => $this->primary_membership_type_ids,
        ];
      }
    }

    foreach ($mtypes_selected as $membership_type_id) {
      $row = [];
      $row['civicrm_membership_membership_type'] = $mtypes_all[$membership_type_id];

      // If the membership type for this row is a "primary" membership type,
      // do not bother providing a breakdown for other memberships, since they
      // are usually mutually exclusive.
      // Ex: if we have Regular/Student memberships as primary, and divisions as
      // non-primary memberships (linked to another contact org), we should not
      // have Regular members who are also Student members.
      $groupby_options = $groupby['options'];

      if ($groupby['field'] == 'primary_membership_type' && isset($this->primary_membership_type_ids[$membership_type_id])) {
        $groupby_options = $dummy_groupby['options'];
      }

      foreach ($groupby_options as $groupby_key => $groupby_val) {
        $row['civicrm_membership_' . $groupby['field']] = $groupby_val;

        foreach ($this->_columnHeaders as $key => $col) {
          // A bit clunky, since it assumes only one entity table (civicrm_membership)
          $col = substr($key, strlen('civicrm_membership_'));
          if (empty($this->_columns['civicrm_membership']['fields'][$col]['function'])) {
            continue;
          }

          $function = $this->_columns['civicrm_membership']['fields'][$col]['function'];

          $params = [
            'membership_type_id' => $membership_type_id,
            'from' => $from,
            'to' => $to,
            $groupby['field'] => $groupby_key,
          ];

          if ($groupby['field'] != 'primary_membership_type' && !empty($this->_params['fields']['primary_membership_type'])) {
            $params['display_primary_membership_type'] = 1;
          }

          // Calculate the value for this cell
          $results = $this->{$function}($params);

          $args = [
            'reset' => 1,
            'class' => 'CRM_Membershipreport_Form_Report_MembershipRenewal',
            'function' => $function,
            'params' => json_encode($params),
          ];
          $row[$key] = '<a class="crm-popup" href="' . CRM_Utils_System::url('civicrm/reportaudit', $args) . '">' . count($results) . '</a>';
        }

        $rows[] = $row;
      }
    }

    $this->formatDisplay($rows);
    $this->doTemplateAssignment($rows);
    $this->endPostProcess($rows);
  }

  /**
   *
   */
  public function calculateNew($params) {
    $rows = [];

    // New members have a join_date that matches the payment date
    // but the start_date gets set to the 1st of the month, hence DATE_FORMAT.
    $sql = 'SELECT m.*, c.display_name, c.preferred_language
      FROM civicrm_membership m
      LEFT JOIN civicrm_contact c ON (c.id = m.contact_id)
      WHERE is_test = 0 AND status_id NOT IN (4,5,6)
        AND membership_type_id = %1
        AND join_date >= %2
        AND join_date <= %3
        AND DATE_FORMAT(join_date, "%Y-%m") = DATE_FORMAT(start_date, "%Y-%m")';

    $sql_params = [
      1 => [$params['membership_type_id'], 'Positive'],
      2 => [$params['from'], 'Timestamp'],
      3 => [$params['to'], 'Timestamp'],
    ];

    if (!empty($params['preferred_language'])) {
      $sql .= ' AND c.preferred_language = %4';
      $sql_params[4] = [$params['preferred_language'], 'String'];
    }

    // $this->addToDeveloperTab($sql);
    $dao = CRM_Core_DAO::executeQuery($sql, $sql_params);
    $rows = $this->copyDaoToResults($dao, $params);

    if (!empty($params['primary_membership_type']) || !empty($params['display_primary_membership_type'])) {
      $this->filterByPrimaryMembershipType($rows, $params, $params['primary_membership_type']);
    }

    $this->translateData($rows);
    return $rows;
  }

  /**
   * Returns membership renewals of members who have lapsed.
   * i.e. their membership had expired, and now they have renewed.
   */
  public function calculateLapsed($params) {
    $rows = [];
    $members = $this->calculateRenewalCommon($params, 'either');

    foreach ($members as $member) {
      // Find all 'primary' memberships, since they may have changed memberships, not lapsed
      $mids = $this->getPrimaryMembershipIDsForContact($member['contact_id']);

      // Check if the previous status of their membership was expired(4) or cancelled(6)
      $dao = CRM_Core_DAO::executeQuery('SELECT mlog.*
        FROM civicrm_membership_log mlog
        WHERE mlog.membership_id IN (%1)
          AND (mlog.status_id != %2 OR mlog.start_date != %3 OR mlog.end_date != %4)
          AND mlog.modified_date < %5
        ORDER BY mlog.modified_date DESC', [
        1 => [implode(',', $mids), 'CommaSeparatedIntegers'],
        2 => [$member['status_id'], 'Positive'],
        3 => [CRM_Utils_Date::isoToMysql($member['start_date']), 'Date'],
        4 => [CRM_Utils_Date::isoToMysql($member['end_date']), 'Date'],
        5 => [CRM_Utils_Date::isoToMysql($member['modified_date']), 'Date'],
      ]);

      if (!$dao->fetch()) {
        continue;
      }

      // 4=Expired, 6=Cancelled
      if (!in_array($dao->status_id, [4,6])) {
        continue;
      }

      $member['last_end_date'] = $dao->end_date;

      $rows[] = $member;
    }

    if (!empty($params['primary_membership_type']) || !empty($params['display_primary_membership_type'])) {
      $this->filterByPrimaryMembershipType($rows, $params, $params['primary_membership_type']);
    }

    $this->translateData($rows);
    $this->dedupeMemberships($rows);
    return $rows;
  }

  /**
   *
   */
  public function calculateRenewalSame($params) {
    $rows = $this->calculateRenewalCommon($params, 'same');
    $this->translateData($rows);
    $this->dedupeMemberships($rows);
    return $rows;
  }

  /**
   *
   */
  public function calculateRenewalChanged($params) {
    $rows = $this->calculateRenewalCommon($params, 'changed');
    $this->translateData($rows);
    $this->dedupeMemberships($rows);
    return $rows;
  }

  /**
   * Returns all membership renewals.
   *
   * This relies on the civicrm_membership_log. It looks for memberships
   * log entries that are now 'current' (and compares with the previous state),
   * since a renewal does not go through 'new'.
   *
   * Another way could be to use civicrm_activity, but I feel that the data
   * structure linking activities and memberships is clunky.
   *
   * Not relying on the payment, since there might not be one.
   *
   * Results are cached because it is called a few times.
   */
  public function calculateRenewalCommon($params, $same_or_change) {
    static $cache = [];
    $cacheKey = sha1(json_encode($params) . $same_or_change);

    if (isset($cache[$cacheKey])) {
      return $cache[$cacheKey];
    }

    // Maybe it should be configurable?
    $current_status_ids = [];
    $current_status_ids[] = CRM_Core_PseudoConstant::getKey('CRM_Member_BAO_Membership', 'status_id', 'Current');
    // @todo FIXME
    $current_status_ids[] = 8;

    $sql = 'SELECT m.id, m.contact_id, m.join_date, c.display_name, c.preferred_language,
        mlog.id as mlogid, mlog.status_id, mlog.start_date, mlog.end_date, mlog.modified_date,
        mlog.membership_id, mlog.membership_type_id
      FROM civicrm_membership_log mlog
      LEFT JOIN civicrm_membership m ON (m.id = mlog.membership_id)
      LEFT JOIN civicrm_contact c ON (c.id = m.contact_id)
      WHERE m.is_test = 0
        AND mlog.status_id IN (%1)
        AND mlog.membership_type_id = %2
        AND DATE_FORMAT(mlog.modified_date, "%Y%m%d") >= DATE_FORMAT(%3, "%Y%m%d")
        AND DATE_FORMAT(mlog.modified_date, "%Y%m%d") <= DATE_FORMAT(%4, "%Y%m%d")';

    $sql_params = [
      1 => [implode(',', $current_status_ids), 'CommaSeparatedIntegers'],
      2 => [$params['membership_type_id'], 'Positive'],
      3 => [$params['from'], 'Timestamp'],
      4 => [$params['to'], 'Timestamp'],
    ];

    if (!empty($params['preferred_language'])) {
      $sql .= ' AND c.preferred_language = %5';
      $sql_params[5] = [$params['preferred_language'], 'String'];
    }

    // $query = CRM_Core_DAO::composeQuery($sql, $sql_params, FALSE);
    // $this->addToDeveloperTab($sql);
    $dao = CRM_Core_DAO::executeQuery($sql, $sql_params);
    $rows = $this->copyDaoToResults($dao, $params);

    // Check to make sure that it was really a renewal, i.e. a different
    // status_id or end_date (although that could also be a change of the start_date?).
    // The previous log entry can have the same status_id if the member renewed early.

    // @todo hardcoded
    $not_renewal_statuses = [1,5]; // New, Pending

    foreach ($rows as $key => &$row) {
      $all_previous = $this->getPreviousMembershipLogs($row['mlogid'], $row['id']);
      $found_previous = false;

      if (empty($all_previous) && $row['start_date'] > $row['join_date']) {
        $found_previous = true;
        continue;
      }

      foreach ($all_previous as $p) {
        // If we were in grace, and the previous status was expired, then it's bad data.
        // Noticed this in a site where the admins would temporarily set members as grace.
        if ($row['status_id'] == 3 && $p['status_id'] == 4) {
          continue;
        }

        // If the previous update was not new or pending, then we discard the row.
        // It was probably an edit unrelated to a membership status change.
        if (in_array($p['status_id'], $not_renewal_statuses)) {
          unset($rows[$key]);
          continue 2;
        }

        // Check if the end_date has changed, which is usually a sign that it was a renewal
        if ($p['end_date'] == $row['end_date']) {
          unset($rows[$key]);
          continue 2;
        }

        // Whether we are looking for same membership type or for changes
        if ($same_or_change != 'either') {
          // On renewals, CiviCRM changes the membership_type_id before it changes the status_id
          // so if we want to detect a membership_type_id change, we need to change the row before it.
          $before_previous = $this->getPreviousMembershipLogs($p['id'], $p['membership_id']);

          if (!empty($before_previous)) {
            $bp = $before_previous[0];

            if ($bp['status_id'] == $p['status_id'] && $bp['membership_type_id'] != $p['membership_type_id']) {
              $p = $bp;
            }
          }

          if (($same_or_change == 'same') != ($p['membership_type_id'] == $row['membership_type_id'])) {
            unset($rows[$key]);
            continue 2;
          }
        }

        $rows[$key]['last_end_date'] = $p['end_date'];
        $rows[$key]['last_modified_date'] = $p['modified_date'];
        $rows[$key]['last_status_id'] = $p['status_id'];
        $rows[$key]['last_membership_type_id'] = $p['membership_type_id'];

        $found_previous = true;
        break;
      }

      if (!$found_previous) {
        unset($rows[$key]);
      }
    }

    if (!empty($params['primary_membership_type']) || !empty($params['display_primary_membership_type'])) {
      $this->filterByPrimaryMembershipType($rows, $params, $params['primary_membership_type']);
    }

    $cache[$cacheKey] = $rows;
    return $rows;
  }

  /**
   * Returns all members who are not Expired=4, Pending=5, Cancelled=6
   *
   * We intentionally do not sum columns, to help with auditing results.
   *
   * @todo Remove hardcoded IDs
   */
  public function calculateTotalActiveMembers($params) {
    $sql = 'SELECT m.id, m.contact_id, m.join_date, c.display_name, c.preferred_language,
        mlog.status_id, mlog.start_date, mlog.end_date, mlog.membership_id, mlog.membership_type_id
      FROM civicrm_membership_log mlog
      LEFT JOIN civicrm_membership m ON (m.id = mlog.membership_id)
      LEFT JOIN civicrm_contact c ON (c.id = m.contact_id)
      WHERE m.is_test = 0 AND mlog.status_id NOT IN (4,5,6)
        AND mlog.membership_type_id = %1
        AND DATE_FORMAT(mlog.start_date, "%Y%m%d") <= DATE_FORMAT(%2, "%Y%m%d")
        AND (DATE_FORMAT(mlog.end_date, "%Y%m%d") >= DATE_FORMAT(%3, "%Y%m%d") OR mlog.end_date IS NULL)';

    $sql_params = [
      1 => [$params['membership_type_id'], 'Positive'],
      2 => [$params['to'], 'Timestamp'],
      3 => [$params['from'], 'Timestamp'],
    ];

    if (!empty($params['preferred_language'])) {
      $sql .= ' AND c.preferred_language = %4';
      $sql_params[4] = [$params['preferred_language'], 'String'];
    }

    $sql .= ' GROUP BY m.id, mlog.membership_type_id';

    // $query = CRM_Core_DAO::composeQuery($sql, $sql_params, FALSE);
    // $this->addToDeveloperTab($query);

    $dao = CRM_Core_DAO::executeQuery($sql, $sql_params);
    $rows = $this->copyDaoToResults($dao, $params);

    if (!empty($params['primary_membership_type']) || !empty($params['display_primary_membership_type'])) {
      $this->filterByPrimaryMembershipType($rows, $params, $params['primary_membership_type']);
    }

    $this->translateData($rows);

    // Add grace members.
    // @todo Should we make this configurable? (or check the settings, whether grace are considered active)
    $t = $this->calculateGraceMembers($params);
    $rows = array_merge($rows, $t);

    $this->dedupeMemberships($rows);

    return $rows;
  }

  /**
   * Returns the members who are in grace during that period,
   * and who did not renew in that period. i.e. it was their last known status.
   *
   * @todo Remove hardcoded IDs. We can guess 'grace' by looking at status
   * after the end_date, but considered 'active'?
   *
   * Results are cached because this function is also called from
   * calculateTotalActiveMembers().
   */
  public function calculateGraceMembers($params) {
    static $cache = [];
    $cacheKey = sha1(json_encode($params));

    if (isset($cache[$cacheKey])) {
      return $cache[$cacheKey];
    }

    $sql = 'SELECT m.id, m.contact_id, m.join_date, c.display_name, c.preferred_language, mlog.modified_date,
        mlog.id as mlogid, mlog.status_id, mlog.start_date, mlog.end_date, mlog.membership_id, mlog.membership_type_id
      FROM civicrm_membership_log mlog
      LEFT JOIN civicrm_membership m ON (m.id = mlog.membership_id)
      LEFT JOIN civicrm_contact c ON (c.id = m.contact_id)
      WHERE m.is_test = 0 AND mlog.status_id = 3
        AND mlog.membership_type_id = %1
        AND DATE_FORMAT(mlog.modified_date, "%Y%m%d") >= DATE_FORMAT(%2, "%Y%m%d")
        AND DATE_FORMAT(mlog.modified_date, "%Y%m%d") <= DATE_FORMAT(%3, "%Y%m%d")';

    $sql_params = [
      1 => [$params['membership_type_id'], 'Positive'],
      2 => [$params['from'], 'Timestamp'],
      3 => [$params['to'], 'Timestamp'],
    ];

    if (!empty($params['preferred_language'])) {
      $sql .= ' AND c.preferred_language = %4';
      $sql_params[4] = [$params['preferred_language'], 'String'];
    }

    $sql .= ' GROUP BY m.id, mlog.membership_type_id';

    // $this->addToDeveloperTab($sql);
    $dao = CRM_Core_DAO::executeQuery($sql, $sql_params);
    $rows = $this->copyDaoToResults($dao, $params);

    // Check to make sure it was their last known status in that period
    foreach ($rows as $key => &$row) {
      $cpt = CRM_Core_DAO::singleValueQuery('SELECT count(*)
        FROM civicrm_membership_log
        WHERE membership_id = %1
          AND id > %2
          AND modified_date <= %3', [
        1 => [$row['id'], 'Positive'],
        2 => [$row['mlogid'], 'Positive'],
        3 => [$params['to'], 'Timestamp'],
      ]);

      if ($cpt) {
        unset($rows[$key]);
      }
    }

    if (!empty($params['primary_membership_type']) || !empty($params['display_primary_membership_type'])) {
      $this->filterByPrimaryMembershipType($rows, $params, $params['primary_membership_type']);
    }

    $this->translateData($rows);
    $this->dedupeMemberships($rows);
    $cache[$cacheKey] = $rows;
    return $rows;
  }

  /**
   *
   */
  public function calculateExpiredMembers($params) {
    $sql = 'SELECT m.id, m.contact_id, m.join_date, c.display_name, c.preferred_language, mlog.modified_date,
        mlog.status_id, mlog.start_date, mlog.end_date, mlog.membership_id, mlog.membership_type_id
      FROM civicrm_membership_log mlog
      LEFT JOIN civicrm_membership m ON (m.id = mlog.membership_id)
      LEFT JOIN civicrm_contact c ON (c.id = m.contact_id)
      WHERE m.is_test = 0 AND mlog.status_id = 4
        AND mlog.membership_type_id = %1
        AND DATE_FORMAT(mlog.modified_date, "%Y%m%d") >= DATE_FORMAT(%2, "%Y%m%d")
        AND DATE_FORMAT(mlog.modified_date, "%Y%m%d") <= DATE_FORMAT(%3, "%Y%m%d")';

    $sql_params = [
      1 => [$params['membership_type_id'], 'Positive'],
      2 => [$params['from'], 'Timestamp'],
      3 => [$params['to'], 'Timestamp'],
    ];

    if (!empty($params['preferred_language'])) {
      $sql .= ' AND c.preferred_language = %4';
      $sql_params[4] = [$params['preferred_language'], 'String'];
    }

    $sql .= ' GROUP BY m.id, mlog.membership_type_id';

    // $this->addToDeveloperTab($sql);
    $dao = CRM_Core_DAO::executeQuery($sql, $sql_params);
    $rows = $this->copyDaoToResults($dao, $params);

    // Check to make sure it was their last known status in that period
    foreach ($rows as $key => &$row) {
      $cpt = CRM_Core_DAO::singleValueQuery('SELECT count(*)
        FROM civicrm_membership_log
        WHERE membership_id = %1
          AND modified_date > %2
          AND modified_date <= %3', [
        1 => [$row['id'], 'Positive'],
        2 => [CRM_Utils_Date::isoToMysql($row['modified_date']), 'Date'],
        3 => [$params['to'], 'Timestamp'],
      ]);

      if ($cpt) {
        unset($rows[$key]);
      }
    }

    if (!empty($params['primary_membership_type']) || !empty($params['display_primary_membership_type'])) {
      $this->filterByPrimaryMembershipType($rows, $params, $params['primary_membership_type']);
    }

    $this->translateData($rows);
    $this->dedupeMemberships($rows);
    return $rows;
  }

  /**
   *
   */
  public function calculateMissingInSpecificColumns($params) {
    $rows = $this->calculateNew($params);
    $rows = array_merge($rows, $this->calculateLapsed($params));
    $rows = array_merge($rows, $this->calculateRenewalSame($params));
    $rows = array_merge($rows, $this->calculateRenewalChanged($params));
    $rows = array_merge($rows, $this->calculateGraceMembers($params));
    $this->dedupeMemberships($rows);

    $active = $this->calculateTotalActiveMembers($params);
    $missing = [];

    foreach ($active as $a) {
      $found = false;

      foreach ($rows as $r) {
        if ($r['id'] == $a['id']) {
          $found = true;
        }
      }

      if (!$found) {
        $missing[$a['id']] = $a;
      }
    }

    return $missing;
  }

  /**
   *
   */
  public function calculateMissingInActiveMembers($params) {
    $rows = $this->calculateNew($params);
    $rows += $this->calculateLapsed($params);
    $rows += $this->calculateRenewalSame($params);
    $rows += $this->calculateRenewalChanged($params);
    $rows += $this->calculateGraceMembers($params);
    $this->dedupeMemberships($rows);

    $active = $this->calculateTotalActiveMembers($params);
    $missing = [];

    foreach ($rows as $a) {
      $found = false;

      foreach ($active as $r) {
        if ($r['id'] == $a['id']) {
          $found = true;
        }
      }

      if (!$found) {
        $missing[] = $a;
      }
    }

    return $missing;
  }

  /**
   * Helper function to fetch the DAO data and returns an array
   * of results.
   */
  function copyDaoToResults(&$dao, $params) {
    $rows = [];

    while ($dao->fetch()) {
      $row = [
        'id' => $dao->id,
        'mlogid' => $dao->mlogid ?? '',
        'contact_id' => $dao->contact_id,
        'join_date' => $dao->join_date,
        'start_date' => $dao->start_date,
        'end_date' => $dao->end_date,
        'status_id' => $dao->status_id,
        'membership_type_id' => $dao->membership_type_id,
      ];

      if (!empty($dao->modified_date)) {
        $row['modified_date'] = $dao->modified_date;
      }

      if (!empty($params['reportaudit'])) {
        $row['contact_id.url.blank'] = CRM_Utils_System::url('civicrm/contact/view', ['reset' => 1, 'cid' => $dao->contact_id]);
        $row['contact_id.display'] = $dao->display_name;
        $row['id.url.popup'] = CRM_Utils_System::url('civicrm/contact/view/membership', ['reset' => 1, 'action' => 'view', 'cid' => $dao->contact_id, 'id' => $dao->id, 'context' => 'membership']);
      }

      $rows[] = $row;
    }

    return $rows;
  }

  /**
   * Fix things such as the Membership Type ID, or Status ID.
   */
  public function translateData(&$rows) {
    $membership_types = civicrm_api3('Membership', 'getoptions', [
      'field' => 'membership_type_id',
    ])['values'];

    $statuses = civicrm_api3('Membership', 'getoptions', [
      'field' => 'status_id',
    ])['values'];

    foreach ($rows as &$row) {
      if (isset($row['membership_type_id'])) {
        if (isset($membership_types[$row['membership_type_id']])) {
          $row['membership_type_id'] = $membership_types[$row['membership_type_id']];
        }
      }
      if (isset($row['last_membership_type_id'])) {
        if (isset($membership_types[$row['last_membership_type_id']])) {
          $row['last_membership_type_id'] = $membership_types[$row['last_membership_type_id']];
        }
      }
      if (isset($row['primary_membership_type_id'])) {
        if (isset($membership_types[$row['primary_membership_type_id']])) {
          $row['primary_membership_type_id'] = $membership_types[$row['primary_membership_type_id']];
        }
      }
      if (isset($row['status_id'])) {
        if (isset($statuses[$row['status_id']])) {
          $row['status_id'] = $statuses[$row['status_id']];
        }
      }
      if (isset($row['last_status_id'])) {
        if (isset($statuses[$row['last_status_id']])) {
          $row['last_status_id'] = $statuses[$row['last_status_id']];
        }
      }
    }
  }

  /**
   * Dedupe results. For example, a member may have gone into grace multiple
   * time in a given period.
   */
  public function dedupeMemberships(&$rows) {
    $seen = [];

    foreach ($rows as $key => $row) {
      if (!empty($seen[$row['id']])) {
        unset($rows[$key]);
      }

      $seen[$row['id']] = 1;
    }
  }

  /**
   * Filter by primary membership type.
   * Assumes that the primary membership dates have overlap with the subtype.
   */
  public function filterByPrimaryMembershipType(&$rows, $params, $primary_membership_type) {
    static $cache = [];

    foreach ($rows as $key => &$row) {
      $contact_id = $row['contact_id'];

      if (empty($cache[$contact_id])) {
        // Fetch primary membership in that same period
        // We are fetching all times, not just the filtered one, so that we can cache
        // and avoid doing a lookup for each primary membership type,
        // as well as avoid counting twice, if the member changed type in that period.
        $dao = CRM_Core_DAO::executeQuery('SELECT m.id, mlog.id as mlogid, m.contact_id, m.join_date, mlog.start_date, mlog.end_date, mlog.status_id, mlog.membership_type_id
           FROM civicrm_membership_log mlog
          LEFT JOIN civicrm_membership m ON (mlog.membership_id = m.id)
          WHERE m.contact_id = %1
            AND DATE_FORMAT(mlog.start_date, "%Y%m%d") <= DATE_FORMAT(%2, "%Y%m%d")
            AND (DATE_FORMAT(mlog.end_date, "%Y%m%d") >= DATE_FORMAT(%3, "%Y%m%d") OR end_date IS NULL)
            AND mlog.membership_type_id IN (%4)
          GROUP BY mlog.membership_type_id', [
          1 => [$row['contact_id'], 'Positive'],
          2 => [$row['end_date'], 'String'],
          3 => [$row['start_date'], 'String'],
          4 => [implode(',', array_keys($this->primary_membership_type_ids)), 'CommaSeparatedIntegers'],
        ]);

        $cache[$contact_id] = $this->copyDaoToResults($dao, $params)[0];
      }

      if (!empty($params['display_primary_membership_type'])) {
        $row['primary_membership_type_id'] = $cache[$contact_id]['membership_type_id'] ?? '';
        continue;
      }

      // This would mean that the member had a submembership, but not a primary membership?
      if (empty($cache[$contact_id])) {
        unset($rows[$key]);
        continue;
      }

      if ($cache[$contact_id]['membership_type_id'] != $primary_membership_type) {
        unset($rows[$key]);
      }
    }
  }

  /**
   * Helper function to fetch membership logs previous a specific entry.
   */
  public function getPreviousMembershipLogs($mlogid, $membership_id) {
    $dao = CRM_Core_DAO::executeQuery('SELECT *
      FROM civicrm_membership_log
      WHERE membership_id = %1
        AND id < %2
      ORDER BY modified_date DESC', [
      1 => [$membership_id, 'Positive'],
      2 => [$mlogid, 'Positive'],
    ]);

    return $dao->fetchAll();
  }

  /**
   * There must be an existing somewhere to do this?
   * Copy-pasting old code.
   */
  private function getFromToDates($fieldName) {
    // get the from/to. Using this instead of where() for more flexiblity (where only returns an sql clause).
    $relative = CRM_Utils_Array::value("{$fieldName}_relative", $this->_params);
    $from     = CRM_Utils_Array::value("{$fieldName}_from", $this->_params);
    $to       = CRM_Utils_Array::value("{$fieldName}_to", $this->_params);

    // from/to dates for intervals are in ISO format by default
    // If it's an ending date interval, ex: 20130930, then add the time
    // to make sure we include the last day completely.
    $from = CRM_Utils_Date::isoToMysql($from);
    $to = CRM_Utils_Date::isoToMysql($to).'235959';

    if ($relative) {
      // From CRM_Report_Form_getFromTo()
      // Calculate the date range for a relative date.
      // We don't use the getFromTo() because we don't care about the end date.
      list($term, $unit) = CRM_Utils_System::explode('.', $relative, 2);
      $dateRange = CRM_Utils_Date::relativeToAbsolute($term, $unit);
      $from = substr($dateRange['from'], 0, 8);

      // Take only Date Part, Sometime Time part is also present in 'to'
      // .. but make sure the 'to' finishes on the end of date, otherwise, ex: 20130930 will ignore the 30th.
      $to = substr($dateRange['to'], 0, 8) . '235959';
    }

    return [$from, $to];
  }
}
