<?php

require_once 'grantincoming.civix.php';

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function grantincoming_civicrm_config(&$config) {
  _grantincoming_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @param array $files
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function grantincoming_civicrm_xmlMenu(&$files) {
  _grantincoming_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function grantincoming_civicrm_install() {
  _grantincoming_civix_civicrm_install();
}

/**
* Implements hook_civicrm_postInstall().
*
* @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
*/
function grantincoming_civicrm_postInstall() {
  _grantincoming_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function grantincoming_civicrm_uninstall() {
  _grantincoming_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function grantincoming_civicrm_enable() {
  _grantincoming_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function grantincoming_civicrm_disable() {
  _grantincoming_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed
 *   Based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function grantincoming_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _grantincoming_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function grantincoming_civicrm_managed(&$entities) {
  _grantincoming_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * @param array $caseTypes
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function grantincoming_civicrm_caseTypes(&$caseTypes) {
  _grantincoming_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function grantincoming_civicrm_angularModules(&$angularModules) {
_grantincoming_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function grantincoming_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _grantincoming_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implementation of hook_civicrm_buildForm
 */
function grantincoming_civicrm_buildForm($formName, &$form) {
  // rename the fields
  if ($formName == 'CRM_Grant_Form_Grant') {
    if ($form->elementExists('grant_report_received')) {
      $form->removeElement('grant_report_received');
      $form->addElement('checkbox', 'grant_report_received', ts('Grant Report Sent?'), NULL);
    }

    if ($form->elementExists('application_received_date')) {
      $form->removeElement('application_received_date');
      $form->add('datepicker', 'application_received_date', ts('Application Due'), [], FALSE, ['time' => FALSE]);
    }

    if ($form->elementExists('contact_id')) {
      $form->removeElement('contact_id');
      $contactField = $form->addEntityRef('contact_id', ts('Grantor'), ['create' => TRUE], TRUE);
      if ($form->getVar('_context') != 'standalone') {
        $contactField->freeze();
      }
    }
  }
}

/**
 * Implementation of hook_civicrm_post()
 *
 */
function grantincoming_civicrm_post($op, $objectName, $objectId, &$objectRef) {
  // check 'Grant Report' activity type
  // and status 'Completed'
  if ($op != 'delete' && $objectName == 'Activity' && $objectRef->status_id == 2) {
    $activityTypes = CRM_Core_PseudoConstant::activityType();
    if ($objectRef->activity_type_id == CRM_Utils_Array::key('Grant Report', $activityTypes)) {
      $grantId = CRM_Core_DAO::getFieldValue('CRM_Activity_DAO_Activity', $objectId, 'source_record_id');
      if ($grantId) {
        // When the activity 'Grant Report' status is set to 'Completed',
        // then 'Grant Report Received' should be set automatically to Yes
        CRM_Core_DAO::setFieldValue('CRM_Grant_DAO_Grant', $grantId, 'grant_report_received', 1);
      }
    }
  }

  if ($op == 'delete' && $objectName == 'Grant') {
    $activityTypes = CRM_Core_PseudoConstant::activityType();
    $grantReport = CRM_Utils_Array::key('Grant Report', $activityTypes);
    $result = civicrm_api3('Activity', 'get', [
      'sequential' => 1,
      'return' => ['id'],
      'source_record_id' => $objectRef->id,
      'activity_type_id' => $grantReport,
    ]);

    if ($result['count'] && $result['id']) {
      //delete this activity if exists when grant is deleted
      $result = civicrm_api3('Activity', 'delete', [
        'id' => $result['id'],
      ]);
    }
  }

  if ($op != 'delete' && $objectName == 'Grant') {
    $session = CRM_Core_Session::singleton();
    $activityTypes = CRM_Core_PseudoConstant::activityType();
    $grantReport = CRM_Utils_Array::key('Grant Report', $activityTypes);

    $result = civicrm_api3('Activity', 'get', [
      'sequential' => 1,
      'return' => ['id'],
      'source_record_id' => $objectRef->id,
      'activity_type_id' => $grantReport,
    ]);

    $activityParams = [
      'source_contact_id' => $session->get('userID'),
      'source_record_id' => $objectRef->id,
      'activity_type_id' => $grantReport,
      'target_id' => $objectRef->contact_id,
      'activity_date_time' => $objectRef->grant_due_date,
      'subject' =>  CRM_Contact_BAO_Contact::displayName($objectRef->contact_id),
    ];

    if ($result['count'] && $result['id']) {
      //update to maintain sync
      $activityParams['id'] = $result['id'];
    }
    else {
      //when a new activity 'Grant Report' is created, it needs to be Scheduled
      $activityParams['status_id'] = 'Scheduled';
    }

    $assignees = [];
    $resultgc = civicrm_api3('GroupContact', 'get', [
      'sequential' => 1,
      'return' => ['contact_id'],
      'group_id' => 'grant_report_assignees',
    ]);
    foreach ($resultgc['values'] as $value) {
      $assignees[] = $value['contact_id'];
    }

    // build the assignees from configured group
    // 'Grant Report Assignees'
    if (!empty($assignees)) {
      $activityParams['assignee_id'] = $assignees;
    }

    if (!CRM_Utils_System::isNull($objectRef->grant_due_date)) {
      //create or update when date is present
      $result = civicrm_api3('Activity', 'create', $activityParams);
    }
    else {
      if ($result['count'] && $result['id']) {
       //delete this activity if exists when date is removed
        $result = civicrm_api3('Activity', 'delete', [
          'id' => $result['id'],
        ]);
      }
    }
  }
}

/**
 * Functions below this ship commented out. Uncomment as required.
 *

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function grantincoming_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function grantincoming_civicrm_navigationMenu(&$menu) {
  _grantincoming_civix_insert_navigation_menu($menu, NULL, array(
    'label' => ts('The Page', array('domain' => 'com.cividesk.grant.incoming')),
    'name' => 'the_page',
    'url' => 'civicrm/the-page',
    'permission' => 'access CiviReport,access CiviContribute',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _grantincoming_civix_navigationMenu($menu);
} // */
