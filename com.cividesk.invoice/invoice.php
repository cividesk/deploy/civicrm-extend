<?php

require_once 'invoice.civix.php';

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function invoice_civicrm_config(&$config) {
  _invoice_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @param array $files
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function invoice_civicrm_xmlMenu(&$files) {
  _invoice_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function invoice_civicrm_install() {
  _invoice_civix_civicrm_install();
}

/**
* Implements hook_civicrm_postInstall().
*
* @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
*/
function invoice_civicrm_postInstall() {
  _invoice_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function invoice_civicrm_uninstall() {
  _invoice_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function invoice_civicrm_enable() {
  _invoice_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function invoice_civicrm_disable() {
  _invoice_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed
 *   Based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function invoice_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _invoice_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function invoice_civicrm_managed(&$entities) {
  _invoice_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * @param array $caseTypes
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function invoice_civicrm_caseTypes(&$caseTypes) {
  _invoice_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function invoice_civicrm_angularModules(&$angularModules) {
_invoice_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function invoice_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _invoice_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Functions below this ship commented out. Uncomment as required.
 *

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function invoice_civicrm_preProcess($formName, &$form) {

} // */

function invoice_civicrm_alterMailParams(&$params, $context = NULL) {
  if ($params['valueName'] == 'contribution_invoice_receipt') {
    //get the custom field id for Invoice Date and Invoice ID
    $result = civicrm_api3('CustomField', 'getsingle', array('return' => array("id"), 'name' => "Invoice_Date"));
    $date = $result['id'];

    $result = civicrm_api3('CustomField', 'getsingle', array('return' => array("id"), 'name' => "Invoice_Id"));
    $id = $result['id'];

    if (!$date || !$id) {
      return;
    }

    $invoiceDate = 'custom_' . $date;
    $invoiceID = 'custom_' . $id;
    //check if invoice has already been generated
    $result = civicrm_api3('Contribution', 'get', array(
      'sequential' => 1,
      'return' => array($invoiceDate, $invoiceID),
      'id' => $params['tplParams']['id'],
    ));

    if (!empty($result['values'][0][$invoiceDate]) && !empty($result['values'][0][$invoiceID])) {
      $params['tplParams']['invoice_number'] = $result['values'][0][$invoiceID];
      $params['PDFFilename'] = $result['values'][0][$invoiceID] . '.pdf';
      $params['tplParams']['invoice_date'] = CRM_Utils_Date::customFormat($result['values'][0][$invoiceDate], '%B %E%f, %Y');
    }
    else {
      list($table, $column, $groupID) = CRM_Core_BAO_CustomField::getTableColumnGroup($id);
      $query = "
SELECT MAX( $column )
FROM {$table}
WHERE LENGTH( $column ) = (
SELECT MAX( LENGTH($column) )
FROM $table)
";
      $inv = CRM_Core_DAO::singleValueQuery($query);

      $max = (int) str_replace(CRM_Contribute_BAO_Contribution::checkContributeSettings('invoice_prefix', TRUE), '', $inv);
      $contactIDs = (array) $params['contactId'];
      foreach ($contactIDs as $contactID) {
        $invoiceNumber = CRM_Contribute_BAO_Contribution::checkContributeSettings('invoice_prefix', TRUE) . ($max+1);
        $result = civicrm_api3('Contribution', 'create',
          array(
            'id' => $params['tplParams']['id'],
            'contact_id' => $contactID,
            $invoiceID => $invoiceNumber,
            $invoiceDate => date('Ymd'))
        );
        $params['PDFFilename'] = $invoiceNumber . '.pdf';
        $params['tplParams']['invoice_number'] = $invoiceNumber;
      }
    }
  }
}

/**
 * Implementation of hook_civicrm_pageRun
 */
function invoice_civicrm_pageRun( &$page ) {
  CRM_Core_Region::instance('page-body')->add(array(
    'script' => "cj('#crm-contribution-invoice_number').hide();",
  ));
}

/**
 * Implementation of hook_civicrm_pre
 */
function invoice_civicrm_pre( $op, $objectName, $objectId, &$objectRef ) {
  if ($objectName == 'Contribution' && $op == 'create' ) {
    $result = civicrm_api3('CustomField', 'getsingle', array('return' => array("id"), 'name' => "Invoice_Date"));
    $date = $result['id'];
    $result = civicrm_api3('CustomField', 'getsingle', array('return' => array("id"), 'name' => "Invoice_Id"));
    $id = $result['id'];
    unset($objectRef['custom'][$date]);
    unset($objectRef['custom'][$id]);
  }
}

/**
 * Implementation of hook_civicrm_buildForm
 */
function invoice_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Contribute_Form_Contribution' || $formName == 'CRM_Custom_Form_CustomDataByType') {
    CRM_Core_Region::instance('page-body')->add(array(
      'script' => "
        // Wait till custom field is dynamically created on form
        setTimeout(function waitInv() {
          if (cj('.custom-group-Invoice_Details .addon').length > 0) {
            cj('.custom-group-Invoice_Details input').attr('readonly', 'readonly');
            cj('.custom-group-Invoice_Details .fa-calendar').hide();
            cj('.custom-group-Invoice_Details .fa-times').hide();
          } else {
            setTimeout(waitInv, 250);
          }
        }, 250);
      ",
    ));
  }

  if ($formName == 'CRM_Contribute_Form_Search') {
    CRM_Core_Region::instance('page-body')->add(array(
      'script' => "
         cj('.crm-contribution-invoice_number').hide();
      ",
    ));
  }
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function invoice_civicrm_navigationMenu(&$menu) {
  _invoice_civix_insert_navigation_menu($menu, NULL, array(
    'label' => ts('The Page', array('domain' => 'com.cividesk.invoice')),
    'name' => 'the_page',
    'url' => 'civicrm/the-page',
    'permission' => 'access CiviReport,access CiviContribute',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _invoice_civix_navigationMenu($menu);
} // */
