<?php return array(
    'root' => array(
        'name' => 'civicrm/emailapi',
        'pretty_version' => '2.9',
        'version' => '2.9.0.0',
        'reference' => 'b70d6e85dabd55bd5fb58686296f8e18653772c3',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'civicrm/emailapi' => array(
            'pretty_version' => '2.9',
            'version' => '2.9.0.0',
            'reference' => 'b70d6e85dabd55bd5fb58686296f8e18653772c3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
