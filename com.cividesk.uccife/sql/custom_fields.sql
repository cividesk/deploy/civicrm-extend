#####################
## SCHEMA CREATION ##
#####################
## Please follow instructions, one group at a time

## Creation of custom groups
## PLEASE take either English, French or Spanish!
INSERT INTO `civicrm_custom_group` (`name`, `title`, `extends`, `extends_entity_column_id`, `extends_entity_column_value`, `style`, `collapse_display`, `help_pre`, `help_post`, `weight`, `is_active`, `table_name`, `is_multiple`, `min_multiple`, `max_multiple`, `collapse_adv_display`, `created_id`, `created_date`) VALUES
('UCCIFE_Directory_Ind', 'UCCIFE Directory', 'Individual',   NULL, NULL, 'Inline', 0, '', '', 8, 1, 'civicrm_value_uccife_directory_ind', 0, NULL, NULL, 0, NULL, NOW()),
('UCCIFE_Directory_Org', 'UCCIFE Directory', 'Organization', NULL, NULL, 'Inline', 0, '', '', 8, 1, 'civicrm_value_uccife_directory_org', 0, NULL, NULL, 0, NULL, NOW());
INSERT INTO `civicrm_option_group` (`name`, `title`, `description`, `is_reserved`, `is_active`) VALUES
('uccife_industrie', 'UCCIFE Industry Codes', NULL, NULL, 1);

INSERT INTO `civicrm_custom_group` (`name`, `title`, `extends`, `extends_entity_column_id`, `extends_entity_column_value`, `style`, `collapse_display`, `help_pre`, `help_post`, `weight`, `is_active`, `table_name`, `is_multiple`, `min_multiple`, `max_multiple`, `collapse_adv_display`, `created_id`, `created_date`) VALUES
('UCCIFE_Directory_Ind', 'Annuaire UCCIFE', 'Individual',   NULL, NULL, 'Inline', 0, '', '', 8, 1, 'civicrm_value_uccife_directory_ind', 0, NULL, NULL, 0, NULL, NOW()),
('UCCIFE_Directory_Org', 'Annuaire UCCIFE', 'Organization', NULL, NULL, 'Inline', 0, '', '', 8, 1, 'civicrm_value_uccife_directory_org', 0, NULL, NULL, 0, NULL, NOW());
INSERT INTO `civicrm_option_group` (`name`, `title`, `description`, `is_reserved`, `is_active`) VALUES
('uccife_industrie', 'Codes Industrie UCCIFE', NULL, NULL, 1);

INSERT INTO `civicrm_custom_group` (`name`, `title`, `extends`, `extends_entity_column_id`, `extends_entity_column_value`, `style`, `collapse_display`, `help_pre`, `help_post`, `weight`, `is_active`, `table_name`, `is_multiple`, `min_multiple`, `max_multiple`, `collapse_adv_display`, `created_id`, `created_date`) VALUES
('UCCIFE_Directory_Ind', 'Directorio UCCIFE', 'Individual',   NULL, NULL, 'Inline', 0, '', '', 8, 1, 'civicrm_value_uccife_directory_ind', 0, NULL, NULL, 0, NULL, NOW()),
('UCCIFE_Directory_Org', 'Directorio UCCIFE', 'Organization', NULL, NULL, 'Inline', 0, '', '', 8, 1, 'civicrm_value_uccife_directory_org', 0, NULL, NULL, 0, NULL, NOW());
INSERT INTO `civicrm_option_group` (`name`, `title`, `description`, `is_reserved`, `is_active`) VALUES
('uccife_industrie', 'Sector de Actividad UCCIFE', NULL, NULL, 1);

## AFTER creating the above, do a replace on the rest of the file
## 100100 with the id for UCCIFE_Directory_Ind
## 100101 with the id for UCCIFE_Directory_Ord
## 100102 with the id for uccife_industrie
## rmfacc08 with the default password
## NOTE: You will UNDO these replacements afterwards

## WATCH for 100101 at beginning of 2nd and 3rd insert, and 100102 at the end of 2nd insert!
INSERT INTO `civicrm_custom_field` (`custom_group_id`, `name`, `label`, `data_type`, `html_type`, `default_value`, `is_required`, `is_searchable`, `is_search_range`, `weight`, `help_pre`, `help_post`, `mask`, `attributes`, `javascript`, `is_active`, `is_view`, `options_per_line`, `text_length`, `start_date_years`, `end_date_years`, `date_format`, `time_format`, `note_columns`, `note_rows`, `column_name`, `option_group_id`) VALUES
(100100, 'Password', 'Mot de passe', 'String', 'Text', 'rmfacc08', 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 255, NULL, NULL, NULL, NULL, 60, 4, 'password', NULL),
(100101, 'Industry', 'Industrie', 'String', 'Select', NULL, 0, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 255, NULL, NULL, NULL, NULL, 60, 4, 'industry_code', 100102),
(100101, 'Description', 'Description', 'Memo', 'TextArea', NULL, 0, 0, 0, 2, NULL, NULL, NULL, 'rows=4, cols=60', NULL, 1, 0, NULL, 255, NULL, NULL, NULL, NULL, 60, 4, 'description', NULL);

INSERT INTO `civicrm_custom_field` (`custom_group_id`, `name`, `label`, `data_type`, `html_type`, `default_value`, `is_required`, `is_searchable`, `is_search_range`, `weight`, `help_pre`, `help_post`, `mask`, `attributes`, `javascript`, `is_active`, `is_view`, `options_per_line`, `text_length`, `start_date_years`, `end_date_years`, `date_format`, `time_format`, `note_columns`, `note_rows`, `column_name`, `option_group_id`) VALUES
(100100, 'Password', 'Contraseña', 'String', 'Text', 'rmfacc08', 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 255, NULL, NULL, NULL, NULL, 60, 4, 'password', NULL),
(100101, 'Industry', 'Sector de Actividad', 'String', 'Select', NULL, 0, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 255, NULL, NULL, NULL, NULL, 60, 4, 'industry_code', 100102),
(100101, 'Description', 'Descripción de Actividad', 'Memo', 'TextArea', NULL, 0, 0, 0, 2, NULL, NULL, NULL, 'rows=4, cols=60', NULL, 1, 0, NULL, 255, NULL, NULL, NULL, NULL, 60, 4, 'description', NULL);

CREATE TABLE IF NOT EXISTS `civicrm_value_uccife_directory_ind` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Default MySQL primary key',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Table that this extends',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'rmfacc08',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_entity_id` (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `civicrm_value_uccife_directory_org` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Default MySQL primary key',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Table that this extends',
  `industry_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_entity_id` (`entity_id`),
  KEY `INDEX_industry_code` (`industry_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

## CREATION of option values IN FRENCH
## SELECT one or the other based on language prefs
INSERT INTO `civicrm_option_value` (`option_group_id`, `label`, `value`, `is_default`, `weight` ) VALUES
(100102,'Administration'                ,'2',    0,10),
(100102,'Aéronautique'                  ,'338',  0,30),
(100102,'Agroalimentaire'               ,'1',    0,20),
(100102,'Architecture'                  ,'3',    0,40),
(100102,'Assurance'                     ,'48',   0,50),
(100102,'Autres'                        ,'9',    0,60),
(100102,'Autres Services Professionnels','10',   0,70),
(100102,'Avocats'                       ,'11',   0,80),
(100102,'Banque'                        ,'12',   0,90),
(100102,'Biens de Consommation'         ,'13',   0,100),
(100102,'Biotechnologie'                ,'14',   0,110),
(100102,'Commerce de détail'            ,'15',   0,120),
(100102,'Communication, Publicité, RP'  ,'16',   0,130),
(100102,'Comptabilité'                  ,'18',   0,140),
(100102,'Conseil'                       ,'19',   0,150),
(100102,'Construction BTP'              ,'20',   0,160),
(100102,'Electronique'                  ,'21',   0,170),
(100102,'Energie'                       ,'22',   0,180),
(100102,'Enseignement'                  ,'23',   0,190),
(100102,'Environnement'                 ,'24',   0,200),
(100102,'Finance'                       ,'25',   0,210),
(100102,'Immobilier'                    ,'26',   0,220),
(100102,'Industrie'                     ,'27',   0,230),
(100102,'Industrie Chimique'            ,'28',   0,240),
(100102,'Industrie Lourde'              ,'29',   0,250),
(100102,'Industrie Manufacturière'      ,'30',   0,260),
(100102,'Ingénierie'                    ,'31',   0,270),
(100102,'Logiciels'                     ,'32',   0,280),
(100102,'Loisirs Restauration'          ,'33',   0,290),
(100102,'Média'                         ,'35',   0,300),
(100102,'Produits de Beauté'            ,'36',   0,310),
(100102,'Recrutement'                   ,'37',   0,320),
(100102,'Sans but lucratif'             ,'38',   0,330),
(100102,'Santé'                         ,'39',   0,340),
(100102,'Systèmes Informatiques'        ,'40',   0,350),
(100102,'Technologie'                   ,'41',   0,360),
(100102,'Télécommunications'            ,'42',   0,370),
(100102,'Textile'                       ,'44',   0,380),
(100102,'Transports & Logistique'       ,'45',   0,390),
(100102,'Vins et Spiritueux'            ,'46',   0,400),
(100102,'Voyages et Hôtellerie'         ,'47',   0,410);
UPDATE `civicrm_option_value` SET `name`=`label` WHERE `option_group_id`=100102;

## CREATION of option values IN ENGLISH
## SELECT one or the other based on language prefs
INSERT INTO `civicrm_option_value` (`option_group_id`, `label`, `value`, `is_default`, `weight` ) VALUES
(100102,'Accounting'                    ,'18',   0,10),
(100102,'Administration'                ,'2',    0,20),
(100102,'Aeronautics'                   ,'338',  0,30),
(100102,'Agriculture'                   ,'1',    0,30),
(100102,'Architecture'                  ,'3',    0,40),
(100102,'Bank'                          ,'12',   0,50),
(100102,'Beauty products'               ,'36',   0,60),
(100102,'Biotechnology'                 ,'14',   0,70),
(100102,'Construction'                  ,'20',   0,80),
(100102,'Consulting'                    ,'19',   0,90),
(100102,'Consumer Goods'                ,'13',   0,100),
(100102,'Communication, Advertising, PR','16',   0,110),
(100102,'Computer Systems'              ,'40',   0,120),
(100102,'Education'                     ,'23',   0,130),
(100102,'Electronics'                   ,'21',   0,140),
(100102,'Energy'                        ,'22',   0,150),
(100102,'Engineering'                   ,'31',   0,160),
(100102,'Environnement'                 ,'24',   0,170),
(100102,'Finance'                       ,'25',   0,180),
(100102,'Healthcare'                    ,'39',   0,190),
(100102,'Industrial'                    ,'27',   0,200),
(100102,'Industrial - Chemical'         ,'28',   0,210),
(100102,'Industrial - Manufacturing'    ,'30',   0,220),
(100102,'Industrial - Prime material'   ,'29',   0,230),
(100102,'Insurance'                     ,'48',   0,240),
(100102,'Legal Services'                ,'11',   0,250),
(100102,'Media'                         ,'35',   0,260),
(100102,'Nonprofit'                     ,'38',   0,270),
(100102,'Other'                         ,'9',    0,280),
(100102,'Other Professional Services'   ,'10',   0,290),
(100102,'Real Estate'                   ,'26',   0,300),
(100102,'Recruiting'                    ,'37',   0,310),
(100102,'Retail'                        ,'15',   0,320),
(100102,'Software'                      ,'32',   0,330),
(100102,'Technology'                    ,'41',   0,340),
(100102,'Telecommunications'            ,'42',   0,350),
(100102,'Textile'                       ,'44',   0,360),
(100102,'Tourism and Restaurants'       ,'33',   0,370),
(100102,'Transports & Logistics'        ,'45',   0,380),
(100102,'Travel and Hospitality'        ,'47',   0,390),
(100102,'Wine & Liquor'                 ,'46',   0,400);
UPDATE `civicrm_option_value` SET `name`=`label` WHERE `option_group_id`=100102;

## CREATION of option values IN SPANISH
## SELECT one or the other based on language prefs
INSERT INTO `civicrm_option_value` (`option_group_id`, `label`, `value`, `is_default`, `weight` ) VALUES
(100102,'Abogados'                        ,'11',   0,250),
(100102,'Administración'                  ,'2',    0,20),
(100102,'Aeronaútica'                     ,'338',  0,30),
(100102,'Agricultura'                     ,'1',    0,30),
(100102,'Alimentos y Bebidas'             ,'33',   0,370),
(100102,'Arquitectura'                    ,'3',    0,40),
(100102,'Bienes de Consumo'               ,'13',   0,100),
(100102,'Biotecnología'                   ,'14',   0,70),
(100102,'Bancos'                          ,'12',   0,50),
(100102,'Comunicación, Publicidad y RR.PP','16',   0,110),
(100102,'Construcción'                    ,'20',   0,80),
(100102,'Consultoría'                     ,'19',   0,90),
(100102,'Contabilidad'                    ,'18',   0,10),
(100102,'Electroníca'                     ,'21',   0,140),
(100102,'Energía'                         ,'22',   0,150),
(100102,'Finanzas'                        ,'25',   0,180),
(100102,'Formación'                       ,'23',   0,130),
(100102,'Industria'                       ,'27',   0,200),
(100102,'Industria Quimíca'               ,'28',   0,210),
(100102,'Industria Maquiladoras'          ,'30',   0,220),
(100102,'Industria Pesada'                ,'29',   0,230),
(100102,'Industria Textil'                ,'44',   0,360),
(100102,'Ingenieria'                      ,'31',   0,160),
(100102,'Inmobiliaria'                    ,'26',   0,300),
(100102,'Medio Ambiente'                  ,'24',   0,170),
(100102,'Medios'                          ,'35',   0,260),
(100102,'Minorista'                       ,'15',   0,320),
(100102,'Otros'                           ,'9',    0,280),
(100102,'Otros Servicios Profesionales'   ,'10',   0,290),
(100102,'Productos de Belleza'            ,'36',   0,60),
(100102,'Reclutamiento'                   ,'37',   0,310),
(100102,'Salud'                           ,'39',   0,190),
(100102,'Seguros'                         ,'48',   0,240),
(100102,'Sin fines de lucro'              ,'38',   0,270),
(100102,'Sistemas Informatico'            ,'40',   0,120),
(100102,'Software'                        ,'32',   0,330),
(100102,'Tecnologías'                     ,'41',   0,340),
(100102,'Telecomunicaciones'              ,'42',   0,350),
(100102,'Transporte y Logística'          ,'45',   0,380),
(100102,'Turismo y Hotelería'             ,'47',   0,390),
(100102,'Vinos y Licores'                 ,'46',   0,400);
UPDATE `civicrm_option_value` SET `name`=`label` WHERE `option_group_id`=100102;

##########################
## ADDITIONAL FUNCTIONS ##
##########################

## SET PASSWORD to default for all entries
INSERT INTO `civicrm_value_uccife_directory_ind` (`entity_id`, `password`)
  (SELECT c.id as entity_id, 'rmfacc08' as `password`
   FROM civicrm_contact c 
   INNER JOIN civicrm_membership m ON c.id = m.contact_id AND m.is_test = 0
   WHERE c.contact_type = 'Individual'
   GROUP BY c.id)