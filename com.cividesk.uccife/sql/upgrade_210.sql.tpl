-- Schema update to account for additional Industry code

SELECT id INTO @opt FROM `civicrm_option_group` WHERE `name` = 'uccife_industrie';
INSERT INTO `civicrm_option_value` (`option_group_id`, `name`, {localize field='label'}`label`{/localize}, `value`, `is_default`, `weight`) VALUES
  (@opt, 'art/design', {localize}'Art / Design'{/localize}, '419', 0, 45);