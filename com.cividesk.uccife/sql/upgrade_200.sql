-- Schema update to account for company logo & individual pictures
-- we are NOT USING the API as we need these field names to not have _## at the end

ALTER TABLE `civicrm_value_uccife_directory_ind`
  ADD COLUMN `photo` int(10) unsigned DEFAULT NULL,
  ADD KEY `FK_civicrm_value_uccife_directory_ind_photo` (`photo`);
ALTER TABLE `civicrm_value_uccife_directory_ind`
  ADD CONSTRAINT `FK_civicrm_value_uccife_directory_ind_photo` FOREIGN KEY (`photo`) REFERENCES `civicrm_file` (`id`) ON DELETE SET NULL;

SELECT id INTO @ind FROM `civicrm_custom_group` WHERE `name` = 'UCCIFE_Directory_Ind';
INSERT INTO `civicrm_custom_field` (`custom_group_id`, `name`, `label`, `data_type`, `html_type`, `default_value`, `is_required`, `is_searchable`, `is_search_range`, `weight`, `help_pre`, `help_post`, `mask`, `attributes`, `javascript`, `is_active`, `is_view`, `options_per_line`, `text_length`, `start_date_years`, `end_date_years`, `date_format`, `time_format`, `note_columns`, `note_rows`, `column_name`, `option_group_id`, `filter`) VALUES
  (@ind, 'Photo', 'Photo', 'File', 'File', NULL, 0, 0, 0, 25, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 255, NULL, NULL, NULL, NULL, 60, 4, 'photo', NULL, NULL);

ALTER TABLE `civicrm_value_uccife_directory_org`
  ADD COLUMN `logo` int(10) unsigned DEFAULT NULL,
  ADD KEY `FK_civicrm_value_uccife_directory_org_logo` (`logo`);
ALTER TABLE `civicrm_value_uccife_directory_org`
  ADD CONSTRAINT `FK_civicrm_value_uccife_directory_org_logo` FOREIGN KEY (`logo`) REFERENCES `civicrm_file` (`id`) ON DELETE SET NULL;

SELECT id INTO @org FROM `civicrm_custom_group` WHERE `name` = 'UCCIFE_Directory_Org';
INSERT INTO `civicrm_custom_field` (`custom_group_id`, `name`, `label`, `data_type`, `html_type`, `default_value`, `is_required`, `is_searchable`, `is_search_range`, `weight`, `help_pre`, `help_post`, `mask`, `attributes`, `javascript`, `is_active`, `is_view`, `options_per_line`, `text_length`, `start_date_years`, `end_date_years`, `date_format`, `time_format`, `note_columns`, `note_rows`, `column_name`, `option_group_id`, `filter`) VALUES
  (@org, 'Logo', 'Logo', 'File', 'File', NULL, 0, 0, 0, 5, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 255, NULL, NULL, NULL, NULL, 60, 4, 'logo', NULL, NULL);
