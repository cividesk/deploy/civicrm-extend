-- Create custom fields for the UCCIFE Directory
-- This is executed AUTOMATICALLY on extension install

-- There are 2 sets of custom fields: one on Individuals and one on Organizations
-- We need to:
--   * create tables to host data
--   * create custom_group definitions
--   * create custom_field definitions
--   * create option_value for the fields

-- Create data table for Individuals
CREATE TABLE IF NOT EXISTS `civicrm_value_uccife_directory_ind` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Default MySQL primary key',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Table that this extends',
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'rmfacc08',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_entity_id` (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Create data table for Organizations
CREATE TABLE IF NOT EXISTS `civicrm_value_uccife_directory_org` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Default MySQL primary key',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Table that this extends',
  `industry_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_entity_id` (`entity_id`),
  KEY `INDEX_industry_code` (`industry_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Create custom_group definitions
INSERT INTO `civicrm_custom_group` (`name`, `title`, `extends`, `extends_entity_column_id`, `extends_entity_column_value`, `style`, `collapse_display`, `help_pre`, `help_post`, `weight`, `is_active`, `table_name`, `is_multiple`, `min_multiple`, `max_multiple`, `collapse_adv_display`, `created_id`, `created_date`) VALUES
  ('UCCIFE_Directory_Ind', 'Annuaire UCCIFE', 'Individual',   NULL, NULL, 'Inline', 0, '', '', 8, 1, 'civicrm_value_uccife_directory_ind', 0, NULL, NULL, 0, NULL, NOW()),
  ('UCCIFE_Directory_Org', 'Annuaire UCCIFE', 'Organization', NULL, NULL, 'Inline', 0, '', '', 8, 1, 'civicrm_value_uccife_directory_org', 0, NULL, NULL, 0, NULL, NOW());
SELECT id INTO @ind FROM `civicrm_custom_group` WHERE `name` = 'UCCIFE_Directory_Ind';
SELECT id INTO @org FROM `civicrm_custom_group` WHERE `name` = 'UCCIFE_Directory_Org';

-- Create option_group for the Industry field (needed in next step)
INSERT INTO `civicrm_option_group` (`name`, `title`, `description`, `is_reserved`, `is_active`) VALUES
  ('uccife_industrie', 'Codes Industrie UCCIFE', NULL, NULL, 1);
SELECT id INTO @opt FROM `civicrm_option_group` WHERE `name` = 'uccife_industrie';

-- Create custom_field definitions
INSERT INTO `civicrm_custom_field` (`custom_group_id`, `name`, `label`, `data_type`, `html_type`, `default_value`, `is_required`, `is_searchable`, `is_search_range`, `weight`, `help_pre`, `help_post`, `mask`, `attributes`, `javascript`, `is_active`, `is_view`, `options_per_line`, `text_length`, `start_date_years`, `end_date_years`, `date_format`, `time_format`, `note_columns`, `note_rows`, `column_name`, `option_group_id`) VALUES
(@ind, 'Password', 'Mot de passe', 'String', 'Text', 'rmfacc08', 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 255, NULL, NULL, NULL, NULL, 60, 4, 'password', NULL),
(@org, 'Industry', 'Industrie', 'String', 'Select', NULL, 0, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 255, NULL, NULL, NULL, NULL, 60, 4, 'industry_code', @opt),
(@org, 'Description', 'Description', 'Memo', 'TextArea', NULL, 0, 0, 0, 2, NULL, NULL, NULL, 'rows=4, cols=60', NULL, 1, 0, NULL, 255, NULL, NULL, NULL, NULL, 60, 4, 'description', NULL);

-- Create option_value for the Industry field
INSERT INTO `civicrm_option_value` (`option_group_id`, `name`, `label`, `value`, `is_default`, `weight` ) VALUES
(@opt,'administration','Administration'                ,'2',    0,10),
(@opt,'agriculture'   ,'Aéronautique'                  ,'338',  0,30),
(@opt,'aeronautics'   ,'Agroalimentaire'               ,'1',    0,20),
(@opt,'architecture'  ,'Architecture'                  ,'3',    0,40),
(@opt,'insurance'     ,'Assurance'                     ,'48',   0,50),
(@opt,'other'         ,'Autres'                        ,'9',    0,60),
(@opt,'services'      ,'Autres Services Professionnels','10',   0,70),
(@opt,'lawyer'        ,'Avocats'                       ,'11',   0,80),
(@opt,'bank'          ,'Banque'                        ,'12',   0,90),
(@opt,'consumer'      ,'Biens de Consommation'         ,'13',   0,100),
(@opt,'biotech'       ,'Biotechnologie'                ,'14',   0,110),
(@opt,'retail'        ,'Commerce de détail'            ,'15',   0,120),
(@opt,'advertising'   ,'Communication, Publicité, RP'  ,'16',   0,130),
(@opt,'accounting'    ,'Comptabilité'                  ,'18',   0,140),
(@opt,'consulting'    ,'Conseil'                       ,'19',   0,150),
(@opt,'construction'  ,'Construction BTP'              ,'20',   0,160),
(@opt,'electronics'   ,'Electronique'                  ,'21',   0,170),
(@opt,'energy'        ,'Energie'                       ,'22',   0,180),
(@opt,'education'     ,'Enseignement'                  ,'23',   0,190),
(@opt,'environment'   ,'Environnement'                 ,'24',   0,200),
(@opt,'finance'       ,'Finance'                       ,'25',   0,210),
(@opt,'real estate'   ,'Immobilier'                    ,'26',   0,220),
(@opt,'industrial'    ,'Industrie'                     ,'27',   0,230),
(@opt,'chemical'      ,'Industrie Chimique'            ,'28',   0,240),
(@opt,'material'      ,'Industrie Lourde'              ,'29',   0,250),
(@opt,'manufacturing' ,'Industrie Manufacturière'      ,'30',   0,260),
(@opt,'engineering'   ,'Ingénierie'                    ,'31',   0,270),
(@opt,'software'      ,'Logiciels'                     ,'32',   0,280),
(@opt,'tourism'       ,'Loisirs Restauration'          ,'33',   0,290),
(@opt,'media'         ,'Média'                         ,'35',   0,300),
(@opt,'beauty'        ,'Produits de Beauté'            ,'36',   0,310),
(@opt,'recruiting'    ,'Recrutement'                   ,'37',   0,320),
(@opt,'nonprofit'     ,'Sans but lucratif'             ,'38',   0,330),
(@opt,'healthcare'    ,'Santé'                         ,'39',   0,340),
(@opt,'computers'     ,'Systèmes Informatiques'        ,'40',   0,350),
(@opt,'technology'    ,'Technologie'                   ,'41',   0,360),
(@opt,'telecom'       ,'Télécommunications'            ,'42',   0,370),
(@opt,'textile'       ,'Textile'                       ,'44',   0,380),
(@opt,'transport'     ,'Transports & Logistique'       ,'45',   0,390),
(@opt,'liquor'        ,'Vins et Spiritueux'            ,'46',   0,400),
(@opt,'travel'        ,'Voyages et Hôtellerie'         ,'47',   0,410);