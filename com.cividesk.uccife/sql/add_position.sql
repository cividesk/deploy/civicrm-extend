-- Schema update to account for individual position in directory
-- we are NOT USING the API as we need these field names to not have _## at the end

-- Create option group
INSERT INTO `civicrm_option_group` (`name`, `title`, `description`, `is_reserved`, `is_active`) VALUES
  ('uccife_position', 'UCCIFE Directory Positions', NULL, NULL, 1);
SELECT id INTO @opt FROM `civicrm_option_group` WHERE `name` = 'uccife_position';

-- Add option values
INSERT INTO `civicrm_option_value` (`option_group_id`, `label`, `value`, `name`, `grouping`, `filter`, `is_default`, `weight`, `description`, `is_optgroup`, `is_reserved`, `is_active`, `component_id`, `domain_id`, `visibility_id`) VALUES
(@opt, 'Normal', '0', 'Normal', NULL, NULL, 0, 1, NULL, 0, 0, 1, NULL, NULL, NULL),
(@opt, 'Featured', '1', 'Featured', NULL, NULL, 0, 2, NULL, 0, 0, 1, NULL, NULL, NULL),
(@opt, 'Excluded', '-1', 'Excluded', NULL, NULL, 0, 3, NULL, 0, 0, 1, NULL, NULL, NULL);

-- Create additional field
ALTER TABLE `civicrm_value_uccife_directory_ind`
  ADD COLUMN `position` int(11) DEFAULT NULL;

-- Add additional field definition
SELECT id INTO @ind FROM `civicrm_custom_group` WHERE `name` = 'UCCIFE_Directory_Ind';
INSERT INTO `civicrm_custom_field` (`custom_group_id`, `name`, `label`, `data_type`, `html_type`, `default_value`, `is_required`, `is_searchable`, `is_search_range`, `weight`, `help_pre`, `help_post`, `mask`, `attributes`, `javascript`, `is_active`, `is_view`, `options_per_line`, `text_length`, `start_date_years`, `end_date_years`, `date_format`, `time_format`, `note_columns`, `note_rows`, `column_name`, `option_group_id`, `filter`) VALUES
  (@ind, 'Position', 'Position', 'Int', 'Select', NULL, 0, 0, 0, 1, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 255, NULL, NULL, NULL, NULL, 60, 4, 'position', @opt, NULL);