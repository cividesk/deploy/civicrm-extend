<?php
function civicrm_api3_uccife_getmemberships($params)
{
  $sql = "
SELECT
    id as Id, name as Nom, description as Description,
    CONCAT(duration_interval, ' ', duration_unit) as Duree,
    IF(relationship_type_id,1,0) as IsCorporate
FROM  civicrm_membership_type
WHERE is_active=1";
  $dao = new CRM_Core_DAO();
  $dao->query($sql);
  while ($dao->fetch()) {
    $row = $dao->toArray();
    $result[] = $row;
  }
  return civicrm_api3_create_success($result);
}
