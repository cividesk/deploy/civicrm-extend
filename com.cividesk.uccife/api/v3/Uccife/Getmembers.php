<?php
/*
 * URLs for service:
 * https://<customer>.cividesk.com/crm/extern/rest.php?key=tULKTiKmqfq0ZhDWSyKZQp13&api_key=c1v1d3skcr0nk3y&entity=Uccife&action=getmembers&type=individual
 * https://<customer>.cividesk.com/crm/extern/rest.php?key=tULKTiKmqfq0ZhDWSyKZQp13&api_key=c1v1d3skcr0nk3y&entity=Uccife&action=getmembers&type=organization
 * https://<customer>.cividesk.com/crm/extern/rest.php?key=tULKTiKmqfq0ZhDWSyKZQp13&api_key=c1v1d3skcr0nk3y&entity=Uccife&action=getmemberships
 *
 * Sync happens every day at 5:00am Paris time (according to Pascal).
 */
function civicrm_api3_uccife_getmembers($params)
{
  if (!isset($_SERVER['DOMAIN'])) {
    return civicrm_api3_create_error('The SUBDOMAIN environment variable is not set');
  }
  if (!isset($params['type'])) {
    return civicrm_api3_create_error('Please indicate the type of members you want to list');
  }

  $version = CRM_Uccife_Upgrader::instance()->getCurrentRevision();

  if ($params['type'] == 'individual') {
    $query['select'] = "
      contact.id as ContactId,
      contact.prefix_id as Civilite,
      contact.last_name as Nom,
      contact.first_name as Prenom,
      contact.job_title as Fonction,
      phone_main.phone as Telephone,
   ".($_SERVER['SUBDOMAIN'] == 'mfcci' ? 'NULL' : 'phone_cell.phone')." as TelephonePortable,
      phone_fax.phone as Fax,
      email.email as Courriel,
      value_uccife_directory_ind.password as MotDePasse,
      IF(membership.owner_membership_id,org_membership.contact_id,'') as OrganizationId,
      IF(membership.owner_membership_id,'',value_uccife_directory_org.industry_code) as SecteurActivites,
      IF(membership.owner_membership_id,'',membership.membership_type_id) as MembershipId,
      IF(membership.owner_membership_id,'',0) as Priorite,
      '' as Employeur,
      '' as Description,
      file.uri as PhotoURL
    ";
    $query['from'] = "
      civicrm_membership membership
      INNER JOIN civicrm_contact contact
              ON contact.id = membership.contact_id
      LEFT JOIN civicrm_membership_type membership_type
              ON membership_type.id = membership.membership_type_id
      LEFT JOIN civicrm_membership_status membership_status
              ON membership_status.id = membership.status_id
      LEFT JOIN civicrm_phone phone_main
              ON contact.id = phone_main.contact_id AND phone_main.is_primary = 1 AND phone_main.phone_type_id != 2
      LEFT JOIN civicrm_phone phone_cell
              ON contact.id = phone_cell.contact_id AND phone_cell.phone_type_id = 2
      LEFT JOIN civicrm_phone phone_fax
              ON contact.id = phone_fax.contact_id AND phone_fax.phone_type_id = 3
      LEFT JOIN civicrm_email email
              ON contact.id = email.contact_id AND email.is_primary = 1
      LEFT JOIN civicrm_value_uccife_directory_ind value_uccife_directory_ind
              ON value_uccife_directory_ind.entity_id = contact.id
      LEFT JOIN civicrm_membership org_membership
              ON org_membership.id = membership.owner_membership_id
      LEFT JOIN civicrm_value_uccife_directory_org value_uccife_directory_org
              ON value_uccife_directory_org.entity_id = contact.employer_id
      LEFT JOIN civicrm_file file ON file.id = value_uccife_directory_ind.photo
      ";
    $query['where'] = "
      membership.is_test = 0
      AND membership_status.is_current_member = 1
      AND contact.contact_type = 'Individual'
      AND contact.is_deleted = 0";
    $query['having'] = $query['order'] = '';

    // Add position
    if (in_array($_SERVER['DOMAIN'], array('cfmci', 'ccfb'))) {
      // If position is NOT NULL use its value, otherwise inherited members will be hidden (-1) and primary members shown (0)
      $query['select'] .= ',' . PHP_EOL . 'IFNULL(value_uccife_directory_ind.position, IF(membership.owner_membership_id,-1,0)) as position';
      $query['having'] .= 'position >= 0';
      $query['order']  .= 'position DESC';
    }

    // Protect against LEFT JOINs being too prolific
    $query['group'] = 'ContactId';
  } elseif ($params['type'] == 'organization') {
    $query['select'] = " DISTINCT
      membership.contact_id as OrganizationId,
      contact.organization_name as NomAdherent,
      address.street_address as Adresse,
      address.state_province_id as Etat,
      \"\" as BP,
      address.postal_code as CodePostal,
      address.city as City,
      address.country_id as Pays,
      website.url as SiteWeb,
      value_uccife_directory_org.industry_code as SecteurActivites,
      value_uccife_directory_org.description as Description,
      membership.membership_type_id as MembershipId,
      0 as Priorite";
    $query['from'] = "
      civicrm_membership membership
      INNER JOIN civicrm_contact contact
              ON contact.id = membership.contact_id
      LEFT JOIN civicrm_membership_type membership_type
              ON membership_type.id = membership.membership_type_id
      LEFT JOIN civicrm_membership_status membership_status
              ON membership_status.id = membership.status_id
      LEFT JOIN civicrm_address address
              ON contact.id = address.contact_id AND address.is_primary = 1
      LEFT JOIN civicrm_website website
              ON website.contact_id = contact.id
      LEFT JOIN civicrm_value_uccife_directory_org value_uccife_directory_org
              ON value_uccife_directory_org.entity_id = contact.id";
    $query['where'] = "
      membership.is_test = 0
      AND membership_status.is_current_member = 1
      AND contact.contact_type = 'Organization'
      AND contact.is_deleted = 0";
    $query['having'] = $query['order'] = '';

    // Add logo field
    if ($version) {
      $query['select'] .= ',' . PHP_EOL . 'file.uri as LogoURL';
      $query['from']   .= PHP_EOL . 'LEFT JOIN civicrm_file file ON file.id = value_uccife_directory_org.logo';
    } else {
      $query['select'] .= ',' . PHP_EOL . 'NULL as LogoURL';
    }

    // Protect against LEFT JOINs being too prolific
    $query['group'] = 'OrganizationId';
  } else {
    return civicrm_api3_create_error('Invalid member type');
  }
  $sql = 'SELECT '.$query['select'].' FROM '.$query['from'].' WHERE '.$query['where']
         .($query['group']?' GROUP BY '.$query['group']:'')
         .($query['having']?' HAVING '.$query['having']:'')
         .($query['order']?' ORDER BY '.$query['order']:'');

  $UCCIFE_prefix_codes = array(1 => '1' /*Mrs*/, 2 => '2' /*Ms*/, 3 => '0' /*Mr*/);
  $UCCIFE_country_codes = uccife_get_country_codes();

  // Get default password from custom fields definition
  $query = "
 SELECT default_value
   FROM civicrm_custom_field
  WHERE custom_group_id = (SELECT id FROM civicrm_custom_group WHERE table_name='civicrm_value_uccife_directory_ind')
        AND name = 'Password'";
  $dao = CRM_Core_DAO::executeQuery($query);
  $default_password = ($dao->fetch() ? $dao->default_value : '');
  if (empty($default_password)) {
    // If there is no default password, select one randomly so we do not have open accounts
    $default_password = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890"), 0, 8);
  }

  $result = array();
  $dao = new CRM_Core_DAO();
  $dao->query($sql);
  while ($dao->fetch()) {
    $row = $dao->toArray();
    if ($params['type'] == 'individual') {
      // Remove sort and other temporary fields
      unset($row['position']);

      // Make sure we have the right set of fields
      // DO NOT unset() as all fields need to be in the export file
      // cf. Pascal Ameline, 1/13/2014
      // Pour pouvoir exécuter la synchro nous avons besoin que tous les enregistrements dans les 2 fichiers XML
      // contiennent tous les champs même vides. En effet, nous nous basons sur le 1er enregistrement de chaque
      // fichier pour effectuer notre mapping entre vos champs et nos champs. Si des champs sont supprimés alors
      // le mapping n’est plus possible dès le 1er enregistrement et la synchro n’est alors plus possible.
      if (empty($row['OrganizationId'])) {
        $row['MembreIndividuel'] = 1;
        // Individual Membership: already blank
      } else {
        $row['MembreIndividuel'] = 0;
        // Corporate Membership: blank fields
        $row['SecteurActivites'] = $row['Description'] = $row['MembershipId'] = $row['Priorite'] = '';
      }
    }
    // fix enums
    if (array_key_exists('Civilite', $row))
      $row['Civilite'] = $UCCIFE_prefix_codes[$row['Civilite']];
    if (array_key_exists('Pays', $row))
      $row['Pays'] = $UCCIFE_country_codes[$row['Pays']];
    if (array_key_exists('Etat', $row) && ($value = $row['Etat']))
      $row['Etat'] = CRM_Core_PseudoConstant::stateProvince($value, false);
    // fix empty values
    if (array_key_exists('MotDePasse', $row) && empty($row['MotDePasse']))
      $row['MotDePasse'] = $default_password;
    if (array_key_exists('Telephone', $row) && empty($row['Telephone']))
      $row['Telephone'] = 'n/a';
    if (array_key_exists('TelephonePortable', $row) && empty($row['TelephonePortable']))
      $row['TelephonePortable'] = 'n/a';
    // reformat to standards
    if (array_key_exists('SiteWeb', $row) && ($value = $row['SiteWeb'])) {
      if (substr($value, 0, 7) == 'http://')
        $row['SiteWeb'] = substr($row['SiteWeb'], 7);
      if (substr($value, -1) == '/')
        $row['SiteWeb'] = substr($row['SiteWeb'], 0, -1);
    }
    // Add uri prefix for file fields
    $config = CRM_Core_Config::singleton();
    $customDir = str_replace('/persist/contribute', '/custom', $config->imageUploadDir);
    $filesDir = str_replace('/persist/contribute', '/files', $config->imageUploadDir);
    $filesURL = str_replace('/persist/contribute', '/files', $config->imageUploadURL);
    foreach (array('PhotoURL', 'LogoURL') as $field) {
      if (CRM_Utils_Array::value($field, $row)) {
        // We need to copy the file to /files since /custom is now protected
        copy($customDir . $row[$field], $filesDir . $row[$field]);
        $row[$field] = $filesURL . $row[$field];
      }
    }
    // add to result array
    $result[] = $row;
  }
  return civicrm_api3_create_success($result);
}

function uccife_get_country_codes() {
  // Generated from the SQL Generator in civicrm-import
  return array(
    1196 => 11, // Afrique du Sud
    1002 => 273, // Albanie
    1003 => 12, // Algérie
    1082 => 13, // Allemagne
    1010 => 14, // Argentine
    1087 => 267, // Arabie saoudite
    1011 => 276, // Arménie
    1013 => 3, // Australie
    1014 => 15, // Autriche
    1017 => 16, // Bangladesh
    1020 => 2, // Belgique
    1016 => 258, // Bahrain
    1029 => 18, // Brésil
    1033 => 19, // Bulgarie
    1037 => 20, // Cambodge
    1039 => 7, // Canada
    1044 => 21, // Chili
    1045 => 22, // Chine
    1048 => 23, // Colombie
    1115 => 24, // Corée du Sud
    1053 => 25, // Costa Rica
    1054 => 26, // Cote d\'Ivoire
    1056 => 27, // Cuba
    1059 => 28, // Danemark
    1065 => 30, // Egypte
    1066 => 31, // El Salvador
    1225 => 29, // Emirats Arabes Unis
    1064 => 32, // Equateur
    1198 => 33, // Espagne
    1227 => 79, // Etats-Unis d\'Amérique
    1228 => 79, // Etats-Unis d\'Amérique
    1075 => 34, // Finlande
    1076 => 228, // France
    1081 => 246, // Géorgie
    1083 => 35, // Ghana
    1226 => 36, // Grande Bretagne
    1085 => 37, // Grèce
    1094 => 38, // Haïti
    1098 => 91, // Hong-Kong
    1099 => 39, // Hongrie
    1101 => 40, // Inde
    1102 => 41, // Indonésie
    1103 => 42, // Iran
    1105 => 252, // Irlande
    1100 => 43, // Islande
    1106 => 44, // Israël
    1107 => 45, // Italie
    1109 => 46, // Japon
    1110 => 47, // Jordanie
    1116 => 279, // Koweït
    1119 => 48, // Lituanie
    1126 => 49, // Luxembourg
    1128 => 242, // Macédoine
    1129 => 50, // Madagascar
    1131 => 51, // Malaisie
    1134 => 52, // Malte
    1146 => 1, // Maroc
    1138 => 231, // Maurice
    1140 => 53, // Mexique
    1142 => 234, // Moldavie
    1143 => 261, // Monaco
    1035 => 270, // Myanmar
    1155 => 255, // Nicaragua
    1157 => 54, // Nigeria
    1161 => 55, // Norvège
    1153 => 282, // Nouvelle-Calédonie
    1154 => 56, // Nouvelle-Zélande
    1163 => 57, // Pakistan
    1168 => 58, // Paraguay
    1152 => 59, // Pays-Bas
    1169 => 60, // Pérou
    1170 => 61, // Philippines
    1172 => 62, // Pologne
    1173 => 63, // Portugal
    1175 => 264, // Quatar
    1050 => 64, // Rép. Dém. du Congo
    1062 => 99, // République dominicaine
    1058 => 65, // République Tchèque
    1176 => 66, // Roumanie
    1177 => 67, // Russie
    1183 => 236, // Saint-Pierre-et-Miquelon
    1242 => 239, // Serbie
    1191 => 68, // Singapour
    1192 => 69, // Slovaquie
    1204 => 70, // Suède
    1205 => 71, // Suisse
    1208 => 72, // Taiwan
    1211 => 73, // Thaïlande
    1218 => 74, // Tunisie
    1219 => 75, // Turquie
    1224 => 249, // Ukraine
    1229 => 76, // Uruguay
    1232 => 78, // Venezuela
    1233 => 77, // Vietnam
  );
}
