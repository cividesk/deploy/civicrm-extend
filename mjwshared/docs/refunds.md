# Refunds UI (experimental)

There is a refunds UI available for payments:
![Refund UI](images/refundui.png)

To access the refund click the "undo" icon by the payment:
![Refund icon](images/refundpaymenticon.png)

It is enabled by default via the setting `mjwshared_refundpaymentui` which can be found at
*Administer->CiviContribute->Stripe Settings: Enable refund payment via UI?*

It allows you to issue refunds for `Completed` payments if the payment processor supports it (eg. Stripe).

It also allows you to choose whether to cancel the **event registration** if there are any linked to the contribution (via line-items).

To access the refunds UI you must have **edit contributions** permission.

## Support for cancelling memberships

If you'd like to see support for cancelling memberships when issuing a refund please <a href="https://www.mjwconsult.co.uk/en/civicrm/pcp/info/?reset=1&id=6">contribute via this page</a>.
