<?php return array(
    'root' => array(
        'name' => 'civicrm/mjwshared',
        'pretty_version' => '1.2.5',
        'version' => '1.2.5.0',
        'reference' => '025dc039f27bc9624834e72ee268142200bd2e67',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'civicrm/mjwshared' => array(
            'pretty_version' => '1.2.5',
            'version' => '1.2.5.0',
            'reference' => '025dc039f27bc9624834e72ee268142200bd2e67',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
