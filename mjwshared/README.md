# mjwshared

This extension does nothing by itself but is required by a number of other extensions developed by MJW Consulting.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

**Always read the [Release Notes](https://lab.civicrm.org/extensions/mjwshared/blob/master/docs/releasenotes.md) carefully before upgrading!**

## Installation

See: https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/#installing-a-new-extension

## Support and Maintenance
This extension is supported and maintained by:

[![MJW Consulting](docs/images/mjwconsulting.jpg)](https://www.mjwconsult.co.uk)

We offer paid [support and development](https://mjw.pt/support) as well as a [troubleshooting/investigation service](https://mjw.pt/investigation).
