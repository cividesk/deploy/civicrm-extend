# ElasticEmail Specific Configuration

ATTENTION: the HTTP Web Notifications (Webhooks) feature is only available in the Unlimited Pro and Email API PRO plans. This feature is needed for this extension, so make sure your plan includes it. Source: https://help.elasticemail.com/en/articles/2376855-how-to-manage-http-web-notifications-webhooks

## Settings in ElasticEmail
Log in to ElasticEmail, go to the Settings menu, then select the Notifications tab.

Create a new webhook by clicking on the (+) button on the right side and give it a name. For the notification link, copy the URL displayed in the CiviCRM setup screen for the extension, and select the Bounce/Error event.
