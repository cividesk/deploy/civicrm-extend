<?php
/*
 +--------------------------------------------------------------------------+
 | Copyright IT Bliss LLC (c) 2012-2013                                     |
 +--------------------------------------------------------------------------+
 | This program is free software: you can redistribute it and/or modify     |
 | it under the terms of the GNU Affero General Public License as published |
 | by the Free Software Foundation, either version 3 of the License, or     |
 | (at your option) any later version.                                      |
 |                                                                          |
 | This program is distributed in the hope that it will be useful,          |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of           |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            |
 | GNU Affero General Public License for more details.                      |
 |                                                                          |
 | You should have received a copy of the GNU Affero General Public License |
 | along with this program.  If not, see <http://www.gnu.org/licenses/>.    |
 +--------------------------------------------------------------------------+
*/

class CRM_Callerid_Page_Lookup extends CRM_Core_Page {

  function run() {
    $phonesearch = CRM_Utils_Request::retrieve('phonesearch', 'Positive', CRM_Core_DAO::$_nullObject, FALSE, 0 );
    echo "name|phone\n";
    if ( $phonesearch ) {
      $sql = "select display_name, phone_numeric from civicrm_phone p inner join civicrm_contact c on ( c.id = p.contact_id ) where p.phone_numeric = '{$phonesearch}' ";
      $dao = CRM_Core_DAO::executeQuery($sql);
      if( $dao->fetch()) {
        echo "{$dao->display_name}|{$dao->phone_numeric}\n";
      }
    }
    CRM_Utils_System::civiExit();
  }
}
