<?php return array(
    'root' => array(
        'name' => 'civicrm/noverwrite',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => 'aa9903207cc8387227f52e29214aa6c36300054b',
        'type' => 'civicrm-ext',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'civicrm/noverwrite' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => 'aa9903207cc8387227f52e29214aa6c36300054b',
            'type' => 'civicrm-ext',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
