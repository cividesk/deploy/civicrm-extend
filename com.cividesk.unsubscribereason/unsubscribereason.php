<?php

require_once 'unsubscribereason.civix.php';

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function unsubscribereason_civicrm_config(&$config) {
  _unsubscribereason_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @param array $files
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function unsubscribereason_civicrm_xmlMenu(&$files) {
  _unsubscribereason_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function unsubscribereason_civicrm_install() {
  _unsubscribereason_civix_civicrm_install();
}

/**
* Implements hook_civicrm_postInstall().
*
* @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
*/
function unsubscribereason_civicrm_postInstall() {
  _unsubscribereason_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function unsubscribereason_civicrm_uninstall() {
  _unsubscribereason_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function unsubscribereason_civicrm_enable() {
  _unsubscribereason_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function unsubscribereason_civicrm_disable() {
  _unsubscribereason_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed
 *   Based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function unsubscribereason_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _unsubscribereason_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function unsubscribereason_civicrm_managed(&$entities) {
  _unsubscribereason_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * @param array $caseTypes
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function unsubscribereason_civicrm_caseTypes(&$caseTypes) {
  _unsubscribereason_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function unsubscribereason_civicrm_angularModules(&$angularModules) {
_unsubscribereason_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function unsubscribereason_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _unsubscribereason_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implementation of hook_civicrm_buildForm
 */
function unsubscribereason_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Mailing_Form_Optout' || $formName == 'CRM_Mailing_Form_Unsubscribe') {
    $qid = CRM_Utils_Request::retrieve('qid', 'Integer', $form);
    list($displayName, $email, $contactId) = CRM_Mailing_Event_BAO_Queue::getContactInfo($qid);

    //get pre-configured profile ID and show the fields in the form
    $result = civicrm_api3('UFGroup', 'get', [
      'sequential' => 1,
      'return' => ['id'],
      'name' => 'Unsubscribe_reason_99',
    ]);

    $id = $result['id'];

    $name = 'mailingProfile';
    buildCustom($id, $contactId, $form, $name);
    $profileFields = CRM_Core_BAO_UFGroup::getFields($id, FALSE, CRM_Core_Action::ADD);

    CRM_Core_BAO_UFGroup::setProfileDefaults($contactId, $profileFields, $defaults, TRUE);
    $form->setDefaults(array('email_confirm' => $email));
    foreach($defaults as $key => $val) {
      $form->setDefaults(array($key => $val));
    }
  }
}


/**
 * Implementation of hook_civicrm_postProcess
 */
function unsubscribereason_civicrm_postProcess($formName, &$form) {
  if ($formName == 'CRM_Mailing_Form_Optout' || $formName == 'CRM_Mailing_Form_Unsubscribe') {
    $params = $form->_submitValues;
    //get pre-configured profile ID and save the unsubscribe reasons for the contact
    $result = civicrm_api3('UFGroup', 'get', [
      'sequential' => 1,
      'return' => ['id'],
      'name' => 'Unsubscribe_reason_99',
    ]);
    $id = $result['id'];
    $qid = CRM_Utils_Request::retrieve('qid', 'Integer', $form);

    $profileFields = CRM_Core_BAO_UFGroup::getFields($id, FALSE, CRM_Core_Action::ADD);
    list($displayName, $email, $contactId) = CRM_Mailing_Event_BAO_Queue::getContactInfo($qid);
    CRM_Contact_BAO_Contact::createProfileContact($params, $profileFields, $contactId);
  }
}

  /**
   * Build the petition profile form.
   *
   * @param int $id
   * @param string $name
   * @param bool $viewOnly
   */
 function buildCustom($id, $contactID, $form, $name, $viewOnly = FALSE) {
    if ($id) {
      $fields = NULL;
      // TODO: contactID is never set (commented above)
      if ($contactID) {
        if (CRM_Core_BAO_UFGroup::filterUFGroups($id, $contactID)) {
          $fields = CRM_Core_BAO_UFGroup::getFields($id, FALSE, CRM_Core_Action::ADD);
        }
      }
      else {
        $fields = CRM_Core_BAO_UFGroup::getFields($id, FALSE, CRM_Core_Action::ADD);
      }

      if ($fields) {
        $form->assign($name, $fields);

        foreach ($fields as $key => $field) {
          if ($viewOnly &&
            isset($field['data_type']) &&
            $field['data_type'] == 'File' || ($viewOnly && $field['name'] == 'image_URL')
          ) {
            // ignore file upload fields
            continue;
          }

          // if state or country in the profile, create map
          list($prefixName, $index) = CRM_Utils_System::explode('-', $key, 2);

          CRM_Core_BAO_UFGroup::buildProfile($form, $field, CRM_Profile_Form::MODE_CREATE, $contactID, TRUE);
          $form->_fields[$key] = $field;
        }
      }
    }
  }

/**
 * Functions below this ship commented out. Uncomment as required.
 *

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function unsubscribereason_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function unsubscribereason_civicrm_navigationMenu(&$menu) {
  _unsubscribereason_civix_insert_navigation_menu($menu, NULL, array(
    'label' => ts('The Page', array('domain' => 'com.cividesk.unsubscribereason')),
    'name' => 'the_page',
    'url' => 'civicrm/the-page',
    'permission' => 'access CiviReport,access CiviContribute',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _unsubscribereason_civix_navigationMenu($menu);
} // */
