<?php

require_once 'participantrolevisibility.civix.php';
use CRM_Participantrolevisibility_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function participantrolevisibility_civicrm_config(&$config) {
  _participantrolevisibility_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function participantrolevisibility_civicrm_xmlMenu(&$files) {
  _participantrolevisibility_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function participantrolevisibility_civicrm_install() {
  _participantrolevisibility_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
 */
function participantrolevisibility_civicrm_postInstall() {
  _participantrolevisibility_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function participantrolevisibility_civicrm_uninstall() {
  _participantrolevisibility_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function participantrolevisibility_civicrm_enable() {
  _participantrolevisibility_civix_civicrm_enable();

  // Set the default Visibility Options for Participant Role
  // Get All the Visibility ID from Option Value API
  $participant_role_options = civicrm_api3('OptionValue', 'get', array(
    'sequential' => 1,
    'option_group_id' => "participant_role",
  ));

  if($participant_role_options['is_error'] ==0 && !empty($participant_role_options['values'])) {
    foreach ($participant_role_options['values'] as $values) {
      // set the default value as Admin (visibility Id:2) If visibility is NULL OR 0
      if (empty($values['visibility_id']) || $values['visibility_id'] == 0) {
        $participant_visibility = civicrm_api3('OptionValue', 'create', array(
          'sequential' => 1,
          'option_group_id' => "participant_role",
          'visibility_id' => 2, // Visibility Id = 2 form Admin
          'id' => $values['id']
        ));
      }
    }
  }
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function participantrolevisibility_civicrm_disable() {
  _participantrolevisibility_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function participantrolevisibility_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _participantrolevisibility_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function participantrolevisibility_civicrm_managed(&$entities) {
  _participantrolevisibility_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function participantrolevisibility_civicrm_caseTypes(&$caseTypes) {
  _participantrolevisibility_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_angularModules
 */
function participantrolevisibility_civicrm_angularModules(&$angularModules) {
  _participantrolevisibility_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function participantrolevisibility_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _participantrolevisibility_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_entityTypes
 */
function participantrolevisibility_civicrm_entityTypes(&$entityTypes) {
  _participantrolevisibility_civix_civicrm_entityTypes($entityTypes);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function participantrolevisibility_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function participantrolevisibility_civicrm_navigationMenu(&$menu) {
  _participantrolevisibility_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('New subliminal message'),
    'name' => 'mailing_subliminal_message',
    'url' => 'civicrm/mailing/subliminal',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _participantrolevisibility_civix_navigationMenu($menu);
} // */


/**
 * Implementation of hook_civicrm_buildForm
 */
function participantrolevisibility_civicrm_buildForm($formName, &$form) {
  if($formName == 'CRM_Admin_Form_Options' && $form->getVar('_gName') == 'participant_role')  {

    // Get the Visibility Options from Option Group 37
    $visibilityOptions = civicrm_api3('OptionValue', 'get', array(
      'sequential' => 1,
      'option_group_id' => 37, //Option group id for Visibility.
    ));
    $selectList = array();
    foreach ($visibilityOptions['values'] as $visibilityKey => $visibilityValue) {
      $selectList[$visibilityValue['value']] = $visibilityValue['label'];
    }
    // Add the Visibility dropdown in the form
    $form->addElement('select', 'visibility_id', ts('Visibility'), $selectList, FALSE, array('class' => 'crm-select2'));

    // Add our template to the page. Note our template uses jQuery to reorder/change the form
    CRM_Core_Region::instance('page-body')->add(array('template' => 'ParticipantRoleOption.tpl'));

  }

  // Restrict the roles visibility on Event Registration form
  if($formName == 'CRM_Event_Form_Registration_Register') {
    // Check if Participant Role field added to Registration form
    if($form->elementExists('participant_role')) {
      // Get all the Participant Roles Ids
      $element = &$form->getElement('participant_role');
      if(!empty($element->_options)) {
        // Get the Visibility ID from Option Value API
        //https://projects.cividesk.com/projects/4?modal=Task-5896-4
        $participant_role_options = civicrm_api3('OptionValue', 'get', array(
          'sequential' => 1,
          'option_group_id' => "participant_role",
          'options' => ['limit' => 0],
        ));

        $adminRoles = array();
        if($participant_role_options['is_error'] ==0 && !empty($participant_role_options['values'])) {
          foreach($participant_role_options['values'] as $values) {
            // Visibility Options : Admin = 2 Public =1
            if($values['visibility_id'] == 2) {
              // Collect all the Role with Admin visibility
              $adminRoles[] = $values['value'];
            }
          }
        }
        // Hide/Unset the option of 'Participant Role' drop-down if the Role has Admin visibility
        foreach($element->_options as $key=>$val) {
          if(!empty($val['attr']['value']) && in_array($val['attr']['value'], $adminRoles)) {
            unset($element->_options[$key]);
          }
        }
        // Reset the array element
        $element->_options = array_values($element->_options);
      }
    }
  }
}
