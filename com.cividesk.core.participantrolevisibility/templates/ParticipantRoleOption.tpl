<table id="participant_visibility_tbl" style="display: none">
<tr class="crm-admin-options-form-block-participant_visibility" id="participant_role_tr">
    <td class="label"><label for="visibility_id">Visibility</label></td>
    <td>
        {$form.visibility_id.html}
        <br/><span class="description">If you allow users to select a Participant Role by including that field on a profile - only statuses with 'Public' visibility are listed.</span>
    </td>
</tr>
</table>

{literal}
<script type="text/javascript">
  cj('#participant_role_tr').insertAfter('tr.crm-admin-options-form-block-is_active');

</script>
{/literal}
