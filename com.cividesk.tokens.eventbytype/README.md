# com.cividesk.tokens.eventbytype


CiviDesk extension for Eventbytype Tokens that provides its users extra tokens for displaying the events in listing format by type. The user can send emails for all the upcoming training events in the list format with corresponding the dates of the events in the list as well.

The following listing of training event is produced by token {event_list.online_training}
![event listing by type](docs/event_listing.png)

Installation
-----------

Installing this extension is quite easy. You only have to:

1. Visit your `civicrm/admin/extensions` page
2. See what is your extension directory, or create one if it doesn't exist
3. Go to that directory and `git clone https://github.com/cividesk/com.cividesk.tokens.eventbytype.git`
4. Change permissions and ownership of the new directory, if needed
5. Refresh the `civicrm/admin/extensions` page
6. Click on "install"



