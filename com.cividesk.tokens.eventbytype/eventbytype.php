<?php

require_once 'eventbytype.civix.php';
use CRM_Eventbytype_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function eventbytype_civicrm_config(&$config) {
  _eventbytype_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function eventbytype_civicrm_xmlMenu(&$files) {
  _eventbytype_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function eventbytype_civicrm_install() {
  _eventbytype_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
 */
function eventbytype_civicrm_postInstall() {
  _eventbytype_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function eventbytype_civicrm_uninstall() {
  _eventbytype_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function eventbytype_civicrm_enable() {
  _eventbytype_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function eventbytype_civicrm_disable() {
  _eventbytype_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function eventbytype_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _eventbytype_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function eventbytype_civicrm_managed(&$entities) {
  _eventbytype_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function eventbytype_civicrm_caseTypes(&$caseTypes) {
  _eventbytype_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_angularModules
 */
function eventbytype_civicrm_angularModules(&$angularModules) {
  _eventbytype_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function eventbytype_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _eventbytype_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_entityTypes
 */
function eventbytype_civicrm_entityTypes(&$entityTypes) {
  _eventbytype_civix_civicrm_entityTypes($entityTypes);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function eventbytype_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function eventbytype_civicrm_navigationMenu(&$menu) {
  _eventbytype_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('New subliminal message'),
    'name' => 'mailing_subliminal_message',
    'url' => 'civicrm/mailing/subliminal',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _eventbytype_civix_navigationMenu($menu);
} // */

/**
 * Implementation of hook_civicrm_tokens
 */
function eventbytype_civicrm_tokens( &$tokens ) {
  // Get all the Events types from API
  $result = civicrm_api3('OptionValue', 'get', array(
    'sequential' => 1,
    'return' => array("name"),
    'option_group_id' => "event_type",
  ));

  // Create the array of event token list to categorised the Event listing by types
  if ($result['is_error'] == 0 && !empty($result['values'])) {
    foreach ($result['values'] as $event_type) {
      $event_type_name = $event_type['name'];
      $event_type_id = strtolower(str_replace(' ', '_', $event_type['name']));
      $tokens['event_list']['event_list.' . $event_type_id] = ts('Events List of ' . $event_type['name']);
    }
  }
}

/**
 * Implementation of hook_civicrm_tokenValues
 */
function eventbytype_civicrm_tokenValues( &$values, &$contactIDs, $jobID = null, $tokens = array(), $context, $componentID = null ) {
  if (is_array($contactIDs)) {
    $contactIDString = implode(',', array_values($contactIDs));
  } else {
    $contactIDString = "$contactIDs";
  }

  // Build the token values as per the contact id(s).
  if (array_key_exists('event_list', $tokens)) {
    if (!is_array($contactIDs)) {
      $crmContactIDs = array($contactIDs);
    } else {
      $crmContactIDs = array_values($contactIDs);
    }
    // Create token value for each contact id(s)
    foreach ($crmContactIDs as $crmContactID) {
      foreach ($tokens['event_list'] as $customTokenKey => $customToken) {
        $event_type_id = $customToken;
        // checking the keys here because at getting the inconsistency in getting the $tokens array
        if(!is_numeric($customTokenKey)) {
          $event_type_id = $customTokenKey;
        }
        $event_type = ucwords(str_replace('_', ' ', $event_type_id));
        // Assign the actual value of token to the token array
        $values[$crmContactID]['event_list.'.$event_type_id] = _get_event_list_by_type($event_type);
      }
    }
  }
}

// Custom function to get Events List (Upcoming ONLY) By Event Type
function _get_event_list_by_type( $event_type ) {
  global $base_url;
  if (empty($event_type)) {
    return;
  }

  // Get the event details of upcomming events only as per the event types
  $result = civicrm_api3('Event', 'get', array(
    'sequential' => 1,
    'return' => array("title", "start_date", "end_date", "id"),
    'start_date' => array('>=' => "today"),
    'is_active' => 1,
    'is_public' => 1,
    'options' => array('sort' => "start_date asc"),
    'event_type_id' => $event_type,
  ));

  // Create the bullet list of all Event title & Event start date (eg:Monday,June 26,2019 - 10.00pm (MT) )
  if ($result['is_error'] == 0 && !empty($result['values'])) {
    $body = "";
    $body .= "<ul>";
    foreach($result['values'] as $event) {
      $event_url = $base_url . "/civicrm/event/info?id=".$event['id'];
      $from = date_format(date_create($event['start_date']), 'l, F d, Y - g:ia')." (MT)";
      $event_date_range = $from;
      $body .= "<li><a href='$event_url'><strong>". $event['title'] . "</strong></a>" . "<br/><br/>" . $event_date_range ."</li><hr/>";
    }
    $body .= "</ul>";

    return $body;
  }
}
