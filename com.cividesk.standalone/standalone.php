<?php
/*
 +--------------------------------------------------------------------------+
 | Copyright IT Bliss LLC (c) 2017-2018                                     |
 +--------------------------------------------------------------------------+
 | This program is free software: you can redistribute it and/or modify     |
 | it under the terms of the GNU Affero General Public License as published |
 | by the Free Software Foundation, either version 3 of the License, or     |
 | (at your option) any later version.                                      |
 |                                                                          |
 | This program is distributed in the hope that it will be useful,          |
 | but WITHOUT ANY WARRANTY; without even the implied warranty of           |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            |
 | GNU Affero General Public License for more details.                      |
 |                                                                          |
 | You should have received a copy of the GNU Affero General Public License |
 | along with this program.  If not, see <http://www.gnu.org/licenses/>.    |
 +--------------------------------------------------------------------------+
*/

/**
 * Implementation of hook_config
 *
 */
function standalone_civicrm_config(&$config) {
  global $rid2cid, $locked_permissions;

  // Read the permissions.csv file and map it in memory
  $fp = fopen(__DIR__ . '/permissions.csv', 'r');
  fgetcsv($fp, 1000); // discard 1st line with role names
  // Create the role_id to column_id translation array
  $line = fgetcsv($fp, 1000);
  array_shift($line);
  $rid2cid = array_flip($line);
  // Read an store the permissions
  while (($line = fgetcsv($fp, 1000)) !== FALSE) {
    $permission = array_shift($line);
    $locked_permissions[$permission] = $line;
  }
}

/**
 * Implementation of hook_permission_check
 *
 */
function standalone_civicrm_permission_check( $permission, &$granted ) {
  global $user, $rid2cid, $locked_permissions;

  // Check if the permission is managed, and then check if granted
  if ($allowed_rids = CRM_Utils_Array::value($permission, $locked_permissions)) {
    $granted = 0; // permission is managed, so disallowed by default
    if ($user && $user->roles) {
      foreach ($user->roles as $rid => $roleName) {
        $col = CRM_Utils_Array::value($rid, $rid2cid);
        if (is_numeric($col) && CRM_Utils_Array::value($col, $allowed_rids)) {
          $granted = 1;
          break;
        }
      }
    }
  }
}
