# Recurring Buttons

![Screenshot](/images/screenshot.png)

This extension converts the donation recurrence checkbox option into radio buttons.
This makes it easier to theme the form in a more esthetic way, and also helps put
more emphasize on the recurring donation options (which are otherwise easy to ignore).

Initially developed by Roshani Kothari
([@roshani](https://chat.civicrm.org/civicrm/messages/@roshani)), with funding of the [Plastic
Pollution Coalition](https://www.plasticpollutioncoalition.org/) and a bit of help from
[Coop Symbiotic](https://www.symbiotic.coop/en).

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.2+
* CiviCRM 5.latest

## Installation

Install as a regular CiviCRM extension.

## Usage

Once enabled, the extension automatically converts the recurring checkbox
into radio buttons.

This extension supports the [radiobuttons](https://lab.civicrm.org/extensions/radiobuttons)
extension, which provides an alternative visual for the buttons (or at least make them easier
to theme).

![Screenshot](/images/screenshot2.png)

## Known Issues

https://lab.civicrm.org/extensions/recurringbuttons/issues

Notable issues:

* The extension supports multiple recurring units (daily, weekly, monthly, yearly), but not custom units (is that even possible?).
* The extension does not support recurring intervals (ex: "every 3 months"). It has not been tested at all.
* Does not support recurring installments (ex: "donate for 6 months"). It has not been tested either.
* If you select to default to a recurring payment, the extension will default to the first recurring unit, unless a query-string argument was passed, such as '&frequency_unit=week', then the page will automatically select the recurring/weekly options.

## Support

Please post bug reports in the issue tracker of this project on CiviCRM's Gitlab:  
https://lab.civicrm.org/extensions/recurringbuttons/issues

This extension was written thanks to the financial support of organisations
using it, as well as the very helpful and collaborative CiviCRM community.

While we do our best to provide free community support for this extension,
please consider financially contributing to support or development of this
extension.

Support via Coop Symbiotic:  
https://www.symbiotic.coop/en

Coop Symbiotic is a worker-owned co-operative based in Canada. We have a strong
experience working with non-profits and CiviCRM. We provide affordable, fast,
turn-key hosting with regular upgrades and proactive monitoring, as well as custom
development and training.
