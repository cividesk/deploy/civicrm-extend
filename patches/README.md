| Patch | Description | Extension PR | Merge Status |
|---------|---------|---------|---------|
| change_selection_error.patch | Support for PHP 8 in cividiscount | https://lab.civicrm.org/extensions/cividiscount/-/merge_requests/280 |merged later than 3.8.7 |
| birthday_report_add_phone_column.patch | Support for phone field in report | https://github.com/systopia/de.systopia.birthdays/pull/21 |merged later than 1.5.0 |
| discount_compatibility.patch | Fix discount compatibilty in 5.46 | https://lab.civicrm.org/extensions/cividiscount/-/merge_requests/278 |merged later than 3.8.7|
| percentagepricesetfield_amount_display.patch | Fix #53: duplicate total display for alternate currency formats | https://github.com/twomice/com.joineryhq.percentagepricesetfield/commit/68a91b9322424750c02f657836fd7e7c3b47f5e6 |merged (not released)|
| extended_report_test_registration.patch | Add participant filter for is_test| https://github.com/eileenmcnaughton/nz.co.fuzion.extendedreport/pull/552| merged|
| extended_report_hook.patch | make the hook usable by passing the sql| https://github.com/eileenmcnaughton/nz.co.fuzion.extendedreport/pull/508| merged|
| extended_report_core_compatibility.patch | fix compatibility with core (5.46+)| | merged|
| contactlayout.patch | cividesk specific changes only| https://projects.cividesk.com/projects/7/tasks/5853 | portforward the patch|
| mailchimp_sync_backward_compatibility.patch | fix compatibility with core (5.46+)| | no need to port forward in next upgrade|
| patches/mosaico-549-improve-performance.patch | fix performance issues on WordPress | https://github.com/veda-consulting-company/uk.co.vedaconsulting.mosaico/releases/tag/2.11 | merged into mosaico 2.11 |
| patches/mosaico-568-wordpress-img-fix.patch | fix performance issues on WordPress | https://github.com/veda-consulting-company/uk.co.vedaconsulting.mosaico/releases/tag/2.11 | merged into mosaico 2.11 |
| patches/mosaico-569-standard-pragma-header.patch | fix performance issues on WordPress | https://github.com/veda-consulting-company/uk.co.vedaconsulting.mosaico/releases/tag/2.11 | merged into mosaico 2.11 |