<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */

use Civi\Api4\ContributionRecur;
use Civi\Payment\Exception\PaymentProcessorException;
use Civi\Payment\PropertyBag;
use CRM_AuthNetEcheck_ExtensionUtil as E;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use net\authorize\api\constants\ANetEnvironment as AnetEnvironment;

abstract class CRM_Core_Payment_AuthorizeNetCommon extends CRM_Core_Payment {

  use CRM_Core_Payment_MJWTrait;

  const RESPONSECODE_APPROVED = 1;
  const RESPONSECODE_DECLINED = 2;
  const RESPONSECODE_ERROR = 3;
  const RESPONSECODE_REVIEW = 4;
  const AUTHNETECHECK_SKIP_WEBHOOK_CHECKS = 'AUTHNETECHECK_SKIP_WEBHOOK_CHECKS';

  /**
   * @fixme: Confirm that this is the correct "timezone" - we copied this from the original core Authorize.net processor.
   * @var string
   */
  const TIMEZONE = 'America/Denver';

  /**
   * Constructor
   *
   * @param string $mode the mode of operation: live or test
   */
  public function __construct($mode, &$paymentProcessor) {
    $this->_paymentProcessor = $paymentProcessor;
  }

  /**
   * @param array $paymentProcessor
   *
   * @return string
   */
  public static function getApiLoginId($paymentProcessor) {
    return trim($paymentProcessor['user_name'] ?? '');
  }

  /**
   * @param array $paymentProcessor
   *
   * @return string
   */
  public static function getTransactionKey($paymentProcessor) {
    return trim($paymentProcessor['password'] ?? '');
  }

  /**
   * @param array $paymentProcessor
   *
   * @return string
   */
  public static function getSignature($paymentProcessor) {
    return trim($paymentProcessor['signature'] ?? '');
  }

  /**
   * This function checks to see if we have the right config values.
   *
   * @return string
   *   the error message if any
   */
  public function checkConfig() {
    $error = [];
    if (!empty($error)) {
      return implode('<p>', $error);
    }
    else {
      return NULL;
    }
  }

  /**
   * Should the first payment date be configurable when setting up back office recurring payments.
   * In the case of Authorize.net this is an option
   * @return bool
   */
  protected function supportsFutureRecurStartDate() {
    return TRUE;
  }

  /**
   * Can recurring contributions be set against pledges.
   *
   * In practice all processors that use the baseIPN function to finish transactions or
   * call the completetransaction api support this by looking up previous contributions in the
   * series and, if there is a prior contribution against a pledge, and the pledge is not complete,
   * adding the new payment to the pledge.
   *
   * However, only enabling for processors it has been tested against.
   *
   * @return bool
   */
  protected function supportsRecurContributionsForPledges() {
    return TRUE;
  }

  /**
   * We can use the processor on the backend
   * @return bool
   */
  public function supportsBackOffice() {
    return TRUE;
  }

  public function supportsRecurring() {
    return TRUE;
  }

  /**
   * We can edit recurring contributions
   * @return bool
   */
  public function supportsEditRecurringContribution() {
    return TRUE;
  }

  /**
   * Submit a payment using Advanced Integration Method.
   *
   * Sets appropriate parameters and calls Smart Debit API to create a payment
   *
   * Payment processors should set payment_status_id.
   *
   * @param array|\Civi\Payment\PropertyBag $params
   *   Assoc array of input parameters for this transaction.
   *
   * @param string $component
   *
   * @return array
   *   Result array
   *
   * @throws \CiviCRM_API3_Exception
   */
  public function doPayment(&$params, $component = 'contribute') {
    /* @var \Civi\Payment\PropertyBag $propertyBag */
    $propertyBag = $this->beginDoPayment($params);

    $zeroAmountPayment = $this->processZeroAmountPayment($propertyBag);
    if ($zeroAmountPayment) {
      return $zeroAmountPayment;
    }

    // @todo From here on we are using the array instead of propertyBag. To be converted later...
    $params = $this->getPropertyBagAsArray($propertyBag);

    if ($propertyBag->getIsRecur() && $propertyBag->has('contributionRecurID')) {
      return $this->doRecurPayment($params, $propertyBag);
    }

    // Authorize.Net will not refuse duplicates, so we should check if the user already submitted this transaction
    if ($this->checkDupe($this->getInvoiceNumber($propertyBag), $params['contributionID'] ?? NULL)) {
      $this->handleError(
        9004,
        E::ts('It appears that this transaction is a duplicate.  Have you already submitted the form once?  If so there may have been a connection problem.  Check your email for a receipt from Authorize.net.  If you do not receive a receipt within 2 hours you can try your transaction again.  If you continue to have problems please contact the site administrator.'),
        $propertyBag->getCustomProperty('error_url')
      );
    }

    $merchantAuthentication = $this->getMerchantAuthentication();
    $order = $this->getOrder($propertyBag);

    $customerData = $this->getCustomerDataType($params);
    $customerAddress = $this->getCustomerAddress($params);

    //create a bank debit transaction
    $transactionRequestType = new AnetAPI\TransactionRequestType();
    $transactionRequestType->setTransactionType("authCaptureTransaction");
    $transactionRequestType->setAmount($this->getAmount($params));
    $transactionRequestType->setCurrencyCode($this->getCurrency($params));
    $transactionRequestType->setPayment($this->getPaymentDetails($params));
    $transactionRequestType->setOrder($order);
    $transactionRequestType->setBillTo($customerAddress);
    $transactionRequestType->setCustomer($customerData);
    $transactionRequestType->setCustomerIP(CRM_Utils_System::ipAddress());
    $request = new AnetAPI\CreateTransactionRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($this->getInvoiceNumber($propertyBag));
    $request->setTransactionRequest($transactionRequestType);
    $controller = new AnetController\CreateTransactionController($request);
    /** @var \net\authorize\api\contract\v1\CreateTransactionResponse $response */
    $response = $controller->executeWithApiResponse($this->getIsTestMode() ? AnetEnvironment::SANDBOX : AnetEnvironment::PRODUCTION);

    if (!$response || !$response->getMessages()) {
      $this->handleError(NULL, 'No response returned', $propertyBag->getCustomProperty('error_url'));
    }

    $tresponse = $response->getTransactionResponse();

    if ($response->getMessages()->getResultCode() == "Ok") {
      if (!$tresponse) {
        $this->handleError(NULL, 'No transaction response returned', $propertyBag->getCustomProperty('error_url'));
      }
      $this->setPaymentProcessorTrxnID($tresponse->getTransId());
      $this->setPaymentProcessorOrderID($tresponse->getTransId());

      switch ($tresponse->getResponseCode()) {
        case self::RESPONSECODE_APPROVED:
          $params['payment_status_id'] = CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'Completed');
          break;

        case self::RESPONSECODE_DECLINED:
        case self::RESPONSECODE_ERROR:
        if ($tresponse->getErrors()) {
          $this->handleError($tresponse->getErrors()[0]->getErrorCode(), $tresponse->getErrors()[0]->getErrorText(), $propertyBag->getCustomProperty('error_url'));
        }
        else {
          $this->handleError(NULL, 'Transaction Failed', $propertyBag->getCustomProperty('error_url'));
        }
        $params['payment_status_id'] = CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'Failed');
          break;

        case self::RESPONSECODE_REVIEW:
          // Keep it in pending state
          break;

      }
    }
    else {
      // resultCode !== 'Ok'
      $errorCode = NULL;
      if ($tresponse && $tresponse->getErrors()) {
        foreach ($tresponse->getErrors() as $tError) {
          $errorCode = $tError->getErrorCode();
          switch ($errorCode) {
            case '39':
              $errorMessages[] = $errorCode . ': ' . $tError->getErrorText() . ' (' . $this->getCurrency($params) . ')';
              break;

            default:
              $errorMessages[] = $errorCode . ': ' . $tError->getErrorText();
          }
        }
        $errorMessage = implode(', ', $errorMessages);
      }
      elseif ($response->getMessages()) {
        foreach ($response->getMessages()->getMessage() as $rError) {
          $errorMessages[] = $rError->getCode() . ': ' . $rError->getText();
        }
        $errorMessage = implode(', ', $errorMessages);
      }
      else {
        $errorCode = NULL;
        $errorMessage = NULL;
      }
      $this->handleError($errorCode, $errorMessage, $propertyBag->getCustomProperty('error_url'));
    }

    return $this->endDoPayment($params);
  }

  /**
   * Get the merchant authentication for AuthNet
   *
   * @return \net\authorize\api\contract\v1\MerchantAuthenticationType
   */
  protected function getMerchantAuthentication() {
    // Create a merchantAuthenticationType object with authentication details
    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    $merchantAuthentication->setName(self::getApiLoginId($this->_paymentProcessor));
    $merchantAuthentication->setTransactionKey(self::getTransactionKey($this->_paymentProcessor));
    return $merchantAuthentication;
  }

  /**
   * Get the customer address for AuthNet
   *
   * @param array $params
   *
   * @return \net\authorize\api\contract\v1\CustomerAddressType
   */
  protected function getCustomerAddress($params) {
    // Set the customer's Bill To address
    $customerAddress = new AnetAPI\CustomerAddressType();
    isset($params['firstName']) ? $customerAddress->setFirstName($params['firstName']) : NULL;
    isset($params['lastName']) ? $customerAddress->setLastName($params['lastName']) : NULL;
    $customerAddress->setAddress($params['street_address']);
    $customerAddress->setCity($params['city']);
    $customerAddress->setState($params['state_province']);
    $customerAddress->setZip($params['postal_code']);
    $customerAddress->setCountry($params['country']);
    return $customerAddress;
  }

  /**
   * Get the customer data for AuthNet
   *
   * @param array $params
   *
   * @return \net\authorize\api\contract\v1\CustomerDataType
   */
  protected function getCustomerDataType($params) {
    // Set the customer's identifying information
    $customerData = new AnetAPI\CustomerDataType();
    $customerData->setType('individual');
    $customerData->setId($params['contactID']);
    $customerData->setEmail($this->getBillingEmail($params, $params['contactID']));
    return $customerData;
  }

  /**
   * Get the customer data for AuthNet
   *
   * @param array $params
   *
   * @return \net\authorize\api\contract\v1\CustomerType
   */
  protected function getCustomerType($params) {
    // Set the customer's identifying information
    $customerData = new AnetAPI\CustomerType();
    $customerData->setType('individual');
    $customerData->setId($params['contactID']);
    $customerData->setEmail($this->getBillingEmail($params, $params['contactID']));
    return $customerData;
  }

  /**
   * Get the order for AuthNet
   *
   * @param \Civi\Payment\PropertyBag $propertyBag
   *
   * @return \net\authorize\api\contract\v1\OrderType
   */
  protected function getOrder(PropertyBag $propertyBag): AnetAPI\OrderType {
    // Order info
    $order = new AnetAPI\OrderType();
    $order->setInvoiceNumber($this->getInvoiceNumber($propertyBag));
    $order->setDescription($propertyBag->getter('description', TRUE), '');
    return $order;
  }

  /**
   * Get the recurring payment interval for AuthNet
   *
   * @param \Civi\Payment\PropertyBag $params
   *
   * @return \net\authorize\api\contract\v1\PaymentScheduleType\IntervalAType
   * @throws \Civi\Payment\Exception\PaymentProcessorException
   */
  protected function getRecurInterval($params) {
    $intervalLength = $params->getRecurFrequencyInterval();
    $intervalUnit = $params->getRecurFrequencyUnit();
    if ($intervalUnit == 'week') {
      $intervalLength *= 7;
      $intervalUnit = 'days';
    }
    elseif ($intervalUnit == 'year') {
      $intervalLength *= 12;
      $intervalUnit = 'months';
    }
    elseif ($intervalUnit == 'day') {
      $intervalUnit = 'days';
    }
    elseif ($intervalUnit == 'month') {
      $intervalUnit = 'months';
    }

    // interval cannot be less than 7 days or more than 1 year
    if ($intervalUnit == 'days') {
      if ($intervalLength < 7) {
        $this->handleError(NULL, 'Payment interval must be at least one week', $params->getCustomProperty('error_url'));
      }
      elseif ($intervalLength > 365) {
        $this->handleError(NULL, 'Payment interval may not be longer than one year', $params->getCustomProperty('error_url'));
      }
    }
    elseif ($intervalUnit == 'months') {
      if ($intervalLength < 1) {
        $this->handleError(NULL, 'Payment interval must be at least one week', $params->getCustomProperty('error_url'));
      }
      elseif ($intervalLength > 12) {
        $this->handleError(NULL, 'Payment interval may not be longer than one year', $params->getCustomProperty('error_url'));
      }
    }

    $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
    $interval->setLength($intervalLength);
    $interval->setUnit($intervalUnit);
    return $interval;
  }

  /**
   * Get the payment schedule for AuthNet
   *
   * @param \Civi\Payment\PropertyBag $propertyBag
   * @param \net\authorize\api\contract\v1\PaymentScheduleType\IntervalAType|NULL $interval
   *
   * @return \net\authorize\api\contract\v1\PaymentScheduleType
   */
  protected function getRecurSchedule(PropertyBag $propertyBag, AnetAPI\PaymentScheduleType\IntervalAType $interval = NULL): AnetAPI\PaymentScheduleType {
    $paymentSchedule = new AnetAPI\PaymentScheduleType();
    if ($interval) {
      $paymentSchedule->setInterval($interval);
    }

    // for open ended subscription totalOccurrences has to be 9999
    $installments = $propertyBag->has('recurInstallments') ? $propertyBag->getRecurInstallments() : 9999;
    $paymentSchedule->setTotalOccurrences($installments);
    return $paymentSchedule;
  }

  /**
   * @param string $startDateString
   *
   * @return \DateTime
   */
  protected function getRecurStartDate(string $startDateString = 'now'): DateTime {
    //allow for post dated payment if set in form
    $startDate = date_create($startDateString);

    /* Format start date in Mountain Time to avoid Authorize.net error E00017
     * we do this only if the day we are setting our start time to is LESS than the current
     * day in mountaintime (ie. the server time of the A-net server). A.net won't accept a date
     * earlier than the current date on it's server so if we are in PST we might need to use mountain
     * time to bring our date forward. But if we are submitting something future dated we want
     * the date we entered to be respected
     */
    $minDate = date_create('now', new DateTimeZone(self::TIMEZONE));
    if (strtotime($startDate->format('Y-m-d')) < strtotime($minDate->format('Y-m-d'))) {
      $startDate->setTimezone(new DateTimeZone(self::TIMEZONE));
    }
    return $startDate;
  }

  /**
   * Submit an Automated Recurring Billing subscription.
   *
   * @param array $params
   * @param \Civi\Payment\PropertyBag $propertyBag
   *
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  protected function doRecurPayment(array $params, PropertyBag $propertyBag): array {
    $merchantAuthentication = $this->getMerchantAuthentication();
    // Subscription Type Info
    $subscription = new AnetAPI\ARBSubscriptionType();
    $subscription->setName($this->getPaymentDescription($params));
    $interval = $this->getRecurInterval($propertyBag);
    $paymentSchedule = $this->getRecurSchedule($propertyBag, $interval);
    $paymentSchedule->setStartDate($this->getRecurStartDate($params['receive_date'] ?? 'now'));

    $subscription->setPaymentSchedule($paymentSchedule);
    $subscription->setAmount($propertyBag->getAmount());
    $subscription->setPayment($this->getPaymentDetails($params));

    $order = $this->getOrder($propertyBag);
    $subscription->setOrder($order);

    $customerAddress = $this->getCustomerAddress($params);
    $customerData = $this->getCustomerType($params);
    $subscription->setBillTo($customerAddress);
    $subscription->setCustomer($customerData);

    $request = new AnetAPI\ARBCreateSubscriptionRequest();
    $request->setmerchantAuthentication($merchantAuthentication);
    $request->setRefId($this->getInvoiceNumber($propertyBag));
    $request->setSubscription($subscription);
    $controller = new AnetController\ARBCreateSubscriptionController($request);
    /** @var \net\authorize\api\contract\v1\ARBCreateSubscriptionResponse $response */
    $response = $controller->executeWithApiResponse($this->getIsTestMode() ? AnetEnvironment::SANDBOX : AnetEnvironment::PRODUCTION);

    if (!$response || !$response->getMessages()) {
      $this->handleError(NULL, 'No response returned', $propertyBag->getCustomProperty('error_url'));
    }

    if ($response->getMessages()->getResultCode() == "Ok") {
      $this->setPaymentProcessorOrderID($response->getRefId());

      $recurParams = [
        'id' => $propertyBag->getContributionRecurID(),
        'processor_id' => $response->getSubscriptionId(),
        'auto_renew' => 1,
      ];
      if (!empty($params['installments'])) {
        if (empty($params['start_date'])) {
          $params['start_date'] = date('YmdHis');
        }
      }

      // Update the recurring payment
      ContributionRecur::update(FALSE)
        ->setValues($recurParams)
        ->addWhere('id', '=', $propertyBag->getContributionRecurID())
        ->execute();
      return $this->endDoPayment($params);
    }
    else {
      if ($response->getMessages()) {
        foreach ($response->getMessages()->getMessage() as $rError) {
          $errorMessages[] = $rError->getCode() . ': ' . $rError->getText();
        }
        $errorMessage = implode(', ', $errorMessages);
      }
      else {
        $errorCode = NULL;
        $errorMessage = NULL;
      }
      $this->handleError($errorCode, $errorMessage, $propertyBag->getCustomProperty('error_url'));
    }
  }

  /**
   * @param string $message
   * @param array $params
   *
   * @return bool
   * @throws \Civi\Payment\Exception\PaymentProcessorException
   */
  public function updateSubscriptionBillingInfo(&$message = '', $params = []) {
    $propertyBag = $this->beginUpdateSubscriptionBillingInfo($params);

    $merchantAuthentication = $this->getMerchantAuthentication();

    $subscription = new AnetAPI\ARBSubscriptionType();
    $subscription->setPayment($this->getPaymentDetails($params));

    $customerAddress = $this->getCustomerAddress($params);
    //$customerData = $this->getCustomerType();
    $subscription->setBillTo($customerAddress);
    //$subscription->setCustomer($customerData);

    $request = new AnetAPI\ARBUpdateSubscriptionRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($this->getInvoiceNumber($propertyBag));
    $request->setSubscriptionId($propertyBag->getRecurProcessorID());
    $request->setSubscription($subscription);
    $controller = new AnetController\ARBUpdateSubscriptionController($request);
    /** @var \net\authorize\api\contract\v1\ARBUpdateSubscriptionResponse $response */
    $response = $controller->executeWithApiResponse($this->getIsTestMode() ? AnetEnvironment::SANDBOX : AnetEnvironment::PRODUCTION);

    if (!$response || !$response->getMessages()) {
      $this->handleError(NULL, 'No response returned', $propertyBag->getCustomProperty('error_url'));
    }

    if ($response->getMessages()->getResultCode() != "Ok") {
      if ($response->getMessages()) {
        foreach ($response->getMessages()->getMessage() as $rError) {
          $errorMessages[] = $rError->getCode() . ': ' . $rError->getText();
        }
        $errorMessage = implode(', ', $errorMessages);
      }
      else {
        $errorCode = NULL;
        $errorMessage = NULL;
      }
      $this->handleError($errorCode, $errorMessage, $propertyBag->getCustomProperty('error_url'));
    }

    return TRUE;
  }

  /**
   * Change the subscription amount
   *
   * @param string $message
   * @param array $params
   *
   * @return bool
   * @throws \Exception
   */
  public function changeSubscriptionAmount(&$message = '', $params = []) {
    $propertyBag = $this->beginChangeSubscriptionAmount($params);

    $merchantAuthentication = $this->getMerchantAuthentication();

    $subscription = new AnetAPI\ARBSubscriptionType();
    $paymentSchedule = $this->getRecurSchedule($propertyBag);
    $subscription->setPaymentSchedule($paymentSchedule);
    $subscription->setAmount($propertyBag->getAmount());
    $request = new AnetAPI\ARBUpdateSubscriptionRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($this->getInvoiceNumber($propertyBag));
    $request->setSubscriptionId($propertyBag->getRecurProcessorID());
    $request->setSubscription($subscription);
    $controller = new AnetController\ARBUpdateSubscriptionController($request);
    /** @var \net\authorize\api\contract\v1\ARBUpdateSubscriptionResponse $response */
    $response = $controller->executeWithApiResponse($this->getIsTestMode() ? AnetEnvironment::SANDBOX : AnetEnvironment::PRODUCTION);

    if (!$response || !$response->getMessages()) {
      $this->handleError(
        NULL,
        'No response returned',
        $propertyBag->getCustomProperty('error_url')
      );
    }

    if ($response->getMessages()->getResultCode() != "Ok") {
      if ($response->getMessages()) {
        foreach ($response->getMessages()->getMessage() as $rError) {
          $errorMessages[] = $rError->getCode() . ': ' . $rError->getText();
        }
        $errorMessage = implode(', ', $errorMessages);
      }
      else {
        $errorCode = NULL;
        $errorMessage = NULL;
      }
      $this->handleError($errorCode, $errorMessage, $propertyBag->getCustomProperty('error_url'));
    }

    return TRUE;
  }

  /**
   * Does this processor support cancelling recurring contributions through code.
   *
   * If the processor returns true it must be possible to take action from within CiviCRM
   * that will result in no further payments being processed.
   *
   * @return bool
   */
  protected function supportsCancelRecurring() {
    return TRUE;
  }

  /**
   * Does the processor support the user having a choice as to whether to cancel the recurring with the processor?
   *
   * If this returns TRUE then there will be an option to send a cancellation request in the cancellation form.
   *
   * @return bool
   */
  protected function supportsCancelRecurringNotifyOptional() {
    return TRUE;
  }

  /**
   * Attempt to cancel the subscription at AuthorizeNet.
   *
   * @param \Civi\Payment\PropertyBag $propertyBag
   *
   * @return array|null[]
   * @throws \Civi\Payment\Exception\PaymentProcessorException
   */
  public function doCancelRecurring(PropertyBag $propertyBag) {
    // By default we always notify the processor and we don't give the user the option
    // because supportsCancelRecurringNotifyOptional() = FALSE
    if (!$propertyBag->has('isNotifyProcessorOnCancelRecur')) {
      // If isNotifyProcessorOnCancelRecur is NOT set then we set our default
      $propertyBag->setIsNotifyProcessorOnCancelRecur(TRUE);
    }
    $notifyProcessor = $propertyBag->getIsNotifyProcessorOnCancelRecur();

    if (!$notifyProcessor) {
      return ['message' => E::ts('Successfully cancelled the subscription in CiviCRM ONLY.')];
    }

    if (!$propertyBag->has('recurProcessorID')) {
      $errorMessage = E::ts('The recurring contribution cannot be cancelled (No reference (contribution_recur.processor_id) found).');
      \Civi::log()->error($errorMessage);
      throw new PaymentProcessorException($errorMessage);
    }

    $merchantAuthentication = $this->getMerchantAuthentication();

    $request = new AnetAPI\ARBCancelSubscriptionRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($this->getInvoiceNumber($propertyBag));
    $request->setSubscriptionId($propertyBag->getRecurProcessorID());
    $controller = new AnetController\ARBCancelSubscriptionController($request);
    /** @var \net\authorize\api\contract\v1\ARBCancelSubscriptionResponse $response */
    $response = $controller->executeWithApiResponse($this->getIsTestMode() ? AnetEnvironment::SANDBOX : AnetEnvironment::PRODUCTION);

    if (!$response || !$response->getMessages()) {
      $this->handleError(NULL, 'No response returned');
    }

    if ($response->getMessages()->getResultCode() != "Ok") {
      if ($response->getMessages()) {
        foreach ($response->getMessages()->getMessage() as $rError) {
          $errorMessages[] = $rError->getCode() . ': ' . $rError->getText();
        }
        $errorMessage = implode(', ', $errorMessages);
      }
      else {
        $errorCode = NULL;
        $errorMessage = NULL;
      }
      $this->handleError($errorCode, $errorMessage);
    }

    return ['message' => E::ts('Successfully cancelled the subscription at Authorize.net.')];
  }

  /**
   * Return the invoice number formatted in the "standard" way
   * @fixme This is how it has always been done with authnet and is not necessarily the best way
   *
   * @param \Civi\Payment\PropertyBag $propertyBag
   *
   * @return string
   */
  protected function getInvoiceNumber(PropertyBag $propertyBag): string {
    if ($propertyBag->has('invoiceID')) {
      return substr($propertyBag->getInvoiceID(), 0, 20);
    }
    return '';
  }

  /**
   * Set the payment details for the subscription
   *
   * @param array $params
   *
   * @return AnetAPI\PaymentType
   */
  abstract protected function getPaymentDetails($params);

  /**
   * Process incoming payment notification (IPN).
   *
   * @throws \CRM_Core_Exception
   * @throws \CiviCRM_API3_Exception
   */
  public function handlePaymentNotification() {
    // Set default http response to 200
    http_response_code(200);
    $payload = file_get_contents("php://input");
    $ipnClass = new CRM_Core_Payment_AuthNetIPN();
    $ipnClass->setVerifyData(TRUE);
    $ipnClass->setData($payload);
    $ipnClass->setPaymentProcessor(CRM_Utils_Request::retrieveValue('processor_id', 'Positive', NULL, FALSE, 'GET'));
    try {
      $ipnClass->onReceiveWebhook();
    }
    catch (\Authnetjson\Exception\AuthnetInvalidJsonException $e) {
      // This was probably a silentpost notification which we don't handle. Ignore it.
      \Civi::log()->debug(E::SHORT_NAME . ' received invalid JSON - do you still have silentpost notifications enabled?');
    }
  }

  /**
   * @param int $paymentProcessorID
   *   The actual payment processor ID that should be used.
   * @param $rawData
   *   The "raw" data, eg. a JSON string that is saved in the civicrm_system_log.context table
   * @param bool $verifyRequest
   *   Should we verify the request data with the payment processor (eg. retrieve it again?).
   * @param null|int $emailReceipt
   *   Override setting of email receipt if set to 0, 1
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   * @throws \ErrorException
   * @throws \Authnetjson\Exception\AuthnetInvalidCredentialsException
   * @throws \Authnetjson\Exception\AuthnetInvalidServerException
   */
  public static function processPaymentNotification($paymentProcessorID, $rawData, $verifyRequest = TRUE, $emailReceipt = NULL) {
    // Set default http response to 200
    http_response_code(200);

    $ipnClass = new CRM_Core_Payment_AuthNetIPN();
    $ipnClass->setVerifyData($verifyRequest);
    $ipnClass->setData($rawData);
    $ipnClass->setPaymentProcessor($paymentProcessorID);
    $ipnClass->setExceptionMode(FALSE);
    if (isset($emailReceipt)) {
      $ipnClass->setSendEmailReceipt($emailReceipt);
    }
    return $ipnClass->onReceiveWebhook();
  }

}
