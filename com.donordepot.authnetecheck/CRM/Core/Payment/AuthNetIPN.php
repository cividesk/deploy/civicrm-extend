<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */

use CRM_AuthNetEcheck_ExtensionUtil as E;
use \Authnetjson\AuthnetWebhook as AuthnetWebhook;
use \Authnetjson\AuthnetApiFactory as AuthnetApiFactory;
use \Authnetjson\AuthnetWebhooksResponse as AuthnetWebhooksResponse;

class CRM_Core_Payment_AuthNetIPN {

  use CRM_Core_Payment_MJWIPNTrait;

  /**
   * @var \CRM_Core_Payment_AuthorizeNetCommon Payment processor
   */
  protected $_paymentProcessor;

  /**
   * Authorize.net webhook transaction ID
   *
   * @var string
   */
  private $trxnId = NULL;

  /**
   * Authorize.net webhook subscription ID
   *
   * @var string
   */
  private $subscriptionId = NULL;

  /**
   * Get the transaction ID
   *
   * @return string
   */
  private function getTransactionId() {
    return $this->trxnId;
  }

  /**
   * Get the subscription ID
   * @return string
   */
  private function getSubscriptionId() {
    return $this->subscriptionId;
  }

  public function onReceiveWebhook() {
    $webhook = new AuthnetWebhook(
      CRM_Core_Payment_AuthorizeNetCommon::getSignature(
        $this->_paymentProcessor->getPaymentProcessor()
      ),
      $this->getData());
    if ($this->getVerifyData() && !$webhook->isValid()) {
      $this->exception('Webhook not valid');
    }

    $this->setEventType($webhook->eventType);
    $this->setEventID($webhook->webhookId);
    switch ($webhook->payload->entityName) {
      case 'transaction':
        $this->trxnId = $webhook->payload->id;
        break;

      case 'subscription':
        $this->subscriptionId = $webhook->payload->id;
        break;
    }

    return $this->processWebhook();
  }

  /**
   * @return bool
   * @throws \CiviCRM_API3_Exception
   * @throws \ErrorException
   * @throws \Authnetjson\Exception\AuthnetInvalidCredentialsException
   * @throws \Authnetjson\Exception\AuthnetInvalidServerException
   */
  public function processWebhook() {
    // Here you can get more information about the transaction
    $request = AuthnetApiFactory::getJsonApiHandler(
      CRM_Core_Payment_AuthorizeNetCommon::getApiLoginId($this->_paymentProcessor->getPaymentProcessor()),
      CRM_Core_Payment_AuthorizeNetCommon::getTransactionKey($this->_paymentProcessor->getPaymentProcessor()),
      $this->_paymentProcessor->getIsTestMode() ? AuthnetApiFactory::USE_DEVELOPMENT_SERVER : AuthnetApiFactory::USE_PRODUCTION_SERVER
    );

    if ($this->getTransactionId()) {
      /** @var \Authnetjson\AuthnetJsonResponse $response */
      $response = $request->getTransactionDetailsRequest(['transId' => $this->getTransactionId()]);

      if ($response->messages->resultCode !== 'Ok') {
        $this->exception('Bad response from getTransactionDetailsRequest in IPN handler. ' . $response->getErrorText());
      }

      // CiviCRM implements separate payment processors for creditCard / eCheck but for AuthorizeNet there is only one.
      // So both CiviCRM payment processors receive the IPN notifications for each type.
      if (property_exists($response->transaction->payment, 'creditCard') && !($this->_paymentProcessor instanceof CRM_Core_Payment_AuthNetCreditcard)) {
        // \Civi::log()->debug('credit card payment but not creditcard processor');
        return TRUE;
      }
      elseif (property_exists($response->transaction->payment, 'bankAccount') && !($this->_paymentProcessor instanceof CRM_Core_Payment_AuthNetEcheck)) {
        // \Civi::log()->debug('bank account payment but not echeck processor');
        return TRUE;
      }
      if (!property_exists($response->transaction->payment, 'creditCard') && !property_exists($response->transaction->payment, 'bankAccount')) {
        $this->exception('Unsupported payment type (not creditCard or bankAccount)');
      }

      // Set parameters required for IPN functions
      if ($this->getParamFromResponse($response, 'is_recur')) {
        $subscriptionID = $this->getParamFromResponse($response, 'subscription_id');
        if (!$this->getRecurringContributionIDFromSubscriptionID($subscriptionID)) {
          \Civi::log()
            ->info($this->_paymentProcessor->getPaymentTypeLabel() . ": Could not find matching recurring contribution for subscription ID: {$subscriptionID}.");
          return TRUE;
        }
      }
      $this->event_date = $this->getParamFromResponse($response, 'trxn_date');
    }
    elseif ($this->getSubscriptionId()) {
      // We only need the contribution_recur_id
      $this->getRecurringContributionIDFromSubscriptionID($this->getSubscriptionId());
    }
    else {
      \Civi::log()->error("AuthNetIPN: Received {$this->getEventType()} ({$this->getEventID()}) webhook but no transactionId or subscriptionId found.");
      return FALSE;
    }

    // Process the event
    switch ($this->getEventType()) {
      case 'net.authorize.payment.authcapture.created':
        // Notifies you that an authorization and capture transaction was created.
        // invoice_id is the same for all completed payments in a authnet subscription (recurring contribution)
        // transaction_id is unique for each completed payment in a authnet subscription (recurring contribution)
        // subscription_id is set on the recurring contribution and is unique per authnet subscription.

        // Check if we already recorded this payment?
        if (civicrm_api3('Mjwpayment', 'get_payment', [
            'trxn_id' => $this->getParamFromResponse($response, 'transaction_id'),
            'status_id' => 'Completed'
          ])['count'] > 0) {
          // Payment already recorded
          return TRUE;
        }

        $contribution = $this->getContributionFromTrxnInfo($this->getParamFromResponse($response, 'invoice_id'));
        $pendingStatusId = CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'Pending');
        if ($contribution && ($contribution['contribution_status_id'] == $pendingStatusId)) {
          $params = [
            'contribution_id' => $contribution['id'],
            'trxn_id' => $this->getParamFromResponse($response, 'transaction_id'),
            'order_reference' => $this->getParamFromResponse($response, 'transaction_id'),
            'trxn_date' => $this->event_date,
            'total_amount' => $this->getParamFromResponse($response, 'total_amount'),
            'contribution_status_id' => $contribution['contribution_status_id']
          ];
          $this->updateContributionCompleted($params);
        }
        elseif ($this->getParamFromResponse($response, 'is_recur')) {
          $params = [
            'trxn_id' => $this->getParamFromResponse($response, 'transaction_id'),
            'order_reference' => $this->getParamFromResponse($response, 'transaction_id'),
            'receive_date' => $this->event_date,
            'contribution_recur_id' => $this->contribution_recur_id,
            'contribution_status_id' => CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'Completed'),
            'total_amount' => $this->getParamFromResponse($response, 'total_amount'),
          ];
          try {
            $this->repeatContribution($params);
          }
          catch (Exception $e) {
            // repeatContribution did not used to throw exception so we catch and return here.
            // We may want to change this in the future.
            \Civi::log()->error('AuthNetIPN: ' . $e->getMessage());
            return FALSE;
          }
        }
        break;

      case 'net.authorize.payment.refund.created':
        // Notifies you that a successfully settled transaction was refunded.
        $contribution = $this->getContributionFromTrxnInfo($this->getParamFromResponse($response, 'invoice_id'));
        if (!$contribution) {
          return FALSE;
        }

        $contribution = civicrm_api3('Mjwpayment', 'get_contribution', ['contribution_id' => $contribution['id']]);

        $params = [
          'contribution_id' => $contribution['id'],
          'total_amount' => 0 - abs($this->getParamFromResponse($response, 'refund_amount')),
          'trxn_date' => $this->getParamFromResponse($response, 'trxn_date'),
          //'fee_amount' => 0 - abs($this->fee),
          'trxn_id' => $this->getParamFromResponse($response, 'transaction_id'),
          'order_reference' => $this->getParamFromResponse($response, 'invoice_id'),
        ];
        if (isset($this->contribution['payments'])) {
          $refundStatusID = (int) CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'Refunded');
          foreach ($this->contribution['payments'] as $payment) {
            if (((int) $payment['status_id'] === $refundStatusID) && ((float) $payment['total_amount'] === $params['total_amount'])) {
              // Already refunded
              return TRUE;
            }
          }
          // This triggers the financial transactions/items to be updated correctly.
          $params['cancelled_payment_id'] = reset($contribution['payments'])['id'];
        }

        $this->updateContributionRefund($params);
        break;

      case 'net.authorize.payment.void.created':
        // Notifies you that an unsettled transaction was voided.
        $contribution = $this->getContributionFromTrxnInfo($this->getParamFromResponse($response, 'invoice_id'));
        if (!$contribution) {
          return FALSE;
        }
        $params = [
          'contribution_id' => $contribution['id'],
          'order_reference' => $this->getParamFromResponse($response, 'invoice_id'),
          'cancel_date' => $this->event_date,
          'cancel_reason' => 'Transaction voided',
        ];
        $this->updateContributionFailed($params);
        break;

      case 'net.authorize.payment.fraud.held':
        // Notifies you that a transaction was held as suspicious.
        break;

      case 'net.authorize.payment.fraud.approved':
        // Notifies you that a previously held transaction was approved.
        $contribution = $this->getContributionFromTrxnInfo($this->getParamFromResponse($response, 'invoice_id'));
        if (!$contribution) {
          return FALSE;
        }
        $params = [
          'contribution_id' => $contribution['id'],
          'trxn_id' => $this->getParamFromResponse($response, 'transaction_id'),
          'order_reference' => $this->getParamFromResponse($response, 'invoice_id'),
          'trxn_date' => $this->event_date,
          'contribution_status_id' => $contribution['contribution_status_id'],
          'total_amount' => $this->getParamFromResponse($response, 'total_amount'),
        ];
        $this->updateContributionCompleted($params);
        break;

      case 'net.authorize.payment.fraud.declined':
        // Notifies you that a previously held transaction was declined.
        $contribution = $this->getContributionFromTrxnInfo($this->getParamFromResponse($response, 'invoice_id'));
        if (!$contribution) {
          return FALSE;
        }
        $params = [
          'contribution_id' => $contribution['id'],
          'order_reference' => $this->getParamFromResponse($response, 'invoice_id'),
          'cancel_date' => $this->event_date,
          'cancel_reason' => 'Fraud declined',
        ];
        $this->updateContributionFailed($params);
        break;

        // Now the "subscription" (recurring) ones
      // case 'net.authorize.customer.subscription.created':
        // Notifies you that a subscription was created.
      // case 'net.authorize.customer.subscription.updated':
        // Notifies you that a subscription was updated.
      // case 'net.authorize.customer.subscription.suspended':
        // Notifies you that a subscription was suspended.
      case 'net.authorize.customer.subscription.terminated':
        // Notifies you that a subscription was terminated.
      case 'net.authorize.customer.subscription.cancelled':
        // Notifies you that a subscription was cancelled.
        $this->updateRecurCancelled(['id' => $this->contribution_recur_id]);
        break;

      // case 'net.authorize.customer.subscription.expiring':
        // Notifies you when a subscription has only one recurrence left to be charged.
    }

    return TRUE;
  }

  /**
   * Retrieve parameters from IPN response
   *
   * @param \Authnetjson\AuthnetJsonResponse $response
   * @param string $param
   *
   * @return mixed
   */
  protected function getParamFromResponse($response, $param) {
    switch ($param) {
      case 'transaction_id':
        return $response->transaction->transId;

      case 'invoice_id':
        return $response->transaction->order->invoiceNumber;

      case 'refund_amount':
      case 'total_amount':
        return $response->transaction->authAmount;

      case 'is_recur':
        return $response->transaction->recurringBilling;

      case 'subscription_id':
        return $response->transaction->subscription->id;

      case 'subscription_payment_number':
        return $response->transaction->subscription->payNum;

      case 'trxn_date':
        return date('YmdHis', strtotime($response->transaction->submitTimeLocal));

    }
  }

  /**
   * Get the contribution ID from the transaction info.
   *
   * @param string $invoiceID
   *
   * @return array|NULL
   * @throws \CiviCRM_API3_Exception
   */
  protected function getContributionFromTrxnInfo($invoiceID) {
    // invoiceNumber (refID) should be set on trxn_id of matching contribution
    $contributionParams = [
      'trxn_id' => $this->trxnId,
      'options' => ['limit' => 1, 'sort' => "id DESC"],
      'contribution_test' => $this->_paymentProcessor->getIsTestMode(),
    ];
    $contribution = civicrm_api3('Contribution', 'get', $contributionParams);

    // But if it is not we derived from first 20 chars of invoice_id so check there
    if (empty($contribution['id'])) {
      $contributionParams['trxn_id'] = ['LIKE' => "{$invoiceID}%"];
    }
    $contribution = civicrm_api3('Contribution', 'get', $contributionParams);
    // Or finally try via invoice_id directly (first 20 chars)
    if (empty($contribution['id'])) {
      unset($contributionParams['trxn_id']);
      $contributionParams['invoice_id'] = ['LIKE' => "{$invoiceID}%"];
    }
    $contribution = civicrm_api3('Contribution', 'get', $contributionParams);

    // We can't do anything without a matching contribution!
    if (empty($contribution['id'])) {
      return NULL;
    }

    return reset($contribution['values']);
  }

  /**
   * Map the subscription/invoiceID to the CiviCRM recurring contribution
   *
   * @param string $subscriptionID
   *
   * @return FALSE|int
   * @throws \CiviCRM_API3_Exception
   */
  protected function getRecurringContributionIDFromSubscriptionID($subscriptionID) {
    $contributionRecur = civicrm_api3('ContributionRecur', 'get', [
      'trxn_id' => $subscriptionID,
    ]);
    if (empty($contributionRecur['id'])) {
      return FALSE;
    }
    $this->contribution_recur_id = $contributionRecur['id'];
    return $this->contribution_recur_id;
  }

}
