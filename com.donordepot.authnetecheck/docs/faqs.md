## Terminology

#### CiviCRM <=> AuthorizeNet

* A CiviCRM **Recurring Contribution** is the equivalent of a AuthorizeNet **Subscription**.
* A CiviCRM **Contribution** is the equivalent of a AuthorizeNet **Transaction**.
* A CiviCRM **Payment** is the equivalent of a AuthorizeNet **Settled Transaction**.
* A CiviCRM **Contact** is the equivalent of a AuthorizeNet **Customer**.

Note that a collection of payments on a subscription have the *same* invoice ID but separate transaction IDs.

## Invoice number, Transactions, Payments, Subscriptions

CiviCRM generates an invoice ID for AuthorizeNet: It's the first 20 characters of the auto-generated invoice_id.

|Field|CiviCRM entity|AuthorizeNet|CiviCRM field|
|---|---|---|---|
|Subscription ID|ContributionRecur|Subscription ID|trxn_id/processor_id|
|Order Reference/Invoice|Contribution|Invoice Number|Not used (invoice_id)|
|Transaction ID|Contribution|Transaction ID|trxn_id|
|Transaction ID|Payment|Transaction ID|trxn_id|
|Order Reference|Payment|Transaction ID|order_reference|

