<?php
use CRM_Onholdcleaner_ExtensionUtil as E;

/**
 * Job.Onholdcleaner API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC/API+Architecture+Standards
 */
function _civicrm_api3_job_Onholdcleaner_spec(&$spec) {
}

/**
 * Job.Onholdcleaner API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_job_Onholdcleaner($params) {
  // Maximum number of unique email addresses cleaned each run
  $email_limit = 1000;

  // Prepare list of email and write in to log file for tracking
  $config = CRM_Core_Config::singleton();
  $onholdEmailCleanerPath = $config->configAndLogDir .DIRECTORY_SEPARATOR . 'onholdemailcleaner_log.csv';
  $first_time = !file_exists($onholdEmailCleanerPath);
  // Create or open file in append mode
  $onholdEmailCleaner = fopen($onholdEmailCleanerPath,"a");
  if ($first_time) {
    // job running first time then add header to file on time
    fputcsv($onholdEmailCleaner, array('Date Deleted', 'Contact Id', 'External Id', 'Display Name', 'Primary Email ID', 'Primary Email','Primary Location Type', 'Secondary Email ID', 'Secondary Email','Secondary Location Type'));
  }

  // get list of email  where fist email is primary and marked as onhold  and second email is different from primary and not on hold
  $getOnholdDuplicateEmail = "
SELECT c.id, c.external_identifier, c.display_name,
       e1.id as 'p_email_id', e1.email as 'primary_email',   e1.location_type_id as 'primary_location',
       e2.id as 's_email_id', e2.email as 'secondary_email', e2.location_type_id as 'secondary_location'
  FROM civicrm_contact c
       INNER JOIN civicrm_email e1 ON e1.contact_id = c.id AND e1.on_hold = 1 AND e1.is_primary = 1 
       INNER JOIN civicrm_email e2 ON e2.contact_id = c.id AND e2.on_hold = 0
 WHERE e1.id <> e2.id AND e1.email <> e2.email
 GROUP BY c.id
 LIMIT {$email_limit} ";
  $dao = CRM_Core_DAO::executeQuery($getOnholdDuplicateEmail);
  $count = 0;
  while ($dao->fetch()) {
    $result = civicrm_api3('Email', 'create', array(
      'sequential' => 1,
      'id' => $dao->s_email_id, // set secondary email as primary
      'is_primary' => 1,
    ));
    if ($result['is_error'] == '0') {
      $onHoldEmailRecord = array(date('m/d/Y h:i:s a', time()), $dao->id, $dao->external_identifier, $dao->display_name, $dao->p_email_id, $dao->primary_email, $dao->primary_location, $dao->s_email_id, $dao->secondary_email, $dao->secondary_location);
      fputcsv($onholdEmailCleaner, $onHoldEmailRecord);
      $count++;
    }
  }

  $message = ($count ? "$count email addresses cleaned. PLease see log file for details." : "No email address needs to be cleaned.");
  return civicrm_api3_create_success($message);
}
