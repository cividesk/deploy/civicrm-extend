# com.cividesk.priceoptionprivacy

Cividesk developed this extension to allow for member only pricing to be displayed when logged in users are recognized as current members while remaining hidden from anonymous or logged-in users not recognized as members. 

Cividesk now supports this extension, and it is used in production by Cividesk and several of its customers.

Installation
------------

Installing this extension is quite easy. You only have to:

1. Visit your `civicrm/admin/extensions` page
2. See what is your extension directory, or create one if it doesn't exist
3. Go to that directory and `git clone https://github.com/cividesk/com.cividesk.priceoptionprivacy.git`
4. Change permissions and ownership of the new directory, if needed
5. Refresh the `civicrm/admin/extensions` page
6. Click on "install"

Upon installation
-----------------

If you have a price field that has different rates for members and non-members, create 2 price field options, one with a visibility of `Member Only`, the other with a visibility of `Public`.

For logged in users recognized as current members (with status - New, Current, Grace), price field options with a visibility of either `Member Only` or `Public` will be displayed. For anonymous users or logged in users not recognized as members, only price field options with a visibility of `Public` will be displayed.
