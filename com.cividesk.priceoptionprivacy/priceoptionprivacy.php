<?php
require_once 'priceoptionprivacy.civix.php';
use CRM_Priceoptionprivacy_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function priceoptionprivacy_civicrm_config(&$config) {
  _priceoptionprivacy_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function priceoptionprivacy_civicrm_xmlMenu(&$files) {
  _priceoptionprivacy_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function priceoptionprivacy_civicrm_install() {
  _priceoptionprivacy_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function priceoptionprivacy_civicrm_postInstall() {
  _priceoptionprivacy_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function priceoptionprivacy_civicrm_uninstall() {
  _priceoptionprivacy_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function priceoptionprivacy_civicrm_enable() {
  _priceoptionprivacy_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function priceoptionprivacy_civicrm_disable() {
  _priceoptionprivacy_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function priceoptionprivacy_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _priceoptionprivacy_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function priceoptionprivacy_civicrm_managed(&$entities) {
  _priceoptionprivacy_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function priceoptionprivacy_civicrm_caseTypes(&$caseTypes) {
  _priceoptionprivacy_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function priceoptionprivacy_civicrm_angularModules(&$angularModules) {
  _priceoptionprivacy_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function priceoptionprivacy_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _priceoptionprivacy_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function priceoptionprivacy_civicrm_entityTypes(&$entityTypes) {
  _priceoptionprivacy_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_buildAmount().
 *
 * @param string $formName
 * @param CRM_Core_Form $form
 */
function priceoptionprivacy_civicrm_buildAmount($pageType, &$form, &$amount) {
  $contactID = CRM_Core_Session::singleton()->getLoggedInContactID();
  $visibilityId = civicrm_api3('OptionValue', 'get', array(
    'name' => "member_only",
  ));
  $memberOnlyId = $visibilityId['values'][$visibilityId['id']]['value'];

  if ($contactID) {
    //Get only active memberships to check if contact is active member or not.
    $membershipResult = civicrm_api3('Membership', 'get', array(
      'active_only' => 1,
      'contact_id' => $contactID,
    ));

    // Check if contact have active membership if not remove member-only price options.
    if ($membershipResult['count'] == 0) {
      foreach($amount as $key => $value) {
        if($value['visibility_id'] == $memberOnlyId ) {
          $className = $amount[$key]['name'].'-content';
          unset($amount[$key]);
          // Hide label.
          CRM_Core_Region::instance('page-body')->add(array(
            'script' => "
              cj('.$className').hide();
            ",
          ));
        }
        if ($value['options']) {
          foreach ($value['options'] as $optKey => $optVal) {
            if ($optVal['visibility_id'] == $memberOnlyId) {
              unset($amount[$key]['options'][$optKey]);
            }
          }
        }
      }
    }
  } else {
    foreach($amount as $key => $value) {
      if($value['visibility_id'] == $memberOnlyId ) {
        $className = $amount[$key]['name'].'-content';
        unset($amount[$key]);
        // Hide label.
        CRM_Core_Region::instance('page-body')->add(array(
          'script' => "
            cj('.$className').hide();
          ",
        ));
      }
      if ($value['options']) {
        foreach ($value['options'] as $optKey => $optVal) {
          if ($optVal['visibility_id'] == $memberOnlyId) {
            unset($amount[$key]['options'][$optKey]);
          }
        }
      }
    }
  }
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 *
function priceoptionprivacy_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 *
function priceoptionprivacy_civicrm_navigationMenu(&$menu) {
  _priceoptionprivacy_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('New subliminal message'),
    'name' => 'mailing_subliminal_message',
    'url' => 'civicrm/mailing/subliminal',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _priceoptionprivacy_civix_navigationMenu($menu);
} // */
