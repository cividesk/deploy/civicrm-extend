SELECT @option_group_id_vis := max(id) from civicrm_option_group where name = 'visibility';
SELECT @option_value_id_vis := max(value) from civicrm_option_value where option_group_id = @option_group_id_vis;
INSERT INTO
   `civicrm_option_value` (`option_group_id`, `label`, `value`, `name`, `grouping`, `filter`, `is_default`, `weight`, `description`, `is_optgroup`, `is_reserved`,
    `is_active`, `component_id`, `visibility_id`, `icon`)
VALUES (@option_group_id_vis, 'Member Only', @option_value_id_vis+1, 'member_only', NULL, 0, NULL, 1, NULL, 0, 0, 1, NULL, NULL , NULL)
