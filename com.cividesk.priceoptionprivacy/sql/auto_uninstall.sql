SELECT @option_group_id_vis := max(id) from civicrm_option_group where name = 'visibility';

DELETE FROM civicrm_option_value
       WHERE name = 'member_only'
       AND option_group_id = @option_group_id_vis;
