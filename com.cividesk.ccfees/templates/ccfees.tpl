{if $coverFees}
<div id="cc_fee_section" class="crm-section"> <div class="label">{$form.cc_process_fee.label}</div><div class="content">{$form.cc_process_fee.html}</div><div class="clear"></div></div>
{/if}
{literal}
<script type="text/javascript">
var cc_price_id = {/literal}"{$coverFees}";{literal}
var coverFeesButtonName = {/literal}"{$coverFeesButtonName}";{literal}
var cc_percentage = {/literal}"{$cc_percentage}";{literal}
//cj('#'+cc_price_id).attr('readonly', true);
cj('#cc_fee_section').insertAfter('#pricesetTotal')
cj("#pricesetTotal" ).append( '<div class="clear"></div>' );
if (cc_price_id) {
  cj('#'+cc_price_id).val('');
  cj('#'+cc_price_id).trigger("keyup");
  cj('#'+cc_price_id).parent().parent().hide();
  if (coverFeesButtonName == '_qf_Participant_next') {
    var btnName = 'input[name^="_qf_Participant_"][name$="_next"]';
  } else {
    var btnName = 'input[name="'+ coverFeesButtonName+'"]';
  }

  cj(btnName).click(function(){
    var t_amount = cj('#pricevalue').data('raw-total');
    if (t_amount && cj('#cc_process_fee').is(':checked')) {
      cj('#'+cc_price_id).val(cc_percentage/100 * t_amount); // % of total amount
    }
  });
}
</script>
{/literal}
