<div class="form-item">
    <fieldset><legend>{ts}Credit Card Fee Setting{/ts}</legend>
        <div class="crm-block crm-form-block crm-cividesk-ccfees-form-block">
            <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl" location="top"}</div>
            <table class="form-layout-compressed">
                <tr class="crm-cividesk-ccfees-form-block">
                    <td class="label">{$form.ccfees_percentage.label}</td><td>{$form.ccfees_percentage.html}</td>
                </tr>
            </table>
            <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl" location="bottom"}</div>
        </div>
    </fieldset>
</div>
