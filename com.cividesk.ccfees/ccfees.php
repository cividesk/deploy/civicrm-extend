<?php

require_once 'ccfees.civix.php';

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function ccfees_civicrm_config(&$config) {
  _ccfees_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @param $files array(string)
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function ccfees_civicrm_xmlMenu(&$files) {
  _ccfees_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function ccfees_civicrm_install() {
  _ccfees_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function ccfees_civicrm_uninstall() {
  _ccfees_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function ccfees_civicrm_enable() {
  _ccfees_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function ccfees_civicrm_disable() {
  _ccfees_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed
 *   Based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function ccfees_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _ccfees_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function ccfees_civicrm_managed(&$entities) {
  _ccfees_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function ccfees_civicrm_caseTypes(&$caseTypes) {
  _ccfees_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function ccfees_civicrm_angularModules(&$angularModules) {
_ccfees_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function ccfees_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _ccfees_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Functions below this ship commented out. Uncomment as required.
 *

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function ccfees_civicrm_preProcess($formName, &$form) {

}

*/

function ccfees_civicrm_buildAmount($pagetype, &$form, &$amounts) {
  if (( !$form->getVar('_action')
        || ($form->getVar('_action') & CRM_Core_Action::PREVIEW)
        || ($form->getVar('_action') & CRM_Core_Action::ADD)
        || ($form->getVar('_action') & CRM_Core_Action::UPDATE)
      )
    && !empty($amounts) && is_array($amounts)) {

    if (in_array(get_class($form), array(
      'CRM_Contribute_Form_Contribution',
      'CRM_Contribute_Form_Contribution_Main',
      'CRM_Event_Form_Registration_AdditionalParticipant',
      'CRM_Event_Form_Registration_Register',
    ))) {
      $buttonName = '';
      if (get_class($form) == 'CRM_Contribute_Form_Contribution_Main') {
        $buttonName = '_qf_Main_upload'; 
      } else if (get_class($form) == 'CRM_Event_Form_Registration_Register') {
        $buttonName = '_qf_Register_upload';
      } else if (get_class($form) == 'CRM_Event_Form_Registration_AdditionalParticipant') {
        $buttonName = '_qf_Participant_next';
      }
      $form->assign('coverFeesButtonName', $buttonName);
      $financialTypes = CRM_Contribute_PseudoConstant::financialType();
      $cc_financial_type_id = CRM_Utils_Array::key('Covered Transaction Fee', $financialTypes);
      foreach ($amounts as $fee_id => &$fee) {
        if (!is_array($fee['options'])) {
          continue;
        }

        foreach ($fee['options'] as $option_id => &$option) {
          if (isset($option['financial_type_id']) && $option['financial_type_id'] == $cc_financial_type_id && $fee['html_type'] == 'Text') {
           $form->_coverFees = 'price_'. $option['price_field_id'];
           $form->_coverFeesID = $option['id'];
           $form->assign('coverFees', 'price_'. $option['price_field_id']);
           $CcSetting = CRM_Utils_Ccfees::getSettings();
           if (empty($CcSetting['ccfees_percentage'])) {
             CRM_Core_Session::setStatus(ts('Percentage amount is not set for Credit Card Fees.'), ts('Warning'), 'alert');
           }
           $form->assign('cc_percentage', $CcSetting['ccfees_percentage']);
          }
        }
      }
    }
  }
}


function ccfees_civicrm_buildForm($fname, &$form) {
  // also skip when content is loaded via ajax, like payment processor, custom data etc
  $snippet = CRM_Utils_Request::retrieve('snippet', 'String', CRM_Core_DAO::$_nullObject, false, null, 'REQUEST');

  if ( $snippet == 4 || ( $form->getVar('_action') && ($form->getVar('_action') & CRM_Core_Action::DELETE ) ) ) {
    return false;
  }
  
  if (in_array($fname, array(
                             'CRM_Contribute_Form_Contribution_Main',
                             'CRM_Event_Form_Registration_AdditionalParticipant',
                             'CRM_Event_Form_Registration_Register',
                            )
    )) {
    $CcSetting = CRM_Utils_Ccfees::getSettings();
    $form->add('checkbox', 'cc_process_fee', ts('Please add %1% to my total to cover credit card processing fees', array(1 => $CcSetting['ccfees_percentage'])));
    CRM_Core_Region::instance('page-body')->add(array('template' => 'ccfees.tpl'));
  }
}

function ccfees_civicrm_postProcess($formName, $form) {
  if (isset($form->_coverFees) && !empty($form->_submitValues['cc_process_fee'])) {
    $lineItem = $form->get('lineItem');
    $lineItem[$form->_priceSetId][$form->_coverFeesID]['qty'] = 1;
    $lineItem[$form->_priceSetId][$form->_coverFeesID]['unit_price'] = $lineItem[$form->_priceSetId][$form->_coverFeesID]['line_total'];
    $form->set('lineItem', $lineItem);
  }
}

/**
 * Implementation of hook_civicrm_navigationMenu
 */
function ccfees_civicrm_navigationMenu( &$params ) {
  // Add menu entry for extension administration page
  _ccfees_civix_insert_navigationMenu($params, 'Administer/Customize Data and Screens', array(
    'name'       => 'Credit Card Fees',
    'url'        => 'civicrm/ccfees/setting',
    'permission' => 'administer CiviCRM',
  ));
}
