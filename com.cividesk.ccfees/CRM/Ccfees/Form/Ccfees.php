<?php

require_once 'CRM/Core/Form.php';

/**
 * Form controller class
 *
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC43/QuickForm+Reference
 */
class CRM_Ccfees_Form_Ccfees extends CRM_Core_Form {
  protected $_settings;

  function preProcess() {
    // Needs to be here as from is build before default values are set
    $this->_settings = CRM_Utils_Ccfees::getSettings();
    if (!$this->_settings) $this->_settings = array();
  }
  
  function buildQuickForm() {
    $this->applyFilter('__ALL__', 'trim');
    $this->add('text', 'ccfees_percentage',  ts('Percentage'),  array('size' => 2, 'maxlength' => 2), TRUE );
    $this->addRule('ccfees_percentage', ts('Please enter a valid Percentage.'), 'money');
    $this->addButtons(array(
      array(
        'type' => 'submit',
        'name' => ts('Submit'),
        'isDefault' => TRUE,
      ),
    ));

    parent::buildQuickForm();
  }

  function setDefaultValues() {
    $defaults = $this->_settings;
    return $defaults;
  }
  
  function postProcess() {
    $params = $this->exportValues();    
    // Save all settings
    foreach (array('ccfees_percentage') as $key => $name) {
      CRM_Utils_Ccfees::setSetting(CRM_Utils_Array::value($name, $params, 0), $name);
    }
  }
}
