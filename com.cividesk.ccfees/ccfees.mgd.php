<?php

return array( 
     0 => 
  array (
    'module' => 'com.cividesk.ccfees',
    'name' => 'ccfees',
    'entity' => 'FinancialType',
    'params' => array(
      'version' => 3,
      'name' => 'Covered Transaction Fee',
      'description' => 'Covered Transaction Fee',
      'is_deductible' => 0,
      'is_active' => 1,
      'is_reserved' => 1
    ),
    ),  
  );

?>
