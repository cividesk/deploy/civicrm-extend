<?php return array(
    'root' => array(
        'name' => 'iats-payments/civicrm',
        'pretty_version' => '1.7.4',
        'version' => '1.7.4.0',
        'reference' => '571319ddb8ed1ced6494e30da8b1e2639db1a741',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'iats-payments/civicrm' => array(
            'pretty_version' => '1.7.4',
            'version' => '1.7.4.0',
            'reference' => '571319ddb8ed1ced6494e30da8b1e2639db1a741',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
