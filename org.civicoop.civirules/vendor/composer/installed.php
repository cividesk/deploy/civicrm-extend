<?php return array(
    'root' => array(
        'name' => 'civicrm/civirules',
        'pretty_version' => '2.43',
        'version' => '2.43.0.0',
        'reference' => '9a544f5c1e31cdefcd063c0135a85ca84aeaac1f',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'civicrm/civirules' => array(
            'pretty_version' => '2.43',
            'version' => '2.43.0.0',
            'reference' => '9a544f5c1e31cdefcd063c0135a85ca84aeaac1f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
