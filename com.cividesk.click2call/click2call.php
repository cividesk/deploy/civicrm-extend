<?php
/**
  Extension to add link to phone number in Contact Summary Page and Contat Search Page.
  templates/CRM/Contact/Form/Selector.tpl file modified: added class to td (phone) element <td class="crm-contact_phone">
*/
function click2call_civicrm_config(&$config) {
  $template =& CRM_Core_Smarty::singleton();

  $extRoot = dirname( __FILE__ ) . DIRECTORY_SEPARATOR;
  $extDir = $extRoot . 'templates';

  if ( is_array( $template->template_dir ) ) {
      array_unshift( $template->template_dir, $extDir );
  } else {
      $template->template_dir = array( $extDir, $template->template_dir );
  }

  $include_path = $extRoot . PATH_SEPARATOR . get_include_path( );
  set_include_path( $include_path );
}

function click2call_civicrm_alterContent(&$content, $context, $tplName, &$object) {

  if ( in_array($tplName, array('CRM/Contact/Page/View/Summary.tpl', 'CRM/Contact/Page/Inline/Phone.tpl') ) ) {
    $content .= '<script type="text/javascript"> 
    if ( cj(".do-not-phone").length == 0 ) {
    cj(".crm-contact_phone").each(function( index ) { 
      var ph = cj.trim(cj(this).html());
      number = ph.replace(/[^+\d]+/g, "");
      cj(this).html("<a href=\'tel:" + number +"\'>"+ ph +"</a>")
   
    } );
    }
    </script>';
  } else if ( in_array($tplName, array('CRM/Contact/Form/Search/Basic.tpl') ) ) {
    $content .= '<script type="text/javascript"> 
    cj(".crm-contact_phone").each(function( index ) {
      if (cj(this).find( "span" ).length == 0 ) {
        var ph = cj.trim(cj(this).html());
        number = ph.replace(/[^+\d]+/g, "");
        cj(this).html("<a href=\'tel:" + number +"\'>"+ ph +"</a>")
      }
    } );
    </script>';  
  }
}

