<?php return array(
    'root' => array(
        'name' => 'cdntaxreceipts/cdntaxreceipts',
        'pretty_version' => '1.7.0',
        'version' => '1.7.0.0',
        'reference' => 'ae8bf780cd53d9145f8b06f1827ac06609f90acc',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'cdntaxreceipts/cdntaxreceipts' => array(
            'pretty_version' => '1.7.0',
            'version' => '1.7.0.0',
            'reference' => 'ae8bf780cd53d9145f8b06f1827ac06609f90acc',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
