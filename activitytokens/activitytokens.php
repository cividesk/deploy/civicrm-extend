<?php

require_once 'activitytokens.civix.php';
use CRM_Activitytokens_ExtensionUtil as E;

function activitytokens_civicrm_container($container) {
  $container->addResource(new \Symfony\Component\Config\Resource\FileResource(__FILE__));
  $container->findDefinition('dispatcher')->addMethodCall('addListener',
    array(\Civi\Token\Events::TOKEN_EVALUATE, 'activitytokens_evaluate_tokens')
  );
}

function activitytokens_evaluate_tokens(\Civi\Token\Event\TokenValueEvent $e) {
  $tokenClass = CRM_ActivityTokens_Tokens::singleton();
  foreach ($e->getRows() as $row) {
    if (isset($row->context['actionSearchResult']) && ($row->context['actionSearchResult']->activity_id || ('civicrm_activity' == $row->context['actionSearchResult']->entity_table && $row->context['actionSearchResult']->entity_id))){
      
      $activity_ids = [];
      if($row->context['actionSearchResult']->activity_id){
        $activity_ids = [$row->context['actionSearchResult']->activity_id];
      } elseif($row->context['actionSearchResult']->entity_id){
        $activity_ids = [$row->context['actionSearchResult']->entity_id];
      }
      
      $targets = $tokenClass->getActivityContacts($activity_ids, 'Activity Targets');
      $assignees = $tokenClass->getActivityContacts($activity_ids, 'Activity Assignees');
      $source = $tokenClass->getActivityContacts($activity_ids, 'Activity Source');

      $row->tokens(CRM_ActivityTokens_Tokens::TOKEN, 'targets', $targets);
      $row->tokens(CRM_ActivityTokens_Tokens::TOKEN, 'assignees', $assignees);
      $row->tokens(CRM_ActivityTokens_Tokens::TOKEN, 'source', $source);
      foreach ($activity_ids as $activity_id) {
        if ($activity_id) {
          $activity = $tokenClass->getActivity($activity_id);
          foreach ($activity as $field => $value) {
            if (stripos($field, 'custom_') === 0) {
              $customFieldID = substr($field, 7);
              $customField = \CRM_Core_BAO_CustomField::getFieldObject($customFieldID);
              // Use the \CRM_Utils_Date::format for date fields
              // because the displayValue is not translated.
              if ($customField->html_type == 'Select Date') {
                $value = \CRM_Utils_Date::customFormat($value);
              } else {
                $value = \CRM_Core_BAO_CustomField::displayValue($value, $customField);
              }
              $row->tokens(CRM_ActivityTokens_Tokens::TOKEN, $field, $value);
            }
          }
        }
      }
    }
  }
}

/**
 * Implements hook__civicrm_tokenValues().
 *
 * @link https://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_tokenValues
 */
function activitytokens_civicrm_tokens(&$tokens) {
  $tokens[CRM_ActivityTokens_Tokens::TOKEN][CRM_ActivityTokens_Tokens::TOKEN.'.targets'] = E::ts('Activity target(s)');
  $tokens[CRM_ActivityTokens_Tokens::TOKEN][CRM_ActivityTokens_Tokens::TOKEN.'.assignees'] = E::ts('Activity assignee(s)');
  $tokens[CRM_ActivityTokens_Tokens::TOKEN][CRM_ActivityTokens_Tokens::TOKEN.'.source'] = E::ts('Activity source');

  $activityCustomGroups = civicrm_api3('CustomGroup', 'get', array('extends' => 'Activity', 'is_active'  => 1,'options' => array('limit' => 0)));
  foreach($activityCustomGroups['values'] as $activityCustomGroup) {
    $customFields = civicrm_api3('CustomField', 'get', array('custom_group_id' => $activityCustomGroup['id'], 'is_active' => 1, 'options' => array('limit' => 0)));
    foreach($customFields['values'] as $customField) {
      $tokens[CRM_ActivityTokens_Tokens::TOKEN][CRM_ActivityTokens_Tokens::TOKEN.'.custom_'.$customField['id']] = $activityCustomGroup['title'].': '.$customField['label'];
    }
  }
}

/**
 * Implements hook__civicrm_tokenValues().
 *
 * @link https://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_tokenValues
 */
function activitytokens_civicrm_tokenValues(&$values, $cids, $job = null, $tokens = array(), $context = null) {
  $tokenClass = CRM_ActivityTokens_Tokens::singleton();
  $tokenClass->tokenValues($values, $cids, $tokens);
}

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function activitytokens_civicrm_config(&$config) {
  _activitytokens_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function activitytokens_civicrm_xmlMenu(&$files) {
  _activitytokens_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function activitytokens_civicrm_install() {
  _activitytokens_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
 */
function activitytokens_civicrm_postInstall() {
  _activitytokens_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function activitytokens_civicrm_uninstall() {
  _activitytokens_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function activitytokens_civicrm_enable() {
  _activitytokens_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function activitytokens_civicrm_disable() {
  _activitytokens_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function activitytokens_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _activitytokens_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function activitytokens_civicrm_managed(&$entities) {
  _activitytokens_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function activitytokens_civicrm_caseTypes(&$caseTypes) {
  _activitytokens_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_angularModules
 */
function activitytokens_civicrm_angularModules(&$angularModules) {
  _activitytokens_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function activitytokens_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _activitytokens_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_entityTypes
 */
function activitytokens_civicrm_entityTypes(&$entityTypes) {
  _activitytokens_civix_civicrm_entityTypes($entityTypes);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function activitytokens_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function activitytokens_civicrm_navigationMenu(&$menu) {
  _activitytokens_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('New subliminal message'),
    'name' => 'mailing_subliminal_message',
    'url' => 'civicrm/mailing/subliminal',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _activitytokens_civix_navigationMenu($menu);
} // */
