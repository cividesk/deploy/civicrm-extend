<?php
use CRM_ActivityTokens_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_ActivityTokens_Upgrader extends CRM_ActivityTokens_Upgrader_Base {

  /**
   * Display a notice about renamed tokens
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_1002() {
    $this->ctx->log->info('Applying update 4200');
    CRM_Core_Session::setStatus(E::ts('The activity tokens have been renamed to <em>activity_tokens</em>. Check all your message templates to reflect those changes'), '', 'info');
    return TRUE;
  }

}
