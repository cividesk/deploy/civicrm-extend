<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

use CRM_Activitytokens_ExtensionUtil as E;

class CRM_ActivityTokens_Tokens {

  const TOKEN = 'activity_tokens';

  private $activities = array();

  private $activityContacts = array();

  /**
   * @var \CRM_ActivityTokens_Tokens
   */
  private static $singleton;

  public function tokenValues(&$values, $cids, $tokens) {
    if ($this->isTokenInTokens($tokens, 'targets')) {
      $this->activityContactToken($values, $cids, 'targets', 'Activity Targets');
    }
    if ($this->isTokenInTokens($tokens, 'assignees')) {
      $this->activityContactToken($values, $cids, 'assignees', 'Activity Assignees');
    }
    if ($this->isTokenInTokens($tokens, 'source')) {
      $this->activityContactToken($values, $cids, 'source', 'Activity Source');
    }

    foreach($cids as $cid) {
      $activity_id = false;
      if (isset($values[$cid]['extra_data']['activity']['id'])) {
        $activity_id = $values[$cid]['extra_data']['activity']['id'];
      } elseif (isset($values['cid']['activity_id'])) {
        $activity_id = $values['cid']['activity_id'];
      }
      if ($activity_id) {
        $activity = $this->getActivity($activity_id);
        foreach ($activity as $field => $value) {
          if (stripos($field, 'custom_') === 0) {
            $customFieldID = substr($field, 7);
            $customField   = \CRM_Core_BAO_CustomField::getFieldObject($customFieldID);
            // Use the \CRM_Utils_Date::format for date fields
            // because the displayValue is not translated.
            if ($customField->html_type == 'Select Date') {
              $value = \CRM_Utils_Date::customFormat($value);
            }
            else {
              $value = \CRM_Core_BAO_CustomField::displayValue($value, $customField);
            }
            $values[$cid][self::TOKEN . '.' . $field] = $value;
          }
        }
      }
    }
  }

  /**
   * @return \CRM_ActivityTokens_Tokens
   */
  public static function singleton() {
    if (!self::$singleton) {
      self::$singleton = new CRM_ActivityTokens_Tokens();
    }
    return self::$singleton;
  }

  protected function activityContactToken(&$values, $cids, $token, $record_type) {
    $contact_ids = $cids;
    if (!is_array($contact_ids)) {
      $contact_ids = array($contact_ids);
    }
    $tokenValues = array();

    foreach($contact_ids as $contact_id) {
      $activity_id = $this->getActivityId($values, $contact_id);
      $tokenValues[$contact_id] = $this->getActivityContacts(array($activity_id), $record_type);
    }
    $this->setTokenValue($values, $cids, $token, $tokenValues);
  }

  private function __construct() {

  }

  public function getActivityContacts($activityIds, $record_type) {
    $contacts = array();
    foreach($activityIds as $activityId) {
      if (!$activityId) {
        continue;
      }
      if (!isset($this->activityContacts[$activityId]) || !isset($this->activityContacts[$activityId][$record_type])) {
        $activityContacts = civicrm_api3('ActivityContact', 'get', [
          'activity_id' => $activityId,
          'record_type_id' => $record_type,
          'options' => ['limit' => 0],
          'api.Contact.get' => ['return' => "display_name"]
        ]);
        $contactNames = [];
        foreach ($activityContacts['values'] as $activityContact) {
          if (isset($activityContact['api.Contact.get']['values'][0]['display_name'])) {
            $contactNames[] = $activityContact['api.Contact.get']['values'][0]['display_name'];
          }
        }
        $this->activityContacts[$activityId][$record_type] = implode(", ", $contactNames);
      }
      $contacts[] = $this->activityContacts[$activityId][$record_type];
    }
    return implode(", ", $contacts);
  }

  public function getActivity($activity_id) {
    if (!isset($this->activities[$activity_id])) {
      $this->activities[$activity_id] = civicrm_api3('Activity', 'getsingle', array('id' => $activity_id));
    }
    return $this->activities[$activity_id];
  }

  private function getActivityId($values, $cid) {
    $activity_id = false;
    if (isset($values[$cid]) && is_array($values[$cid]) && isset($values[$cid]['activity_id'])) {
      $activity_id = $values[$cid]['activity_id'];
    } elseif (isset($values['activity_id']) && $values['activity_id']) {
      $activity_id = $values['activity_id'];
    } elseif (isset($values[$cid]['extra_data']['activity']['id'])) {
      $activity_id = $values[$cid]['extra_data']['activity']['id'];
    }
    return $activity_id;
  }

  /**
   * Check whether a token is present in the set of tokens.
   *
   * @param $tokens
   * @param $token
   * @return bool
   */
  protected function isTokenInTokens($tokens, $token) {
    if (in_array($token, $tokens)) {
      return true;
    } elseif (isset($tokens[$token])) {
      return true;
    } elseif (isset($tokens[self::TOKEN]) && in_array($token, $tokens[self::TOKEN])) {
      return true;
    } elseif (isset($tokens[self::TOKEN][$token])) {
      return true;
    }
    return FALSE;
  }

  /**
   * Set the value for a token and checks whether cids is an array or not.
   *
   * @param $values
   * @param $cids
   * @param $token
   * @param $tokenValues
   */
  protected function setTokenValue(&$values, $cids, $token, $tokenValues) {
    if (is_array($cids)) {
      foreach ($cids as $cid) {
        $values[$cid][self::TOKEN . '.' . $token] = $tokenValues[$cid];
      }
    }
    else {
      $values[self::TOKEN . '.' . $token] = $tokenValues[$cids];
    }
  }

}
