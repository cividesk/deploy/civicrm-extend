# Activity Tokens

This extensions adds activity tokens such as:

* activity assignees
* activity targets
* activity source
* custom fields

The tokens could then be used in scheduled reminders.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v5.6+
* CiviCRM >= 5

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl activitytokens@hhttps://lab.civicrm.org/extensions/activitytokens/-/archive/master/activitytokens-master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://lab.civicrm.org/extensions/activitytokens.git
cv en activitytokens
```

## Usage

You can find the tokens when composing a message on the scheduled reminders screen.
