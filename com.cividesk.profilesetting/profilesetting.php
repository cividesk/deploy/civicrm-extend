<?php

require_once 'profilesetting.civix.php';
use CRM_Profilesetting_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function profilesetting_civicrm_config(&$config) {
  _profilesetting_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function profilesetting_civicrm_xmlMenu(&$files) {
  _profilesetting_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function profilesetting_civicrm_install() {
  _profilesetting_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
 */
function profilesetting_civicrm_postInstall() {
  _profilesetting_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function profilesetting_civicrm_uninstall() {
  _profilesetting_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function profilesetting_civicrm_enable() {
  _profilesetting_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function profilesetting_civicrm_disable() {
  _profilesetting_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function profilesetting_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _profilesetting_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function profilesetting_civicrm_managed(&$entities) {
  _profilesetting_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function profilesetting_civicrm_caseTypes(&$caseTypes) {
  _profilesetting_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_angularModules
 */
function profilesetting_civicrm_angularModules(&$angularModules) {
  _profilesetting_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function profilesetting_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _profilesetting_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_entityTypes
 */
function profilesetting_civicrm_entityTypes(&$entityTypes) {
  _profilesetting_civix_civicrm_entityTypes($entityTypes);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function profilesetting_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 */
function profilesetting_civicrm_navigationMenu(&$menu) {
  _profilesetting_civix_insert_navigation_menu($menu, 'Administer/Customize Data and Screens/Profiles', array(
    'label' => E::ts('Additional Settings'),
    'name' => 'additional_profile_settings',
    'url' => 'civicrm/admin/setting/profile',
    'permission' => 'administer CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _profilesetting_civix_navigationMenu($menu);
}

function profilesetting_civicrm_summaryOverlayProfile(&$profileID, $contactType) {
  $profileID = CRM_Profilesetting_Utils::getSettings('profilesettings_overlay_' . strtolower($contactType));
}

function profilesetting_civicrm_pageRun(&$page) {
  if ($page->getVar('_name') == 'CRM_Profile_Page_Dynamic') {
    $profileID = $page->getVar('_gid');
    $overLayProfileIDs = CRM_Profilesetting_Utils::getSettings();
    if (in_array($profileID, $overLayProfileIDs)) {
      $template = CRM_Core_Smarty::singleton();
      $template->assign('overlayProfile', TRUE);
    }
  }
}
