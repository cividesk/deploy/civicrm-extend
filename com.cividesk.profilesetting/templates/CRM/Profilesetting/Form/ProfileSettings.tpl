{crmScope extensionKey='com.cividesk.profilesetting'}
  <div class="crm-block crm-form-block crm-profilesetting-form-block">
    <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl" location="top"}</div>
    <table class="form-layout">
      <tr class="crm-profilesetting-form-block-mosaico_layout">
        <td class="label">{$form.profilesettings_overlay_individual.label}</td>
        <td>{$form.profilesettings_overlay_individual.html}</td>
      </tr>
      <tr class="crm-profilesetting-form-block-mosaico_layout">
        <td class="label">{$form.profilesettings_overlay_household.label}</td>
        <td>{$form.profilesettings_overlay_household.html}</td>
      </tr>
      <tr class="crm-profilesetting-form-block-mosaico_layout">
        <td class="label">{$form.profilesettings_overlay_organization.label}</td>
        <td>{$form.profilesettings_overlay_organization.html}</td>
      </tr>
    </table>
    <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl" location="bottom"}</div>
  </div>
{/crmScope}
