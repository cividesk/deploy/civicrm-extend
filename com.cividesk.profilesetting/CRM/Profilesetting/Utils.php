<?php

use CRM_Profilesetting_ExtensionUtil as E;

/**
 * Class CRM_Profilesetting_Utils
 */
class CRM_Profilesetting_Utils {

  /**
   * States, provinces
   * @var array
   */
  private static $profileLists;

  /**
   * Get a list of profile.
   *
   * @return array
   *   Array (string $machineName => string $label).
   */
  public static function getProfileList() {
    static $profileLists = NULL;
    if (!self::$profileLists) {
      $result = civicrm_api3('UFGroup', 'get', array(
        'sequential' => 1,
        'return' => array("id", "title"),
        'group_type' => array('IN' => array("Organization", "Contact", "Individual", "Household")),
        'is_active' => 1,
      ));
      $profileLists = array();
      foreach ($result['values'] as $profileDetails) {
        $profileLists[$profileDetails['id']] = $profileDetails['title'];
      }
      !self::$profileLists = $profileLists;
    }
    return self::$profileLists;
  }

  /**
   * Returns normalizer settings
   */
  static function getSettings($name = NULL) {
    // default Profile ID
    $summaryOverlayProfileId = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_UFGroup', 'summary_overlay', 'id', 'name');
    if (!empty($name)) {
      return CRM_Core_BAO_Setting::getItem('Profilesettings', $name, '', $summaryOverlayProfileId);
    }
    $settingsField = array('profilesettings_overlay_individual', 'profilesettings_overlay_household', 'profilesettings_overlay_organization');
    $settings = array();
    foreach ($settingsField as $fieldName) {
      $settings[$fieldName] = CRM_Core_BAO_Setting::getItem('Profilesettings', $fieldName, '', $summaryOverlayProfileId);
    }
    return $settings;
  }

}