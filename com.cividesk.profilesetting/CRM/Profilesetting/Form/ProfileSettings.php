<?php

use CRM_Profilesetting_ExtensionUtil as E;

/**
 * Form controller class
 *
 * @see https://wiki.civicrm.org/confluence/display/CRMDOC/QuickForm+Reference
 */
class CRM_Profilesetting_Form_ProfileSettings extends CRM_Admin_Form_Setting {
  protected $_settings = array(
    'profilesettings_overlay_individual'   => 'Individual Profile',
    'profilesettings_overlay_household'    => 'Household Profile',
    'profilesettings_overlay_organization' => 'Organization Profile'
  );

  /**
   * Build the form object.
   */
  public function buildQuickForm() {
    parent::buildQuickForm();
  }
}
