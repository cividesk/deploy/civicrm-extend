<?php

class CRM_Kamlanguage_Main {

  /**
   * @return array
   */
  public static function getLanguages() {
    $menu = [];

    // Get the available (enabled) languages for CiviCRM
    $languages = CRM_Core_I18n::uiLanguages();

    // It does not make sense to display the language selector for a single language
    if (count($languages) < 2) {
      return [];
    }

    $inheritLocale = Civi::settings()->get('inheritLocale');
    $config = CRM_Core_Config::singleton();

    // Drupal7
    if ($inheritLocale && function_exists('drupal_multilingual') && drupal_multilingual()) {
      // Based on the locale.module
      $types = language_types_configurable(FALSE);
      $type = $types[0];

      $path = $_GET['q'];
      $links = language_negotiation_get_switch_links($type, $path);

      if (empty($links->links)) {
        return $menu;
      }

      global $language;
      $langcode = $language->language;

      $menu = [
        'label' => $links->links[$langcode]['language']->native,
        'name' => 'kamlanguage_items',
        'icon' => 'crm-i fa-language',
        'child' => [],
      ];

      unset($links->links[$langcode]);

      foreach ($links->links as $key => $val) {
        $node = [
          'label' => $val['language']->native,
          'url' => url($val['href'], ['language' => $val['language']]) . '?' . http_build_query(_civicrm_get_url_parameters()), // FIXME: hack? (based on civicrm.module)
          'name' => 'lang_' . $key,
          'icon' => 'crm-i fa-fw',
        ];

        $menu['child'][] = $node;
      }
    }
    // Drupal8
    elseif ($inheritLocale && $config->userFramework == 'Drupal8') {
      $links = \Drupal::languageManager()->getLanguageSwitchLinks(\Drupal\Core\Language\LanguageInterface::TYPE_INTERFACE, new \Drupal\Core\Url('<current>'));

      if (empty($links->links)) {
        return $menu;
      }

      $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();

      $menu = [
        'label' => $links->links[$langcode]['title'],
        'name' => 'kamlanguage_items',
        'icon' => 'crm-i fa-language',
        'child' => [],
      ];

      foreach ($links->links as $lang => $link) {
        // Replace the language prefix with the right one
        // This feels like an odd hack, but works on Drupal 9.3
        $base = 'internal:';
        $url = $link['url']->toString();
        $url = \Drupal\Core\Url::fromUri("{$base}{$url}", [
          'language' => $link['language'],
        ])->toString();

        $node = [
          'label' => $link['title'],
          'url' => $url . '?' . $_SERVER['QUERY_STRING'],
          'name' => 'lang_' . $lang,
          'icon' => 'crm-i fa-fw',
        ];

        $menu['child'][] = $node;
      }
    }
    else {
      // Fallback: no CMS-specific URL, rely on the lcMessages URL param
      $menu = [
        'label' => $languages[CRM_Core_I18n::getLocale()],
        'name' => 'kamlanguage_items',
        'icon' => 'crm-i fa-language',
        'child' => [],
      ];

      $queryParams = [];
      if (!empty($_SERVER['QUERY_STRING'])) {
        $query = explode('&', urldecode($_SERVER['QUERY_STRING']));
        foreach ($query as $param) {
          $parts = explode('=', $param);
          $queryParams[$parts[0]] = $parts[1];
        }
      }
      foreach ($languages as $key => $label) {
        $queryParams['lcMessages'] = $key;
        // q is also available from CRM_Utils_System::currentPath. page is Wordpress only and not required because
        //   it's added by CRM_Utils_System::url automatically.
        unset($queryParams['q'], $queryParams['page']);
        $node = [
          'label' => $label,
          'url' => CRM_Utils_System::url(CRM_Utils_System::currentPath(), $queryParams, TRUE),
          'name' => 'lang_' . $key,
          'icon' => 'crm-i fa-fw',
        ];

        $menu['child'][] = $node;
      }
    }

    return $menu;
  }

}
