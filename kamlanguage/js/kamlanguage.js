(function($) {
  $(document)
    .on('crmLoad', '#civicrm-menu', function() {
      if (CRM.vars && CRM.vars.kamlanguage && !CRM.menubar.getItem('kamlanguage')) {
        CRM.menubar.addItems(-1, null, [CRM.vars.kamlanguage]);

        // Workaround for AngularJS pages, where the #hash value is important
        $('#civicrm-menu').on('click.smapi', function(e, item) {
          var parent_name = $(item).parent().data('name');

          if (typeof parent_name == 'string' && parent_name.substr(0, 5) == 'lang_') {
            var hash = window.location.hash;

            if (hash && hash != '#') {
              $(item).attr('href', $(item).attr('href') + hash);
            }
          }
        });
      }
    });
})(CRM.$);
