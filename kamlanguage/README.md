# Language Switcher

Adds a language switcher to the CiviCRM menu bar.

This extension was heavily inspired by the [recentmenu](https://github.com/civicrm/org.civicrm.recentmenu) extension.

![Screenshot](/images/screenshot.png)

The extension is licensed under [AGPL-3.0](LICENSE.txt).

You can find the latest version at:  
https://lab.civicrm.org/extensions/kamlanguage

## Requirements

* PHP v7.0+
* CiviCRM 5.29 or later

## Installation

Install as a regular CiviCRM extension.

## Getting Started

A language menu item will automatically be added to the CiviCRM menu if it detected a supported CMS configuration.

## Known Issues

* When using the option "Inherit CMS language", only Drupal7 and Drupal8 have been tested (with the i18n/locale modules). Otherwise, it will fallback to using the CiviCRM "lcMessages" variable for changing the language (which is the regular CiviCRM behavrious when not using the "Inherit Language" option). We would like to add better support for WordPress as well (Polylang and WPML). Please contact us if you are interested.

## Support

Please post bug reports in the issue tracker of this project on Gitlab:  
https://lab.civicrm.org/extensions/kamlanguage

Support via Coop SymbioTIC:  
https://www.symbiotic.coop/en

Coop Symbiotic is a worker-owned co-operative based in Canada. We have a strong
experience working with non-profits and CiviCRM. We provide affordable, fast,
turn-key hosting with regular upgrades and proactive monitoring, as well as
custom development and training.
