<?php

/**
 * This class generates form components for the Member token settings form
 *
 */
class CRM_Membertoken_Form_Settings extends CRM_Admin_Form_Setting {

  /**
   * Pre process function.
   */
  public function preProcess() {
  }

  /**
   * Build the form object.
   *
   * @return void
   */
  public function buildQuickForm() {
    $intro = ts("Please define the value for the <strong>{member.message}</strong> token:");
    $this->assign('intro', $intro);

    //post-field text for the configuration options for Current/Expired and Non Member
    $current = ts('Message if the contact has at least one membership in New or Active status');
    $expired = ts('Otherwise, if the contact has at least one membership in Grace or Expired status');
    $non_member = ts('Otherwise, if the contact has no membership, or a membership with any other status');

    $this->assign('current', $current);
    $this->assign('expired', $expired);
    $this->assign('non_member', $non_member);

    $this->add('textarea', 'current_text', ts('Current Member'), ['cols' => '50', 'rows' => '5']);
    $this->add('textarea', 'expired_text', ts('Expired Member'), ['cols' => '50', 'rows' => '5']);
    $this->add('textarea', 'non_text', ts('Non-Member'), ['cols' => '50', 'rows' => '5']);

    $footer = ts('You can use the tokens  <strong>{member.id}</strong>, <strong>{member.start_date}</strong>, <strong>{member.end_date}</strong>, <strong>{member.type}</strong> and <strong>{member.status}</strong> in the first two messages above');

    $this->assign('footer', $footer);
    $this->addButtons(array(
      array (
        'type' => 'submit',
        'name' => ts('Submit'),
        'isDefault' => TRUE,
      )
    ));

    $this->assign('elementNames', $this->getRenderableElementNames());
    parent::buildQuickForm();
  }

  /**
   * Set default values for the form.
   *
   * @return array
   *   default value for the fields on the form
   */
  public function setDefaultValues() {
    $this->_defaults['current_text'] = Civi::settings()->get('current_text');
    $this->_defaults['expired_text'] = Civi::settings()->get('expired_text');
    $this->_defaults['non_text'] = Civi::settings()->get('non_text');
    return $this->_defaults;
  }

  /**
   * Process postProcess.
   *
   *
   * @return void
   */
  public function postProcess() {
    $formValues = $this->_submitValues;

    $currentText = isset($formValues['current_text']) ? $formValues['current_text'] : NULL;
    Civi::settings()->set('current_text', $currentText);
    
    $expiredText = isset($formValues['expired_text']) ? $formValues['expired_text'] : NULL;
    Civi::settings()->set('expired_text', $expiredText);
    
    $nonText = isset($formValues['non_text']) ? $formValues['non_text'] : NULL;
    Civi::settings()->set('non_text', $nonText);
  }

  /**
   * Get the fields/elements defined in this form.
   *
   * @return array (string)
   */
  function getRenderableElementNames() {
    // The _elements list includes some items which should not be
    // auto-rendered in the loop -- such as "qfKey" and "buttons". These
    // items don't have labels. We'll identify renderable by filtering on
    // the 'label'.
    $elementNames = array();
    foreach ($this->_elements as $element) {
      $label = $element->getLabel();
      if (!empty($label)) {
        $elementNames[] = $element->getName();
      }
    }
    return $elementNames;
  }

}
