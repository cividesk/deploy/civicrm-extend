<?php

require_once 'membershipstatustoken.civix.php';

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function membershipstatustoken_civicrm_config(&$config) {
  _membershipstatustoken_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @param array $files
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function membershipstatustoken_civicrm_xmlMenu(&$files) {
  _membershipstatustoken_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function membershipstatustoken_civicrm_install() {
  _membershipstatustoken_civix_civicrm_install();
}

/**
* Implements hook_civicrm_postInstall().
*
* @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
*/
function membershipstatustoken_civicrm_postInstall() {
  _membershipstatustoken_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function membershipstatustoken_civicrm_uninstall() {
  _membershipstatustoken_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function membershipstatustoken_civicrm_enable() {
  _membershipstatustoken_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function membershipstatustoken_civicrm_disable() {
  _membershipstatustoken_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed
 *   Based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function membershipstatustoken_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _membershipstatustoken_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function membershipstatustoken_civicrm_managed(&$entities) {
  _membershipstatustoken_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * @param array $caseTypes
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function membershipstatustoken_civicrm_caseTypes(&$caseTypes) {
  _membershipstatustoken_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function membershipstatustoken_civicrm_angularModules(&$angularModules) {
_membershipstatustoken_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function membershipstatustoken_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _membershipstatustoken_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Functions below this ship commented out. Uncomment as required.
 *

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function membershipstatustoken_civicrm_preProcess($formName, &$form) {

} // */


function evaluateMembershipText($contactID) {
  $current_text = Civi::settings()->get('current_text');
  $expired_text = Civi::settings()->get('expired_text');
  $non_text = Civi::settings()->get('non_text');

  //calculate all memberships for this contact on basis of the following status
  //{1 : New}
  //{2 : Current}
  //{3 : Grace}
  //{4 : Expired}
  $query = "
      SELECT m.id, m.contact_id, m.start_date, m.end_date, m.status_id,
	     mt.name as type, mt.minimum_fee, mt.duration_unit, mt.duration_interval,
             mp.id as owner_mid, mp.contact_id as owner_cid
      FROM   civicrm_membership m
             LEFT JOIN civicrm_membership_type mt ON mt.id = m.membership_type_id
             LEFT JOIN civicrm_membership mp ON mp.id = m.owner_membership_id
      WHERE  m.contact_id IN ({$contactID})
        AND  m.is_test = 0
        AND  m.status_id IN (1, 2, 3, 4)
      ORDER BY m.status_id DESC
      ";

    $dao = CRM_Core_DAO::executeQuery($query);
    $fields = ['id', 'contact_id', 'start_date', 'end_date', 'type', 'status_id', 'owner_id', 'checksum'];

    $membership = [];
    $membershipStatuses = CRM_Member_PseudoConstant::membershipStatus();
    while ( $dao->fetch( ) ) {
      if (in_array($dao->status_id, [1 , 2])) {
        // New / Current
        $status = $current_text;
      } elseif (in_array($dao->status_id, [3 , 4])) {
        // Grace / Expired
        $status = $expired_text;
      }

      foreach ($fields as $field) {
        if (in_array($field, ['start_date', 'end_date', 'join_date'])) {
          $membership[$field] = CRM_Utils_Date::customFormat($dao->$field);
        } elseif ($field == 'status_id') {
          $membership['status'] = $membershipStatuses[$dao->$field];
        } else {
          $membership[$field] = $dao->$field;
        }
      }
      // Set tokens member.owner_id and member.checksum to the primary member's
      $membership['owner_id'] = ($dao->owner_mid ? $dao->owner_cid : $dao->contact_id);
      $membership['first_name'] = CRM_Core_DAO::getFieldValue('CRM_Contact_DAO_Contact', $contactID, 'first_name');
      $membership['checksum'] = CRM_Contact_BAO_Contact_Utils::generateChecksum($membership['owner_id']);
    }

    foreach($membership as $token => $value) {
      $status = CRM_Utils_Token::token_replace('member', $token, $value, $status);
    }

    //if no membership of status (New/Current/Grace/Expired) is found
    //get 'not a member' text
    if (!$status) {
      $membership['contact_id'] = $contactID;
      $membership['first_name'] = CRM_Core_DAO::getFieldValue('CRM_Contact_DAO_Contact', $contactID, 'first_name');
      $membership['checksum'] = CRM_Contact_BAO_Contact_Utils::generateChecksum($contactID);
      foreach($membership as $token => $value) {
        $status = CRM_Utils_Token::token_replace('member', $token, $value, $non_text);
      }
    }

    return $status;
}

/**
 * Implementation of hook_civicrm_tokenValues
 */
function membershipstatustoken_civicrm_tokenValues( &$values, &$contactIDs, $jobID = null, $tokens = array(), $context = null, $componentID = null) {
  if (!is_array($contactIDs)) {
    $crmContactIDs = array($contactIDs);
  } else {
    $crmContactIDs = array_values($contactIDs);
  }

  //https://projects.cividesk.com/projects/95/tasks/4559
  foreach($crmContactIDs as $crmContactID) {
    if (array_key_exists('member', $tokens)) {
      //calculate the membership text based on membership status
      $values[$crmContactID]['member.message'] = evaluateMembershipText($crmContactID);
    }
  }
}

/**
 * Implementation of hook_civicrm_tokens
 */
function membershipstatustoken_civicrm_tokens(&$tokens) {
  $tokens['member']['member.message'] = ts('Membership detailed message');
}

/**
 * Implementation of hook_civicrm_alterAdminPanel
 */
function membershipstatustoken_civicrm_alterAdminPanel(&$adminPanel) {
  $adminPanel['CiviMember']['fields']['{weight}.MemberTokenSettings']['title'] = ts('Member Message Token Settings');
  $adminPanel['CiviMember']['fields']['{weight}.MemberTokenSettings']['url'] =  CRM_Utils_System::url('civicrm/membertoken/settings' , 'reset=1');
  $adminPanel['CiviMember']['fields']['{weight}.MemberTokenSettings']['desc'] = ts('Member Message Token Settings');
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function membershipstatustoken_civicrm_navigationMenu(&$menu) {
  _membershipstatustoken_civix_insert_navigation_menu($menu, NULL, array(
    'label' => ts('The Page', array('domain' => 'com.cividesk.membershipstatustoken')),
    'name' => 'the_page',
    'url' => 'civicrm/the-page',
    'permission' => 'access CiviReport,access CiviContribute',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _membershipstatustoken_civix_navigationMenu($menu);
} // */
