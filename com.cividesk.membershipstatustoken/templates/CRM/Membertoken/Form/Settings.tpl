{* this template is used for adding/editing text for membership status tokens*}
<div class="crm-form crm-form-block crm-membertoken-form-block">
  <div class="crm-submit-buttons">
    {include file="CRM/common/formButtons.tpl" location='top'}
  </div>
  <div class="help">{$intro}</div>

   <table class="form-layout-compressed">
    <tr>
      <td class="label">{$form.current_text.label}</td><td>{$form.current_text.html}<br />
       <span class="description">{$current}</span></td>
    </tr>
    <tr>
      <td class="label">{$form.expired_text.label}</td><td>{$form.expired_text.html}<br />
       <span class="description">{$expired}</span></td>
    </tr>
    <tr>
      <td class="label">{$form.non_text.label}</td><td>{$form.non_text.html}<br />
       <span class="description">{$non_member}</span></td>
    </tr>
  </table>
  <div class="help">{$footer}</div>
  <div class="crm-submit-buttons">
    {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>
</div>
