<?php

require_once 'shoreditchwpworkarounds.civix.php';
use CRM_Shoreditchwpworkarounds_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/ 
 */
function shoreditchwpworkarounds_civicrm_config(&$config) {
  _shoreditchwpworkarounds_civix_civicrm_config($config);

  if (empty(CRM_Utils_Request::retrieveValue('snippet', 'String'))) {
    $path = CRM_Utils_System::getUrlPath();
    $item = CRM_Core_Menu::get($path);
    $classname = 'page-' . strtr($item['path'], '/', '-');

    Civi::resources()
      ->addScriptFile('shoreditchwpworkarounds', 'shoreditchwpworkarounds.js')
      ->addScript('CRM.shoreditchwpworkaroundsTweaks("' . $classname . '")')
      ->addStyleFile('shoreditchwpworkarounds', 'shoreditchwpworkarounds.css');
  }
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function shoreditchwpworkarounds_civicrm_xmlMenu(&$files) {
  _shoreditchwpworkarounds_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function shoreditchwpworkarounds_civicrm_install() {
  _shoreditchwpworkarounds_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function shoreditchwpworkarounds_civicrm_postInstall() {
  _shoreditchwpworkarounds_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function shoreditchwpworkarounds_civicrm_uninstall() {
  _shoreditchwpworkarounds_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function shoreditchwpworkarounds_civicrm_enable() {
  _shoreditchwpworkarounds_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function shoreditchwpworkarounds_civicrm_disable() {
  _shoreditchwpworkarounds_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function shoreditchwpworkarounds_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _shoreditchwpworkarounds_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function shoreditchwpworkarounds_civicrm_managed(&$entities) {
  _shoreditchwpworkarounds_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function shoreditchwpworkarounds_civicrm_angularModules(&$angularModules) {
  _shoreditchwpworkarounds_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function shoreditchwpworkarounds_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _shoreditchwpworkarounds_civix_civicrm_alterSettingsFolders($metaDataFolders);
}
