(function($, _, ts){

  /**
   * Various UI workarounds.
   */
  CRM.shoreditchwpworkaroundsTweaks = function(path) {
    $(function() {
      $('body').addClass(path).addClass('page-civicrm');
    });
  };

})(CRM.$, CRM._, CRM.ts('shoreditchwpworkarounds'));
