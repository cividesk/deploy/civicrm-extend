# Shoreditch WordPress Workarounds

![Screenshot](/images/screencast.gif)

Provides a few visual improvements so that Shoreditch in WordPress looks a bit more like it does in Drupal7.

You most probably want to use the [CiviCRM Admin
Utilities](https://civicrm.org/blog/danaskallman/wordpress-admin-styles-for-civicrm-with-wordpress)
and/or [haystacktheme](https://lab.civicrm.org/extensions/haystacktheme)
instead.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.1+
* CiviCRM 5.latest

## Installation

Install as a regular CiviCRM extension.

## Usage

Just enable the extension.

## Known Issues

It has not been tested on public forms, but it should not cause new issues.
