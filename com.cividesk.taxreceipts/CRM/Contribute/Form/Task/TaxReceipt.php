<?php
/**
 * User: sachindoijad
 * Date: 27/08/19
 * Time: 3:08 PM
 */


use Civi\Token\TokenProcessor;
class CRM_Contribute_Form_Task_TaxReceipt extends CRM_Contribute_Form_Task {

        use CRM_Contact_Form_Task_PDFTrait;
  /**
   * All the existing templates in the system.
   *
   * @var array
   */
  public $_templates = NULL;

  public $_single = NULL;

  public $_cid = NULL;

  /**
   * Build all the data structures needed to build the form.
   */
  public function preProcess() {
    //Tax -receipt to be sent only for the contribution of status 'Completed'
    $id = CRM_Utils_Request::retrieve('id', 'Positive',
      $this, FALSE
    );
    if ($id) {
      $this->_contributionIds = array($id);
      $this->_componentClause = " civicrm_contribution.id IN ( $id ) ";
      $this->_single = TRUE;
      $this->assign('totalSelectedContributions', 1);
    }
    else {
      parent::preProcess();
    }
    // check that all the contribution ids have pending status
    $query = "
      SELECT count(*)
      FROM   civicrm_contribution
      WHERE  contribution_status_id != 1
      AND    {$this->_componentClause}";

    $count = CRM_Core_DAO::singleValueQuery($query);
    if ($count != 0) {
      CRM_Core_Error::statusBounce("Please select contributions only with Completed status.");
    }

    $this->skipOnHold = $this->skipDeceased = FALSE;
    CRM_Contact_Form_Task_PDFLetterCommon::preProcess($this);
    // store case id if present
    $this->_caseId = CRM_Utils_Request::retrieve('caseid', 'CommaSeparatedIntegers', $this, FALSE);
    if (!empty($this->_caseId) && strpos($this->_caseId, ',')) {
      $this->_caseIds = explode(',', $this->_caseId);
      unset($this->_caseId);
    }

    // retrieve contact ID if this is 'single' mode
    $cid = CRM_Utils_Request::retrieve('cid', 'CommaSeparatedIntegers', $this, FALSE);

    $this->_activityId = CRM_Utils_Request::retrieve('id', 'Positive', $this, FALSE);

    if ($cid) {
      CRM_Contact_Form_Task_PDFLetterCommon::preProcessSingle($this, $cid);
      $this->_single = TRUE;
    }
    else {
      parent::preProcess();
    }
    $this->assign('single', $this->_single);
  }

  /**
   * This virtual function is used to set the default values of
   * various form elements
   *
   * access        public
   *
   * @return array
   *   reference to the array of default values
   */
  /**
   * @return array
   */
  public function setDefaultValues() {
    $defaults = array();
    if (isset($this->_activityId)) {
      $params = array('id' => $this->_activityId);
      CRM_Activity_BAO_Activity::retrieve($params, $defaults);
      $defaults['html_message'] = CRM_Utils_Array::value('details', $defaults);
    }
    else {
      $defaults['thankyou_update'] = 1;
    }
    $defaults = $defaults + CRM_Contact_Form_Task_PDFLetterCommon::setDefaultValues();
    return $defaults;
  }

  /**
   * Build the form
   *
   * @access public
   *
   * @return void
   */
  public function buildQuickForm(){
    //enable form element
    global $formName;
    $formName = 'CRM_Contribute_Form_Task_TaxReceipt';
    $this->assign('suppressForm', FALSE);

    // Build common form elements
    CRM_Contribute_Form_Task_PDFLetterCommon::buildQuickForm($this);

    // specific need for contributions
    $this->add('static', 'more_options_header', NULL, ts('Tax Receipts Options'));

    $emailOptions = array('' => ts('Generate PDFs for printing (only)'),
                          'email' => ts('Send emails where possible. Generate printable PDFs for contacts who cannot receive email.'),
                          'both' => ts('Send emails where possible. Generate printable PDFs for all contacts.')
                        );

    $this->addElement('select', 'email_options', ts('Print and email options'), $emailOptions, array(), "<br/>", FALSE);

    $this->addButtons(array(array('type' => 'upload', 'name' => ts('Send Tax Receipts'), 'isDefault' => TRUE,), array('type' => 'cancel', 'name' => ts('Done'),),));
  }

  /**
   * Process the form after the input has been submitted and validated.
   */
  public function postProcess(){
    if($this->getName() == 'TaxReceipt') {
      // Set the value of Custom field "Tax receipt sent" to Yes(true) Which is NO(false) By-default
      // Set the value of Custom field "Tax receipt Date" to Current default date
      $contributionId = $this->getVar('_contributionIds');

      //get the custom field id for Tax receipt sent Date and Tax sent
      $result = civicrm_api3('CustomField', 'getsingle', array('return' => array('id'), 'name' => 'Tax_receipt_sent_date'));
      $date = $result['id'];

      $result = civicrm_api3('CustomField', 'getsingle', array('return' => array('id'), 'name' => 'Tax_receipt_sent'));
      $id = $result['id'];

      $taxDate = 'custom_' . $date;
      $taxSent = 'custom_' . $id;

      if (!empty($contributionId)) {
        foreach($contributionId as $contrId) {
          $default_date =  date('Y').date('m').date('d');
          $set_params = array('entityID' => $contrId, $taxSent => 1, $taxDate => $default_date);
          CRM_Core_BAO_CustomValueTable::setValues($set_params);
        }
      }
    }
       $formValues = $this->controller->exportValues($this->getName());
    [$formValues, $html_message] = $this->processMessageTemplate($formValues);

    $messageToken = CRM_Utils_Token::getTokens($html_message);

    $returnProperties = [];
    if (isset($messageToken['contact'])) {
      foreach ($messageToken['contact'] as $key => $value) {
        $returnProperties[$value] = 1;
      }
    }

    $isPDF = FALSE;
    $emailParams = [];
    if (!empty($formValues['email_options'])) {
      $returnProperties['email'] = $returnProperties['on_hold'] = $returnProperties['is_deceased'] = $returnProperties['do_not_email'] = 1;
      $emailParams = [
        'subject' => $formValues['subject'] ?? NULL,
        'from' => $formValues['from_email_address'] ?? NULL,
      ];

      $emailParams['from'] = CRM_Utils_Mail::formatFromAddress($emailParams['from']);

      // We need display_name for emailLetter() so add to returnProperties here
      $returnProperties['display_name'] = 1;
      if (stristr($formValues['email_options'], 'pdfemail')) {
        $isPDF = TRUE;
      }
    }
    // update dates ?
    $receipt_update = $formValues['receipt_update'] ?? FALSE;
    $thankyou_update = $formValues['thankyou_update'] ?? FALSE;
    $nowDate = date('YmdHis');
    $receipts = $thanks = $emailed = 0;
    $updateStatus = '';
    $task = 'CRM_Contribution_Form_Task_PDFLetterCommon';
    $realSeparator = ', ';
    $tableSeparators = [
      'td' => '</td><td>',
      'tr' => '</td></tr><tr><td>',
    ];
    //the original thinking was mutliple options - but we are going with only 2 (comma & td) for now in case
    // there are security (& UI) issues we need to think through
    if (isset($formValues['group_by_separator'])) {
      if (in_array($formValues['group_by_separator'], ['td', 'tr'])) {
        $realSeparator = $tableSeparators[$formValues['group_by_separator']];
      }
      elseif ($formValues['group_by_separator'] == 'br') {
        $realSeparator = "<br />";
      }
    }
    // a placeholder in case the separator is common in the string - e.g ', '
    $separator = '****~~~~';
    $groupBy = $this->getSubmittedValue('group_by');

    // skip some contacts ?
    $skipOnHold = $this->skipOnHold ?? FALSE;
    $skipDeceased = $this->skipDeceased ?? TRUE;
    $contributionIDs = $this->getIDs();
    if ($this->isQueryIncludesSoftCredits()) {
      $contributionIDs = [];
      $result = $this->getSearchQueryResults();
      while ($result->fetch()) {
        $this->_contactIds[$result->contact_id] = $result->contact_id;
        $contributionIDs["{$result->contact_id}-{$result->contribution_id}"] = $result->contribution_id;
      }
    }
    [$contributions, $contacts] = $this->buildContributionArray($groupBy, $contributionIDs, $returnProperties, $skipOnHold, $skipDeceased, $messageToken, $task, $separator, $this->isQueryIncludesSoftCredits());
    $html = [];
    $contactHtml = $emailedHtml = [];
    foreach ($contributions as $contributionId => $contribution) {
      $contact = &$contacts[$contribution['contact_id']];
      $grouped = FALSE;
      $groupByID = 0;
      if ($groupBy) {
        $groupByID = empty($contribution[$groupBy]) ? 0 : $contribution[$groupBy];
        $contribution = $contact['combined'][$groupBy][$groupByID];
        $grouped = TRUE;
      }

      if (empty($groupBy) || empty($contact['is_sent'][$groupBy][$groupByID])) {
        $html[$contributionId] = $this->generateHtml($contact, $contribution, $groupBy, $contributions, $realSeparator, $tableSeparators, $messageToken, $html_message, $separator, $grouped, $groupByID);
        $contactHtml[$contact['contact_id']][] = $html[$contributionId];
        if ($this->isSendEmails()) {
          if (CRM_Contribute_Form_Task_PDFLetterCommon::emailLetter($contact, $html[$contributionId], $isPDF, $formValues, $emailParams)) {
            $emailed++;
            if (!stristr($formValues['email_options'], 'both')) {
              $emailedHtml[$contributionId] = TRUE;
            }
          }
        }
        $contact['is_sent'][$groupBy][$groupByID] = TRUE;
      }
      if ($this->isLiveMode()) {
        // Update receipt/thankyou dates
        $contributionParams = ['id' => $contributionId];
        if ($receipt_update) {
          $contributionParams['receipt_date'] = $nowDate;
        }
        if ($thankyou_update) {
          $contributionParams['thankyou_date'] = $nowDate;
        }
        if ($receipt_update || $thankyou_update) {
          civicrm_api3('Contribution', 'create', $contributionParams);
          $receipts = ($receipt_update ? $receipts + 1 : $receipts);
          $thanks = ($thankyou_update ? $thanks + 1 : $thanks);
        }
      }
    }

    $contactIds = array_keys($contacts);
    // CRM-16725 Skip creation of activities if user is previewing their PDF letter(s)
    if ($this->isLiveMode()) {
      $this->createActivities($html_message, $contactIds, CRM_Utils_Array::value('subject', $formValues, ts('Thank you letter')), CRM_Utils_Array::value('campaign_id', $formValues), $contactHtml);
    }
    $html = array_diff_key($html, $emailedHtml);

    //CRM-19761
    if (!empty($html)) {
      $fileName = $this->getFileName();
      if ($this->getSubmittedValue('document_type') === 'pdf') {
        CRM_Utils_PDF_Utils::html2pdf($html, $fileName . '.pdf', FALSE, $formValues);
      }
      else {
        CRM_Utils_PDF_Document::html2doc($html, $fileName . '.' . $this->getSubmittedValue('document_type'), $formValues);
      }
    }

    $this->postProcessHook();

    if ($emailed) {
      $updateStatus = ts('Receipts have been emailed to %1 contributions.', [1 => $emailed]);
    }
    if ($receipts) {
      $updateStatus = ts('Receipt date has been updated for %1 contributions.', [1 => $receipts]);
    }
    if ($thanks) {
      $updateStatus .= ' ' . ts('Thank-you date has been updated for %1 contributions.', [1 => $thanks]);
    }

    if ($updateStatus) {
      CRM_Core_Session::setStatus($updateStatus);
    }
    if (!empty($html)) {
      // ie. we have only sent emails - lets no show a white screen
      CRM_Utils_System::civiExit();
    }

//    CRM_Contribute_Form_Task_PDFLetterCommon::postProcess($this);
  }
public function buildContributionArray($groupBy, $contributionIDs, $returnProperties, $skipOnHold, $skipDeceased, $messageToken, $task, $separator, $isIncludeSoftCredits) {
    $contributions = $contacts = [];
    foreach ($contributionIDs as $item => $contributionId) {
      $contribution = CRM_Contribute_BAO_Contribution::getContributionTokenValues($contributionId, $messageToken)['values'][$contributionId];
      $contribution['campaign'] = $contribution['contribution_campaign_title'] ?? NULL;
      $contributions[$contributionId] = $contribution;

      if ($isIncludeSoftCredits) {
        //@todo find out why this happens & add comments
        [$contactID] = explode('-', $item);
        $contactID = (int) $contactID;
      }
      else {
        $contactID = $contribution['contact_id'];
      }
      if (!isset($contacts[$contactID])) {
        $contacts[$contactID] = [];
        $contacts[$contactID]['contact_aggregate'] = 0;
        $contacts[$contactID]['combined'] = $contacts[$contactID]['contribution_ids'] = [];
      }

      $contacts[$contactID]['contact_aggregate'] += $contribution['total_amount'];
      $groupByID = empty($contribution[$groupBy]) ? 0 : $contribution[$groupBy];

      $contacts[$contactID]['contribution_ids'][$groupBy][$groupByID][$contributionId] = TRUE;
      if (!isset($contacts[$contactID]['combined'][$groupBy]) || !isset($contacts[$contactID]['combined'][$groupBy][$groupByID])) {
        $contacts[$contactID]['combined'][$groupBy][$groupByID] = $contribution;
        $contacts[$contactID]['aggregates'][$groupBy][$groupByID] = $contribution['total_amount'];
      }
      else {
        $contacts[$contactID]['combined'][$groupBy][$groupByID] = CRM_Contribute_Form_Task_PDFLetterCommon::combineContributions($contacts[$contactID]['combined'][$groupBy][$groupByID], $contribution, $separator);
        $contacts[$contactID]['aggregates'][$groupBy][$groupByID] += $contribution['total_amount'];
      }
    }
    // Assign the available contributions before calling tokens so hooks parsing smarty can access it.
    // Note that in core code you can only use smarty here if enable if for the whole site, incl
    // CiviMail, with a big performance impact.
    // Hooks allow more nuanced smarty usage here.
    CRM_Core_Smarty::singleton()->assign('contributions', $contributions);
    $resolvedContacts = civicrm_api3('Contact', 'get', [
      'return' => array_keys($returnProperties),
      'id' => ['IN' => array_keys($contacts)],
      'options' => ['limit' => 0],
    ])['values'];
    foreach ($contacts as $contactID => $contact) {
      $contacts[$contactID] = array_merge($resolvedContacts[$contactID], $contact);
    }
    return [$contributions, $contacts];
}


  public function generateHtml($contact, $contribution, $groupBy, $contributions, $realSeparator, $tableSeparators, $messageToken, $html_message, $separator, $grouped, $groupByID) {
    static $validated = FALSE;
    $html = NULL;

    $groupedContributions = array_intersect_key($contributions, $contact['contribution_ids'][$groupBy][$groupByID]);
    CRM_Contribute_Form_Task_PDFLetter::assignCombinedContributionValues($contact, $groupedContributions, $groupBy, $groupByID);

    if (empty($groupBy) || empty($contact['is_sent'][$groupBy][$groupByID])) {
      if (!$validated && in_array($realSeparator, $tableSeparators) && !CRM_Contribute_Form_Task_PDFLetter::isValidHTMLWithTableSeparator($messageToken, $html_message)) {
        $realSeparator = ', ';
        CRM_Core_Session::setStatus(ts('You have selected the table cell separator, but one or more token fields are not placed inside a table cell. This would result in invalid HTML, so comma separators have been used instead.'));
      }
      $validated = TRUE;
      $html = str_replace($separator, $realSeparator, $this->resolveTokens($html_message, $contact['contact_id'], $contribution['id'], $grouped, $separator, $groupedContributions));
    }

    return $html;
  }
   protected function resolveTokens(string $html_message, int $contactID, $contributionID, $grouped, $separator, $contributions): string {
    $tokenContext = [
      'smarty' => (defined('CIVICRM_MAIL_SMARTY') && CIVICRM_MAIL_SMARTY),
      'contactId' => $contactID,
      'schema' => ['contributionId'],
    ];
    if ($grouped) {
      // First replace the contribution tokens. These are pretty ... special.
      // if the text looks like `<td>{contribution.currency} {contribution.total_amount}</td>'
      // and there are 2 rows with a currency separator of
      // you wind up with a string like
      // '<td>USD</td><td>USD></td> <td>$50</td><td>$89</td>
      // see https://docs.civicrm.org/user/en/latest/contributions/manual-receipts-and-thank-yous/#grouped-contribution-thank-you-letters
      $tokenProcessor = new TokenProcessor(\Civi::dispatcher(), $tokenContext);
      $contributionTokens = CRM_Utils_Token::getTokens($html_message)['contribution'] ?? [];
      foreach ($contributionTokens as $token) {
        $tokenProcessor->addMessage($token, '{contribution.' . $token . '}', 'text/html');
      }

      foreach ($contributions as $contribution) {
        $tokenProcessor->addRow([
          'contributionId' => $contribution['id'],
          'contribution' => $contribution,
        ]);
      }
      $tokenProcessor->evaluate();
      $resolvedTokens = [];
      foreach ($contributionTokens as $token) {
        foreach ($tokenProcessor->getRows() as $row) {
          $resolvedTokens[$token][$row->context['contributionId']] = $row->render($token);
        }
        $html_message = str_replace('{contribution.' . $token . '}', implode($separator, $resolvedTokens[$token]), $html_message);
      }
    }
    $tokenContext['contributionId'] = $contributionID;
    return CRM_Core_TokenSmarty::render(['html' => $html_message], $tokenContext)['html'];
   }

    protected function isSendEmails(): bool {
    return $this->isLiveMode() && $this->getSubmittedValue('email_options');
  }
/**
   * List available tokens for this form.
   *
   * @return array
   */
  public function listTokens() {
    $tokens = CRM_Core_SelectValues::contactTokens();
    $tokens = array_merge(CRM_Core_SelectValues::contributionTokens(), $tokens);
    return $tokens;
  }
}
