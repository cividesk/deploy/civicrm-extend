<?php

require_once 'taxreceipts.civix.php';
use CRM_Taxreceipts_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function taxreceipts_civicrm_config(&$config) {
  _taxreceipts_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_xmlMenu
 */
function taxreceipts_civicrm_xmlMenu(&$files) {
  _taxreceipts_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function taxreceipts_civicrm_install() {
  _taxreceipts_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postInstall
 */
function taxreceipts_civicrm_postInstall() {
  _taxreceipts_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_uninstall
 */
function taxreceipts_civicrm_uninstall() {
  _taxreceipts_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function taxreceipts_civicrm_enable() {
  _taxreceipts_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_disable
 */
function taxreceipts_civicrm_disable() {
  _taxreceipts_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_upgrade
 */
function taxreceipts_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _taxreceipts_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function taxreceipts_civicrm_managed(&$entities) {
  _taxreceipts_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_caseTypes
 */
function taxreceipts_civicrm_caseTypes(&$caseTypes) {
  _taxreceipts_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_angularModules
 */
function taxreceipts_civicrm_angularModules(&$angularModules) {
  _taxreceipts_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_alterSettingsFolders
 */
function taxreceipts_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _taxreceipts_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_entityTypes
 */
function taxreceipts_civicrm_entityTypes(&$entityTypes) {
  _taxreceipts_civix_civicrm_entityTypes($entityTypes);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_preProcess
 *
function taxreceipts_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function taxreceipts_civicrm_navigationMenu(&$menu) {
  _taxreceipts_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('New subliminal message'),
    'name' => 'mailing_subliminal_message',
    'url' => 'civicrm/mailing/subliminal',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _taxreceipts_civix_navigationMenu($menu);
} // */


/**
 * Implementation of hook_civicrm_searchTasks().
 *
 * Add 'Tax Receipt - Print or email' as a new search task for contributions
 */
function taxreceipts_civicrm_searchTasks($objectType, &$tasks) {
  if($objectType == 'contribution') {
    $tasks[] = array(
      'title' => 'Tax Receipt - Print or email',
      'class' => 'CRM_Contribute_Form_Task_TaxReceipt',
    );
  }
}

/**
 * Implementation of hook_civicrm_pre
 */
function taxreceipts_civicrm_pre($op, $objectName, $objectId, &$objectRef ) {
  global $formName;
  // Create the activity of type 'Tax Receipt Generated' when Tax Receipt form has been submitted
  if ($op != 'delete' && $objectName == 'Activity' && $formName == 'CRM_Contribute_Form_Task_TaxReceipt' && $tax = CRM_Core_PseudoConstant::getKey('CRM_Activity_DAO_Activity', 'activity_type_id', 'Tax Receipt generated')) {
    $objectRef['activity_type_id'] = $tax;
  }
}

/**
 * Implementation of hook_civicrm_post
 */
function taxreceipts_civicrm_post( $op, $objectName, $objectId, &$objectRef ) {
  if ($objectName == 'Contribution' && $op == 'create') {
    //get the custom field id for Tax sent
    $result = civicrm_api3('CustomField', 'getsingle', array('return' => array('id'), 'name' => 'Tax_receipt_sent'));
    $taxSent = 'custom_' . $result['id'];

    //https://projects.cividesk.com/projects/4/tasks/5067
    //set the value for tax receipt for contributions created
    $set_params = array('entityID' => $objectId, $taxSent => 0);
    CRM_Core_BAO_CustomValueTable::setValues($set_params);
  }
}

/**
 * Implementation of hook_civicrm_buildForm.
 */
function taxreceipts_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Contribute_Form_Task_TaxReceipt') {
    if ($form->elementExists('template')) {
      $elements = $form->getElement('template');
      $options = $elements->_options;
      //show templates that have string 'tax'
      //in them for ease of use on Tax Receipt form
      foreach ($options as $key => $option) {
        if (stripos($option['text'], 'tax') !== false){
          unset($options[$key]);
        }
        else {
          $optionValue = $option['attr']['value'];
          if (!empty($optionValue)) {
            CRM_Core_Region::instance('page-body')->add(array(
              'script' => "
                cj('.CRM_Contribute_Form_Task_TaxReceipt #template option[value=' + $optionValue + ']').hide();
              ",
            ));
          }
        }
      }
    }
  }
}
