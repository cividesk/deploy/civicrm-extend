<?php

class CRM_Backup_Form_Backup extends CRM_Admin_Form_Setting {
  protected $_testButtonName;

  /**
   * Build the form object.
   *
   * @return void
   */
  public function buildQuickForm() {
    // Check dependencies and display error messages
    $this->applyFilter('__ALL__', 'trim');
    $this->add('text', 'backup_dirpath', ts('Backup Directory Path'));
    $this->add('text', 'backup_url', ts('Backup Directory Url'));
    $this->addYesNo('backup_civicrm_db', ts('Backup CiviCRM DB'));
    $this->addYesNo('backup_cms_db', ts('Backup CMS DB'));
    $this->addYesNo('backup_filedir', ts('Backup Files DIR.'));

    $this->addFormRule(array('CRM_Backup_Form_Backup', 'formRule'));

    $this->addButtons(array(
      array(
        'type' => 'submit',
        'name' => ts('Save'),
        'isDefault' => TRUE,
      ),
      array(
        'type' => 'cancel',
        'name' => ts('Cancel'),
      ),
    ));
  }

  /**
   * Set default values for the form.
   *
   * @return array
   *   default value for the fields on the form
   */
  public function setDefaultValues() {
    $this->_defaults = CRM_Backup::getSetting();
    return $this->_defaults;
  }

  /**
   * Global validation rules for the form.
   *
   * @param array $fields
   *   Posted values of the form.
   *
   * @return array
   *   list of errors to be posted back to the form
   */
  public static function formRule($fields) {
    if (empty($fields['backup_dirpath'])) {
      $errors['backup_dirpath'] = 'You must enter an directory path.';
    }
    return empty($errors) ? TRUE : $errors;
  }

  /**
   * Process the form submission.
   *
   *
   * @return void
   */
  public function postProcess() {
    // flush caches so we reload details for future requests
    // CRM-11967
    CRM_Utils_System::flushCache();

    $formValues = $this->controller->exportValues($this->_name);
    foreach (array('backup_dirpath', 'backup_url', 'backup_civicrm_db', 'backup_cms_db', 'backup_filedir') as $name) {
      CRM_Backup::setSetting($name, $formValues[$name]);
    }
  }
}

