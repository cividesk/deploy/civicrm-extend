<?php

require_once 'CRM/Core/Page.php';

class CRM_Backup_Page_Backup extends CRM_Core_Page {
  public function run() {
    // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
    CRM_Utils_System::setTitle(ts('Backup Manager'));
    $backup_manager = new CRM_Backup();
    $backup_manager->cleanup();
    $list = $backup_manager->list_files();
    $this->assign('list', $list);
    $url = CRM_Utils_System::url('civicrm/backup/new', "reset=1");
    $this->assign('backupNew', $url);

    parent::run();
  }
}
