<?php



class CRM_Backup {
  /**
   * @var string
   */
  protected $_backup_dir;

  /**
   * @var string
   */
  protected $_backup_url;
  /**
   * @var int
   */
  protected $_keep_files;

  /**
   * @var string
   */
  protected $_compression;

  /**
   * @var string
   */
  protected $_db_host;

  /**
   * @var string
   */
  protected $_db_port;

  /**
   * @var string
   */
  protected $_db_protocol;

  /**
   * @var string
   */
  protected $_db_user;

  /**
   * @var string
   */
  protected $_db_passwd;

  /**
   * @var array
   */
  protected $_previous_count = array();

  /**
   * @var array
   */
  protected $_backup_files = array();

  /**
   * Database names to backup
   * Loaded via config
   *
   * @var array
   */
  protected $_db_names = array();


  const BACKUP_EXTENSION_SETTINGS = 'Backup Configurations';

  static function setSetting($setting, $value) {
    // Encrypt API key before storing in database
    return CRM_Core_BAO_Setting::setItem(
      $value,
      CRM_Backup::BACKUP_EXTENSION_SETTINGS,
      $setting);
  }

  static function getSetting($setting = NULL) {
    // Start with the default values for settings
    // Merge the settings defined in DB (no more groups in 4.7, so has to be one by one ...)
    foreach (array('backup_dirpath', 'backup_url', 'backup_civicrm_db', 'backup_cms_db', 'backup_filedir') as $name) {
      $value = CRM_Core_BAO_Setting::getItem(CRM_Backup::BACKUP_EXTENSION_SETTINGS, $name);
      if (!is_null($value)) {
        $settings[$name] = $value;
      }
    }
    // And finaly returm what was asked for ...
    if (!empty($setting)) {
      return CRM_Utils_Array::value($setting, $settings);
    } else {
      return $settings;
    }
  }
  /**
   * Initialize by loading the config
   *
   */
  public function __construct() {
    $config = $this->_get_config();

    if (empty($config)) {
      throw new Exception('Unable to read config');
    }
    if (!is_dir($config['backup_dir'])) {
      mkdir($config['backup_dir']);
    }

    // Load config
    foreach ($config as $key => $val) {
      $prop = '_'.$key;

      if (property_exists($this, $prop)) {
        $this->{$prop} = $val;
      }
    }
  }

  /**
   * Returns the config file data
   *
   * @return array
   */
  protected function _get_config() {

    $dsn_crm = DB::parseDSN(CIVICRM_DSN);
    $dsn_cms = DB::parseDSN(CIVICRM_UF_DSN);
    // CIVICRM_STANDALONE_PLUS
    return array(
      'backup_dir'    => Civi::settings()->get('backup_dirpath') .'/backup',
      'backup_url'    => Civi::settings()->get('backup_url') .'/backup',
      'keep_files'    => 3,
      'compression'   => 'gzip', // gzip, bzip2, etc
      'db_host'       => $dsn_crm['hostspec'],
      'db_port'       => $dsn_crm['port'],
      'db_protocol'   => '',
      'db_user'       => $dsn_crm['username'],
      'db_passwd'     => $dsn_crm['password'],
      'db_names'      => array(
        $dsn_crm['database'],
        $dsn_cms['database'],
      )
    );
  }

  /**
   * Run backup
   *
   * @return boolean true | false
   */
  public function backup() {
    $suffix = date('Y-m-d-H-i-s');

    if ( ! empty($this->_db_names)) {
      foreach ($this->_db_names as $name) {
        // Back it up
        $this->_single_backup($name, $suffix);
      }
    }
    $this->_single_backup('datafile', $suffix, false);

  }

  /**
   * Performs backup for a single database
   *
   * @param string $db_name
   * @param string $suffix
   * @param boolean $isDb
   * @return boolean
   */
  protected function _single_backup($db_name, $suffix, $isDb = true) {
    if (!is_dir($this->_backup_dir . '/' . $db_name)) {
      mkdir($this->_backup_dir . '/' . $db_name);
    }
    if ($isDb) {
      $filename = $this->_backup_dir . '/' . $db_name . '/' . $db_name . '_' . $suffix . '.sql';

      $params = array();
      $params[] = '-h' . $this->_db_host;
      //$params[] = '--port '.$this->_db_port;
      if ($this->_db_protocol) {
        $params[] = '--protocol ' . $this->_db_protocol;
      }
      $params[] = '-u' . $this->_db_user;
      $params[] = '-p' . $this->_db_passwd;

      $command = "mysqldump $db_name " . implode(' ', $params);

      if ($this->_compression) {
        $command .= " | {$this->_compression} > $filename.bz2";
      } else {
        $command = "mysqldump $db_name " . implode(' ', $params) . " > $filename";
      }
    }
    else {
      if (!is_dir($this->_backup_dir . '/' . $db_name)) {
        mkdir($this->_backup_dir . '/' . $db_name);
      }
      $custom_path = CRM_Core_Config::singleton()->customFileUploadDir;
      $custom_path = explode('civicrm', $custom_path);
      $filesDir = $custom_path['0'];
      $filename = $this->_backup_dir . '/' . $db_name . '/' . $db_name . '_' . $suffix . '.zip';

      // Backup the files directory
      $command = "cd $filesDir; zip -qr $filename . -x";
      $excludes = array(
        'backup/*',
        'civicrm/backup/*',
        'civicrm/files/CiviMail.*/*',
      );
      foreach ($excludes as $exclude) {
        $command .= " \"$exclude\"";
      }
    }
    $coomand = "nohup {$command} &";
    return system($command);
  }

  /**
   * Deletes backup files that should not be kept
   *
   * @return boolean
   */
  public function cleanup() {
    foreach ($this->_db_names as $name) {
      $this->_single_cleanup($name);
    }
    $this->_single_cleanup('datafile');

  }

  /**
   * List backup files
   *
   * @return boolean
   */
  public function list_files($link = true) {
    $lists = array();
    foreach ($this->_db_names as $name) {
      $lists[$name] = $this->_get_backup_files($name);
    }
    $lists['datafile'] = $this->_get_backup_files('datafile');
    if ($link) {
      $links = array();
      foreach ($lists as $name => $list ) {
        foreach ($list as $fileName) {
          $url = $this->_backup_url .'/' .$name .'/'.$fileName;
          $links[$name][] = "<a href='{$url}'>$fileName</a>";
        }
      }
      return $links;
    }
    return $lists;
  }

  /**
   * Cleans up backup for a single db or file
   *
   * @param string $db_name
   * @return boolean
   */
  protected function _single_cleanup($db_name) {
    $this->_get_backup_files($db_name);

    $delete_files = $this->_get_files_to_delete($db_name);
    if ( ! empty($delete_files)) {
      foreach ($delete_files as $key => $file) {
        $filename = $this->_backup_dir.'/'.$db_name.'/'.$file;
        $this->_delete($filename);
      }
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Returns all files to be deleted
   *
   * @param string $db_name
   * @return array $files | false
   */
  protected function _get_files_to_delete($db_name) {
    // Only return files that should not be kept
    if ( ! empty($this->_backup_files[$db_name])) {
      $ret = array();

      $count = count($this->_backup_files[$db_name]);

      if ($count > $this->_keep_files) {
        for ($c = $this->_keep_files; $c < $count; $c++) {
          if (
            isset($this->_backup_files[$db_name][$c]) &&
            $this->_backup_files[$db_name][$c] != '.' &&
            $this->_backup_files[$db_name][$c] != '..'
          ) {
            $ret[] = $this->_backup_files[$db_name][$c];
          }
        }
      }

      return $ret;
    }
    return FALSE;
  }

  /**
   * Returns all backup files in descending order
   *
   * @param string $db_name
   * @return array $files | boolean false
   */
  protected function _get_backup_files($db_name) {
    $dir = $this->_backup_dir.'/'.$db_name;

    $files = array_diff(scandir($dir, 1), array('.', '..'));

    if ( ! empty($files)) {
      $this->_backup_files[$db_name] = $files;

      return $this->_backup_files[$db_name];
    }
    return FALSe;
  }

  /**
   * Deletes the backup file
   *
   * @param string $filename
   * @return boolean
   */
  protected function _delete($filename) {
    if (is_file($filename)) {
      return unlink($filename);
    }

    return FALSE;
  }

  /**
   * Writes the counter back to the log file
   *
   * @param string $db_name
   * @param int $count
   * @return boolean
   */
  protected function _set_count($db_name, $count) {
    $filename = dirname(__FILE__).'/'.$db_name.'_count';

    $file = fopen($filename, 'wb');

    if ($file) {
      return fwrite($file, (string) $count);
    }

    return FALSE;
  }

  /**
   * Retrieves the backup count from the text file
   *
   * @param string $db_name
   * @return int
   */
  protected function _get_count($db_name) {
    $filename = dirname(__FILE__).'/'.$db_name.'_count';

    $file = fopen($filename, 'rb');

    if ($file) {
      $count = fgets($file, 4096);

      if ( ! is_numeric($count)) {
        $count = FALSE;
      }

      fclose($file);

      $this->_previous_count[$db_name] = $count;

      return $this->_previous_count[$db_name];
    }
  }
  static function takeBackup() {
    $backup_manager = new CRM_Backup();
    $backup_manager->backup();
    $url = CRM_Utils_System::url('civicrm/backup', "reset=1");
    CRM_Utils_System::redirect($url);
    return;
  }
}
