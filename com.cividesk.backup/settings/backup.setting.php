<?php
/*
 +--------------------------------------------------------------------+
 | CiviCRM version 4.7                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2013                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License and the CiviCRM Licensing Exception along                  |
 | with this program; if not, contact CiviCRM LLC                     |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*/

/**
 *
 * @package CRM
 * @copyright CiviCRM LLC (c) 2004-2013
 * $Id$
 *
 */
/*
 * Settings metadata file
 */

return array(
  'backup_dirpath' => array(
    'group_name' => 'Backup Configurations',
    'group' => 'com.cividesk.backup',
    'name' => 'backup_dirpath',
    'type' => 'String',
    'default' => '',
    'add' => '4.7',
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => 'Backup File Directory path',
    'help_text' => 'Backup File Directory path',
  ),
  'backup_url' => array(
    'group_name' => 'Backup Configurations',
    'group' => 'com.cividesk.backup',
    'name' => 'backup_url',
    'type' => 'String',
    'default' => '',
    'add' => '4.7',
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => 'Backup File Directory Url',
    'help_text' => 'Backup File Directory Url',
  ),
  'backup_civicrm_db' => array(
    'group_name' => 'Backup Configurations',
    'group' => 'com.cividesk.backup',
    'name' => 'backup_civicrm_db',
    'type' => 'Boolean',
    'default' => 0,
    'add' => '4.7',
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => 'CiviCRM Database backup',
    'help_text' => 'CiviCRM Database backup',
  ),
  'backup_cms_db' => array(
    'group_name' => 'Backup Configurations',
    'group' => 'com.cividesk.backup',
    'name' => 'backup_cms_db',
    'type' => 'Boolean',
    'default' => 0,
    'add' => '4.7',
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => 'CMS Database backup',
    'help_text' => 'CMS Database backup',
  ),
  'backup_filedir' => array(
    'group_name' => 'Backup Configurations',
    'group' => 'com.cividesk.backup',
    'name' => 'backup_filedir',
    'type' => 'Boolean',
    'default' => 0,
    'add' => '4.7',
    'is_domain' => 1,
    'is_contact' => 0,
    'description' => 'Files directory backup',
    'help_text' => 'Files directory backup',
  ),
 );

