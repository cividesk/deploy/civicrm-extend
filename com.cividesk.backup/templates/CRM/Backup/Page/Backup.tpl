<div class="help">
    Help Text
</div>

<div class="crm-block crm-form-block crm-backup_list-form-block">
    {strip}
        {if $list}
            {counter start=0 skip=1 print=false}
            {foreach from=$list item=rows key=name}
                    <div class="crm-accordion-header1">
                        {$name}
                    </div><!-- /.crm-accordion-header -->
                        <div id="{$name}" class="boxBlock">
                            <table class="name-layout-1">
                                {foreach from=$rows item=row}
                                    <tr id="row_{counter}" class="crm-backup-list">
                                        <td class="crm-backup-list-title" style="width:35%;">
                                            <div style="font-size:10px;margin-top:3px;">
                                                {$row}
                                            </div>
                                        </td>
                                    </tr>
                                {/foreach}
                            </table>
                        </div>
            {/foreach}
        {else}
            <div class="messages status no-popup">
                <div class="icon inform-icon"></div>&nbsp; {ts}There are currently no Backup avaialble.{/ts}
            </div>
        {/if}
    {/strip}

</div>

<div>
<a href="{$backupNew}">Back up Now</a>
</div>
