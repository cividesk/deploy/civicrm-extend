<div class="crm-block crm-form-block crm-backup-form-block">
    <div id="backup" class="backup">
        <fieldset>
            <legend>{ts}Backup Configuration{/ts}</legend>
            <table class="form-layout-compressed">
                <tr class="crm-backup-form-block-backup_dirpath">
                    <td class="label">{$form.backup_dirpath.label}</td>
                    <td>{$form.backup_dirpath.html}<br  />
                        <span class="description">{ts}Valid Directory path{/ts} </span>
                    </td>
                </tr>
                <tr class="crm-backup-form-block-backup_url">
                    <td class="label">{$form.backup_url.label}</td>
                    <td>{$form.backup_url.html}<br  />
                        <span class="description">{ts}Web Url path{/ts} </span>
                    </td>
                </tr>
                <tr class="crm-backup-form-block-backup_civicrm_db">
                    <td class="label">{$form.backup_civicrm_db.label}</td>
                    <td>{$form.backup_civicrm_db.html}<br  />
                        <span class="description">{ts}CiviCRM Database backup{/ts} </span>
                    </td>
                </tr>
                <tr class="crm-backup-form-block-backup_cms_db">
                    <td class="label">{$form.backup_cms_db.label}</td>
                    <td>{$form.backup_cms_db.html}<br  />
                        <span class="description">{ts}CMS Database backup{/ts} </span>
                    </td>
                </tr>
                <tr class="crm-backup-form-block-backup_filedir">
                    <td class="label">{$form.backup_filedir.label}</td>
                    <td>{$form.backup_filedir.html}<br  />
                        <span class="description">{ts}Files directory backup{/ts} </span>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div class="spacer"></div>
    <div class="crm-submit-buttons">
        {include file="CRM/common/formButtons.tpl"}
    </div>
</div>
