<?php

require_once 'richtext.civix.php';

use CRM_Richtext_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function richtext_civicrm_config(&$config) {
  _richtext_civix_civicrm_config($config);

  $smarty = CRM_Core_Smarty::singleton();
  array_push($smarty->plugins_dir, __DIR__ . '/smarty');

}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function richtext_civicrm_xmlMenu(&$files) {
  _richtext_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function richtext_civicrm_install() {
  _richtext_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function richtext_civicrm_postInstall() {
  _richtext_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function richtext_civicrm_uninstall() {
  _richtext_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function richtext_civicrm_enable() {
  _richtext_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function richtext_civicrm_disable() {
  _richtext_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function richtext_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _richtext_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function richtext_civicrm_managed(&$entities) {
  _richtext_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function richtext_civicrm_caseTypes(&$caseTypes) {
  _richtext_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function richtext_civicrm_angularModules(&$angularModules) {
  _richtext_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function richtext_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _richtext_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function richtext_civicrm_entityTypes(&$entityTypes) {
  _richtext_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_thems().
 */
function richtext_civicrm_themes(&$themes) {
  _richtext_civix_civicrm_themes($themes);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 *
function richtext_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 *
function richtext_civicrm_navigationMenu(&$menu) {
  _richtext_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('New subliminal message'),
    'name' => 'mailing_subliminal_message',
    'url' => 'civicrm/mailing/subliminal',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _richtext_civix_navigationMenu($menu);
} // */


/**
 * Implements hook_civicrm_buildForm()
 */
function richtext_civicrm_buildForm($formName, &$form) {

  switch ($formName) {
    case 'CRM_Event_Form_Participant':
    // Replace textarea with wysiwyg element for receipt_text
    if (array_key_exists('receipt_text', $form->_elementIndex)) {
      $form->removeElement('receipt_text');
      $form->add('wysiwyg', 'receipt_text', ts('Confirmation Message'));
    }
    break;

    case 'CRM_Event_Form_ManageEvent_Registration':
      $form->removeElement('confirm_email_text');
      $form->add('wysiwyg', 'confirm_email_text', ts('Message'));
      break;

  }
}

/**
 * Implements hook_civicrm_pre().
 */
function richtext_civicrm_pre($op, $objectName, $objectId, &$params) {
  if ($objectName == 'Participant') {
    // Avoid escaping of HTML for receipt_text
    $params['receipt_text'] = html_entity_decode($params['receipt_text']);
  }

  if ($objectName == 'Event' && array_key_exists('confirm_email_text',$params)) {
    // Avoid escaping of HTML for receipt_text
    $params['confirm_email_text'] = html_entity_decode($params['confirm_email_text']);
  }
}


