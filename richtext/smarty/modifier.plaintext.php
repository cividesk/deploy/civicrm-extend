<?php
/**
 * Remove formatting in an HTML string
 *
 * @param $string HTML string
 * @return Text string
 */
function smarty_modifier_plaintext($string) {

  $string = CRM_Utils_String::htmlToText($string);

  return $string;
}

/* vim: set expandtab: */

