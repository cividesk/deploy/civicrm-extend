<?php
/**
 * Decode entity formatting in an encoded HTML string
 *
 * @param $string encoded string
 * @return HTML string
 */
function smarty_modifier_htmltext($string) {


  $string = html_entity_decode($string);

  return $string;
}

