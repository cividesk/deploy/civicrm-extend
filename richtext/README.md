# Richtext Extension

Provide rich text features for plain text areas.

The only text field which is currently supported is the `receipt_text` field from the contribution pages.

## Target platform

* PHP v7.0
* CiviCRM 5.10

## Installation

### Receipt templates

Once the extension is properly installed, communication templates that refer to the `{$receipt_text}` token should be modified. 

* **HTML Format**: Replace all occurrences of `{$receipt_text|htmlize}` with `{$receipt_text}`, as the `htmlize` filter is no longer required and may cause extranous spacing or break links.
* **Plain-Text Format**: Replace all occurrences of `{$receipt_text}` with `{$receipt_text|plaintext}`, as the `receipt_text` token is no longer a plain text string and its HTML content should be converted to text.

These templates might *theorically* be fixed by running the the following SQL command *once*:

```sql
UPDATE civicrm_msg_template
SET 
  msg_html = REPLACE(msg_html, '{$receipt_text|htmlize}', '{$receipt_text}'),
  msg_text = REPLACE(msg_text, '{$receipt_text}', '{$receipt_text|plaintext}')
WHERE msg_text LIKE '%receipt_text%' OR msg_html LIKE '%receipt_text%';
```

In practice, it's not so straight forward. It is advised to make these changes *manually* to the templates found by the following SELECT query and listed under _Mailings : Message Templates : System Workflow Messages_ (`civicrm/admin/messageTemplates`).

```sql
SELECT id, msg_title, is_default from civicrm_msg_template 
WHERE (msg_text LIKE '%receipt_text%' OR msg_html LIKE '%receipt_text%';
```

| id | msg_title                                            | is_default | note |
|:---|:-----------------------------------------------------|:-----------|:-----|
|  5 | Contributions - Receipt (off-line)                   |          1 | Off-line messages are not richtext enhanced |
|  6 | Contributions - Receipt (off-line)                   |          0 | Off-line messages are not richtext enhanced |
|  7 | **Contributions - Receipt (on-line)**                |          1 | BEWARE: Modified on-line contribution receipts may have unusual `receipt_text` formatting |
|  8 | Contributions - Receipt (on-line)                    |          0 | Default message templates may not need to be enhanced |
| 29 | Memberships - Signup and Renewal Receipts (off-line) |          1 | Off-line messages are not richtext enhanced |
| 30 | Memberships - Signup and Renewal Receipts (off-line) |          0 | Off-line messages are not richtext enhanced |
| 31 | **Memberships - Receipt (on-line)**                  |          1 | BEWARE: Modified on-line membership receipts may have unusual `receipt_text` formatting |
| 32 | Memberships - Receipt (on-line)                      |          0 | Default message templates may not need to be enhanced |

## Notes

This extension provides the following Smarty modifiers :

* `plaintext`: makes use of CiviCRM's internal `CRM_Utils_String::htmlToText` function in order to convert HTML formatting to plain text.
* `htmltext`: makes use of PHP's `html_entity_decode()` function in order to unescape HTML entities, if required.

At the moment, only the `receipt_text` field from the Contribution Thank You page is ready to process HTML contents. Off-line memberships and contributions remain in plain text.

This is a *very* basic module. It provides a simple pattern for enhancing other plain text areas without patching core.

