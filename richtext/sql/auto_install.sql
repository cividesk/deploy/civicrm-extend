UPDATE civicrm_msg_template
SET
  msg_html = REPLACE(msg_html, '{$event.confirm_email_text|htmlize}', '{$event.confirm_email_text}'),
  msg_text = REPLACE(msg_text, '{$event.confirm_email_text}', '{$event.confirm_email_text|plaintext}')
WHERE msg_text LIKE '%confirm_email_text%' OR msg_html LIKE '%confirm_email_text%';
