<?php

class CRM_ContributionSearch_BAO_Query extends CRM_Contact_BAO_Query_Interface {
  static $_networkFields = [];
  protected static $_isSet = 0;
  public function &getFields() {
    return self::$_networkFields;
  }

  /**
   * @param $query
   *
   */
  public function select(&$query) {
  }

  /**
   * @param $query
   *
   */
  public function where(&$query) {
    foreach ($query->_paramLookup as $k => $values) {
      switch ($k) {
        case 'lineitemft_financial_type_id' :
          list($name, $op, $value, $grouping, $wildcard) = reset($values);
          $query->_where[$grouping][] = CRM_Contact_BAO_Query::buildClause("civicrm_line_item_search.financial_type_id", $op, $value, 'Integer');
          list($op, $value) = CRM_Contact_BAO_Query::buildQillForFieldValue('CRM_Contribute_DAO_Contribution', 'financial_type_id', $value, $op, []);
          if (!($name == 'id' && $value == 0)) {
            $query->_qill[$grouping][] = ts('%1 %2 %3', [1 => ts('Line Item Financial Type'), 2 => $op, 3 => $value]);
          }
          $query->_tables['civicrm_contribution'] = $query->_whereTables['civicrm_contribution'] = 1;
          $query->_tables['civicrm_line_item_search'] = $query->_whereTables['civicrm_line_item_search'] = 1;
          break;
      }
    }
  }

  /**
   * @param string $name
   * @param $mode
   * @param $side
   *
   */
  public function from($name, $mode, $side) {
    if ($name == 'civicrm_line_item_search') {
      return "
        LEFT JOIN civicrm_line_item civicrm_line_item_search
          ON civicrm_line_item_search.contribution_id = civicrm_contribution.id
      ";
    }
  }

  /**
   * @param $tables
   *
   */
  public function setTableDependency(&$tables) {
  }

  public function getPanesMapper(&$panes) {
  }

  /**
   * @param $panes
   *
   */
  public function registerAdvancedSearchPane(&$panes) {
  }

  /**
   * @param CRM_Core_Form $form
   * @param $type
   *
   */
  public function buildAdvancedSearchPaneForm(&$form, $type) {
  }

  /**
   * @param $paneTemplatePathArray
   * @param $type
   *
   */
  public function setAdvancedSearchPaneTemplatePath(&$paneTemplatePathArray, $type) {
  }

}
