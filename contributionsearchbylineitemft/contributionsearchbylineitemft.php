<?php

require_once 'contributionsearchbylineitemft.civix.php';
// phpcs:disable
use CRM_Contributionsearchbylineitemft_ExtensionUtil as E;
// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function contributionsearchbylineitemft_civicrm_config(&$config) {
  _contributionsearchbylineitemft_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function contributionsearchbylineitemft_civicrm_xmlMenu(&$files) {
  _contributionsearchbylineitemft_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function contributionsearchbylineitemft_civicrm_install() {
  _contributionsearchbylineitemft_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function contributionsearchbylineitemft_civicrm_postInstall() {
  _contributionsearchbylineitemft_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function contributionsearchbylineitemft_civicrm_uninstall() {
  _contributionsearchbylineitemft_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function contributionsearchbylineitemft_civicrm_enable() {
  _contributionsearchbylineitemft_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function contributionsearchbylineitemft_civicrm_disable() {
  _contributionsearchbylineitemft_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function contributionsearchbylineitemft_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _contributionsearchbylineitemft_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function contributionsearchbylineitemft_civicrm_managed(&$entities) {
  _contributionsearchbylineitemft_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function contributionsearchbylineitemft_civicrm_caseTypes(&$caseTypes) {
  _contributionsearchbylineitemft_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function contributionsearchbylineitemft_civicrm_angularModules(&$angularModules) {
  _contributionsearchbylineitemft_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function contributionsearchbylineitemft_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _contributionsearchbylineitemft_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function contributionsearchbylineitemft_civicrm_entityTypes(&$entityTypes) {
  _contributionsearchbylineitemft_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_thems().
 */
function contributionsearchbylineitemft_civicrm_themes(&$themes) {
  _contributionsearchbylineitemft_civix_civicrm_themes($themes);
}



/**
 * Implements hook_civicrm_queryObjects().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_queryObjects
 */
function contributionsearchbylineitemft_civicrm_queryObjects(&$queryObjects, $type) {
  if ($type == 'Contact') {
    $queryObjects[] = new CRM_ContributionSearch_BAO_Query();
  }
}

/**
 * Implements hook_civicrm_buildForm().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_buildForm
 */
function contributionsearchbylineitemft_civicrm_buildForm($formName, &$form) {
  if (('CRM_Contribute_Form_Search' == $formName && $form->getVar('_context') == 'search')
    || ('CRM_Contact_Form_Search_Advanced' == $formName && ('CiviContribute' == $form->_searchPane || $form->_flagSubmitted))
  ) {
    $financialTypes = [];
    CRM_Financial_BAO_FinancialType::getAvailableFinancialTypes($financialTypes, CRM_Core_Action::VIEW);
    $form->addSelect('lineitemft_financial_type_id',
      ['entity' => 'contribution', 'name' => 'financial_type_id', 'multiple' => 'multiple', 'context' => 'search', 'options' => $financialTypes]
    );
    if ('CRM_Contact_Form_Search_Advanced' == $formName && 'CiviContribute' != $form->_searchPane) {
      return;
    }

    CRM_Core_Region::instance('page-body')->add([
      'template' => 'CRM/ContributionSearch/Form/SearchForm.tpl',
    ]);
  }
}
