<table style='display:none'>
  <tr id='contributionsearchbylineitemft-filters'>
    <td>
       {ts}Line Item Financial Type{/ts}<br>
      {$form.lineitemft_financial_type_id.html}
    </td>
  </tr>
</table>

{literal}
<script type="text/javascript">
  CRM.$(function($) {
    $('div#contribution_recur').closest('tr').before($('tr#contributionsearchbylineitemft-filters'));
  });
</script>
{/literal}
